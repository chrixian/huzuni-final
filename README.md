# Huzuni #

Every version of huzuni found within my repositories!

The format of the folders are as follows: **v{version}mc{game version}**

I decided not to remove any notes files, as they show some of the thought processes behind the production of the mod.

I have a collection of notes on another PC, so once I get to it, I'll throw those into here as well.