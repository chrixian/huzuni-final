package in.brud.huzuni.ui.ingame;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.ui.ingame.IngameTheme;
import net.halalaboos.huzuni.utils.RenderUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.util.MathHelper;
import net.minecraft.world.chunk.Chunk;
import org.lwjgl.opengl.GL11;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A new Informative theme since the current one was
 * gettin' a little old.
 *
 * @author brudin
 * @version 1.0
 * @since 1/25/14
 */
public class Compact implements IngameTheme {

    /**
     * COLORS TO USE:
     *
     * TOP OF GRADIENT:     0xFF383839
     * BOTTOM GRADIENT:     0xFF2e3436
     * BORDER:              0xFF232729
     *
     * TEXT COLOR:          0xFFdfdfdf
     * SUBTEXT COLOR:       0XFFbfbfbf
     *
     * HEADER COLOR:        0xFF191c1e
     * HEADER TEXT:         0xFFd8d8d8
     *
     * BACKGROUND:          0xFF2e3436
     */

    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
        String coords = "XYZ: " + (int) mc.thePlayer.posX + ", " + (int) mc.thePlayer.posY + ", " + (int) mc.thePlayer.posZ;
        int width = 80, yPos = 14;

        if(mc.fontRenderer.getStringWidth(coords) > width - 10) {
            width = mc.fontRenderer.getStringWidth(coords) + 10;
        }

        if(getWidest() > width - 10) {
            width = getWidest() + 10;
        }

        if(!(getArrayHeight() <= 0)) {
        RenderUtils.drawRect(0, 0, width + 1, getArrayHeight() + 10, 0xFF232729);
        RenderUtils.drawRect(0, 0, width, 9 + getArrayHeight(), 0xFF2e3436);
        }
        RenderUtils.drawGradientRect(0, 10, width, 13, 0, 0xFF191c1e);
        RenderUtils.drawRect(0, 0, width, 10F, 0xFF191c1e);
        mc.fontRenderer.drawStringWithShadow(coords, 2, 2, 0xFFd8d8d8);

        for (DefaultModule mod : Huzuni.modManager.getDefaultModules()) {
            if (mod.shouldRenderWhenEnabled() && mod.isEnabled()) {
                mc.fontRenderer.drawStringWithShadow(mod.getRenderName(), 2, yPos, 0xFFbfbfbf);
                yPos += 10;
            }
        }
    }

    private int getArrayHeight() {
        int yPos = 4;
        for (DefaultModule mod : Huzuni.modManager.getDefaultModules()) {
            if (mod.shouldRenderWhenEnabled() && mod.isEnabled()) {
                yPos += 10;
            }
        }
        return yPos;
    }

    private int getWidest() {
        int width = 50;
        for (DefaultModule module : Huzuni.modManager.getDefaultModules()) {
            if (module.isEnabled()) {
                if (module.shouldRenderWhenEnabled()) {
                    if (width < mc.fontRenderer.getStringWidth(module.getRenderName()) + 6)
                        width = mc.fontRenderer.getStringWidth(module.getRenderName()) + 6;
                }
            }
        }
        return width;
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#getName()
     */
    @Override
    public String getName() {
        return "Compact";
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#onKeyPressed(int)
     */
    @Override
    public void onKeyPressed(int keyCode) {}

}