/**
 * 
 */
package net.halalaboos.huzuni.client;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;


/**
 * @author Halalaboos
 *
 * @since Sep 18, 2013
 */
public final class DonatorLoader {

	public static final String CAPE_URL = "http://halalaboos.net/client/capes/HuzuniCape.png";

	public static final String donorURL = "http://halalaboos.net/donations.txt";
	
	private static final List<String> donators = new ArrayList<String>();
	
	private static boolean loaded = false;
	
	private static boolean donator = false;
	
	public static boolean isDonator(EntityPlayer entityPlayer) {
		return donators.contains(entityPlayer.getCommandSenderName().toLowerCase());
	}
	
	public static boolean isDonator(String name) {
		return donators.contains(name.toLowerCase());
	}
	
	public static boolean isDonator() {
		if (!donator)
			donator = Minecraft.getMinecraft().getSession() == null ? false : donators.contains(Minecraft.getMinecraft().getSession().getUsername().toLowerCase());
		return donator || Settings.isVip();
	}

	public static List<String> getDonators() {
		return donators;
	}
	
}
