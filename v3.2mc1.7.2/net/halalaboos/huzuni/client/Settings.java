/**
 *
 */
package net.halalaboos.huzuni.client;

import com.darkmagician6.eventapi.Dispatcher;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.lib.io.config.ConfigListener;
import net.halalaboos.lib.io.config.DefaultConfig;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Halalaboos
 * @since Jul 25, 2013
 */
public final class Settings {
    public static final DefaultConfig config = new DefaultConfig("Settings", getConfigFile());

    /**
     * Used to prevent constant access to the hashmap.
     * */
    private static float lineSize = 0;
    
    private static boolean enabled = true;
    
    private static boolean globalFont = false;
    
    private static boolean antiAlias = true;
    
    private static boolean voidFog = false;

    private static int ingameTheme = 0;

    private static boolean vip = false;
    
    private static boolean customChat = true;

    private static boolean censorLongMessages = false;
    
    public static boolean isHuzuniEnabled() {
        return enabled;
    }

    public static void setHuzuniEnabled(boolean enabled_) {
        config.put("Enabled", enabled_);
        enabled = enabled_;
        Dispatcher.cancelCalling = !enabled_;
        Huzuni.logger.log("Set huzuni to: " + enabled_ + ".");
    }

    public static void setAntialiasing(boolean antiAlias_) {
        config.put("Antialias", antiAlias_);
        antiAlias = antiAlias_;
        Huzuni.logger.log("Set antialiasing to: " + antiAlias_ + ".");
    }

    public static boolean isAntiAlias() {
        return antiAlias;
    }

    public static boolean isGlobalFont() {
        return globalFont;
    }

    public static void setGlobalFont(boolean global) {
        config.put("Global Custom Font", global);
        globalFont = global;
        Huzuni.logger.log("Set globalfont to: " + global + ".");
    }

    public static boolean shouldLoadModules() {
        return config.getBoolean("Load Modules");
    }

    public static void setLoadModules(boolean load) {
        config.put("Load Modules", load);
    }

    public static Font getFont() {
        return new Font(config.get("Font"), Font.TRUETYPE_FONT, config.getInt("Font Size"));
    }

    public static void setFont(Font font) {
        config.put("Font", font.getName());
        config.put("Font Size", font.getSize());
    }
    
    public static float getLineSize() {
    	return lineSize;
    }
    
    public static void setLineSize(float size) {
    	lineSize = size;
        config.put("Line Size", size);
    }

    public static void setFontAntiAlias(boolean antiAlias) {
    	config.put("Font Antialias", antiAlias);
    }
    
    public static boolean isFontAntiAlias() {
    	return config.getBoolean("Font Antialias");
    }
    
    public static void setFontFractionalMetrics(boolean fractionMetrics) {
    	config.put("Font Fractional Metrics", fractionMetrics);
    }
    
    public static boolean isFontFractionalMetrics() {
    	return config.getBoolean("Font Fractional Metrics");
    }
    
    public static int getIngameTheme() {
		return ingameTheme;
	}

	public static void setIngameTheme(int ingameTheme) {
		config.put("Ingame Theme", ingameTheme);
		Settings.ingameTheme = ingameTheme;
	}

	public static boolean isVoidFog() {
		return voidFog;
	}

	public static void setVoidFog(boolean voidFog) {
		Settings.voidFog = voidFog;
		config.put("Void Fog", voidFog);
	}

	public static boolean isCustomChat() {
		return customChat;
	}

	public static void setCustomChat(boolean customChat) {
		Settings.customChat = customChat;
		config.put("Custom Chat", customChat);
	}

	public static boolean isCensorLongMessages() {
		return censorLongMessages;
	}

	public static void setCensorLongMessages(boolean censorLongMessages) {
		Settings.censorLongMessages = censorLongMessages;
		config.put("Censor Long Messages", censorLongMessages);
	}

	public static boolean isVip() {
		return vip;
	}

	public static void setVip(boolean vip) {
		Settings.vip = vip;
	}

	public static void addListener(ConfigListener configListener) {
		config.addListener(configListener);
	}
	
	public static void removeListener(ConfigListener configListener) {
		config.removeListener(configListener);
	}
	
	public static void put(String id, Object value) {
		config.put(id, value);
	}

	public static void put(String id, String value) {
		config.put(id, value);
	}

	/**
     * Sets up the default
     */
    public static void setupDefaultSettings() {
        config.put("Used", false);
        setHuzuniEnabled(true);
        setAntialiasing(true);
        setLoadModules(true);
        setGlobalFont(false);
        setFontAntiAlias(true);
        setFontFractionalMetrics(false);
        setFont(new Font("Tahoma", Font.PLAIN, 18));
        setLineSize(1F);
        setIngameTheme(0);
        setVoidFog(false);
        setCustomChat(true);
        setCensorLongMessages(false);
        config.put("GUI Theme", 0);
    }


    public static void save() {
        config.save();
    }

    public static void load() {
		config.load();
		Huzuni.display.fontRenderer.setFont(getFont());
		Huzuni.display.fontRenderer.setAntiAlias(Settings.isFontAntiAlias());
		Huzuni.display.fontRenderer.setFractionalMetrics(Settings.isFontFractionalMetrics());
		lineSize = config.getFloat("Line Size");
		enabled = config.getBoolean("Enabled");
		globalFont = config.getBoolean("Global Custom Font");
		antiAlias = config.getBoolean("Antialias");
		voidFog = config.getBoolean("Void Fog");
		customChat = config.getBoolean("Custom Chat");
		censorLongMessages = config.getBoolean("Censor Long Messages");
		ingameTheme = config.getInt("Ingame Theme");
		Huzuni.display.setTheme(getIngameTheme());
    }
    
    /**
     * @return File to hold our settings.
     */
    public static File getConfigFile() {
        File configFile = new File(Huzuni.getSaveDirectory(), "Settings.txt");
        if (!configFile.exists())
            try {
                configFile.createNewFile();
            } catch (IOException e) {
                Huzuni.logger.log(e.getMessage());
            }
        return configFile;
    }
}
