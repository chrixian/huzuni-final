package net.halalaboos.huzuni.client.waypoints;

import java.awt.Color;
import java.io.File;

import javax.xml.transform.TransformerException;

import org.lwjgl.input.Keyboard;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.events.game.EventGameEnd;
import net.halalaboos.huzuni.events.game.EventGameStartup;
import net.halalaboos.huzuni.mods.Keybindable;
import net.halalaboos.huzuni.mods.Module;
import net.halalaboos.lib.io.XMLFileHandler;
import net.halalaboos.lib.manager.Manager;

public final class WaypointManager extends Manager<Waypoint> implements Listener {
	private final XMLFileHandler fileHandler = new XMLFileHandler(new File(Huzuni.getSaveDirectory(), "Waypoints.xml"));
	
	private final WaypointRenderer waypointRenderer;
	
	public WaypointManager() {
		super();
		waypointRenderer = new WaypointRenderer(this);
		registry.registerListener(this);
	}
	
	@Override
	public boolean add(Waypoint waypoint) {
		boolean returnVal = super.add(waypoint);
		saveWaypoints();
		return returnVal;
	}
	
	@Override
	public boolean remove(Waypoint waypoint) {
		boolean returnVal = super.remove(waypoint);
		saveWaypoints();
		return returnVal;
	}
	
	@EventTarget
    public void onGameStartup(EventGameStartup event) {
    	loadWaypoints();
    }

    @EventTarget
    public void onGameEnd(EventGameEnd event) {
    	saveWaypoints();
    }
	
	public void loadWaypoints() {
        try {
        	// Read the document.
            Document doc = fileHandler.read();
            NodeList elementList = doc.getElementsByTagName("Waypoint");
            for (int i = 0; i < elementList.getLength(); i++) {
                Node node = elementList.item(i);
                // If it's an element.
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    String name = element.getAttribute("Name"),
                    server = element.getAttribute("Server");
                    boolean shouldRender = Boolean.parseBoolean(element.getAttribute("Render"));
                    double x = Double.parseDouble(element.getElementsByTagName("X").item(0).getTextContent()),
                    y = Double.parseDouble(element.getElementsByTagName("Y").item(0).getTextContent()),
                    z = Double.parseDouble(element.getElementsByTagName("Z").item(0).getTextContent());
                    Color color = Color.decode(element.getElementsByTagName("Color").item(0).getTextContent());
                    Waypoint waypoint = new Waypoint(name, server, x, y, z, color);
                    waypoint.setRender(shouldRender);
                    add(waypoint);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	public void saveWaypoints() {
        fileHandler.createDocument();
        
        Element waypointsRoot = fileHandler.createElement("Waypoints");
        fileHandler.addElement(waypointsRoot);
        
        for (Waypoint waypoint : getList()) {
            Element waypointElement = fileHandler.createElement("Waypoint");
            waypointsRoot.appendChild(waypointElement);
            // Give waypoint attributes.
            waypointElement.setAttribute("Name", waypoint.getName());
            waypointElement.setAttribute("Server", waypoint.getServer());
            waypointElement.setAttribute("Render", "" + waypoint.shouldRender());

            waypointElement.appendChild(fileHandler.createElement("X", "" + waypoint.getX()));
            waypointElement.appendChild(fileHandler.createElement("Y", "" + waypoint.getY()));
            waypointElement.appendChild(fileHandler.createElement("Z", "" + waypoint.getZ()));
            waypointElement.appendChild(fileHandler.createElement("Color", "" + waypoint.getColor().getRGB()));
        }
        try {
            fileHandler.write();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
	}
}
