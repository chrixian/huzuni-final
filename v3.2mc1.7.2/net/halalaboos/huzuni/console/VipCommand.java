/**
 * 
 */
package net.halalaboos.huzuni.console;

import net.halalaboos.huzuni.client.Settings;

/**
 * @author Halalaboos
 *
 */
public abstract class VipCommand implements Command {
	
	/**
	 * @see net.halalaboos.huzuni.console.Command#run(java.lang.String, java.lang.String[])
	 */
	@Override
	public void run(String input, String[] args) {
		if (Settings.isVip()) {
			runVip(input, args);
		} else
			huzuni.addChatMessage("You must be VIP to use this!");
	}

	public abstract void runVip(String input, String[] args);
	
}
