package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.console.Command;

public class Add implements Command {

    @Override
    public String[] getAliases() {
        return new String[] {"add"};
    }

    @Override
    public String[] getHelp() {
        return new String[] {"add <username>"};
    }

    @Override
    public String getDescription() {
        return "Adds a user to the friends list. (Protect them from kill aura, distinguishable from other players)";
    }

    @Override
    public void run(String input, String[] args) {
        boolean sucessful = huzuni.friendManager.add(args[0]);
        huzuni.addChatMessage("Friend '" + args[0] + "' " + (sucessful ? "added!" : "already in your friends!"));
        huzuni.friendManager.saveFriends();
    }

}
