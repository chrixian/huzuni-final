/**
 *
 */
package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.console.Command;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
/**
 * @author Halalaboos
 * @since Jul 18, 2013
 */
public class Enchant implements Command {

    /**
     * @see net.halalaboos.huzuni.console.Command#getAliases()
     */
    @Override
    public String[] getAliases() {
        return new String[] {"enchant"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getHelp()
     */
    @Override
    public String[] getHelp() {
        return new String[] {"enchant (Have to be in creative mode)"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getDescription()
     */
    @Override
    public String getDescription() {
        return "Forces all possible enchantments onto an item.";
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#run(java.lang.String, java.lang.String[])
     */
    @Override
    public void run(String input, String[] args) {
        if (mc.playerController.isInCreativeMode()) {
            ItemStack item = mc.thePlayer.inventory.getCurrentItem();
            if (item == null) {
                huzuni.addChatMessage("You must be holding an item!");
                return;
            }
            String enchantments = "Item enchanted with: ";
            for (int i = 0; i < Enchantment.enchantmentsList.length; i++) {
                Enchantment enchantment = Enchantment.enchantmentsList[i];
                if (enchantment == null)
                    continue;
                if (enchantment.type.canEnchantItem(item.getItem())) {
                    item.addEnchantment(enchantment, enchantment.getMaxLevel());
                    this.mc.playerController.sendEnchantPacket(0, i);
                    enchantments += "\247a" + StatCollector.translateToLocal(enchantment.getName()) + " " + enchantment.getMaxLevel() + "\247f, ";
                }
            }
            huzuni.addChatMessage(enchantments);
        } else
            huzuni.addChatMessage("You have to be in creative mode!");
    }

}
