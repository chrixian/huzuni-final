/**
 * 
 */
package net.halalaboos.huzuni.console.commands;

import com.google.common.base.Charsets;

import net.halalaboos.huzuni.console.Command;
import net.halalaboos.huzuni.console.VipCommand;
import net.minecraft.network.play.client.C17PacketCustomPayload;

/**
 * @author Halalaboos
 *
 */
public class ItemEnhancer extends VipCommand {

	/**
	 * @see net.halalaboos.huzuni.console.Command#getAliases()
	 */
	@Override
	public String[] getAliases() {
		return new String[] { "enhance" };
	}

	/**
	 * @see net.halalaboos.huzuni.console.Command#getHelp()
	 */
	@Override
	public String[] getHelp() {
		return new String[] { "enhance <length>" };
	}

	/**
	 * @see net.halalaboos.huzuni.console.Command#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Makes the name of your currently held item extremely large";
	}

	/**
	 * @see net.halalaboos.huzuni.console.VipCommand#runVip(java.lang.String, java.lang.String[])
	 */
	@Override
	public void runVip(String input, String[] args) {
		if (mc.playerController.isInCreativeMode()) {
			int length = Integer.parseInt(args[0]);
			String sweg = "";
	        for(int i = 0; i < length; i++){
	        	sweg = sweg + "\247k\247lg";
	        }
	        mc.thePlayer.getCurrentEquippedItem().func_151001_c(sweg);
	       	huzuni.addChatMessage("Now, drop the item and pick it back up again.");
		} else
			huzuni.addChatMessage("You must be in creative mode!");
	}

}
