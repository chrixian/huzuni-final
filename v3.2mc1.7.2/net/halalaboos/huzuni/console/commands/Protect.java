/**
 *
 */
package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.console.Command;
import net.minecraft.util.EnumChatFormatting;

/**
 * @author Halalaboos
 * @since Jul 21, 2013
 */
public class Protect implements Command {

    /**
     * @see net.halalaboos.huzuni.console.Command#getAliases()
     */
    @Override
    public String[] getAliases() {
        return new String[] {"protect", "prot"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getHelp()
     */
    @Override
    public String[] getHelp() {
        return new String[] {"protect <username> <alias>"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getDescription()
     */
    @Override
    public String getDescription() {
        return "Hides users name with an alias you specify.";
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#run(java.lang.String, java.lang.String[])
     */
    @Override
    public void run(String input, String[] args) {
        String username = args[0];
        String alias = input.split(username + " ")[1];
        if (alias.equalsIgnoreCase(username)) {
            huzuni.addChatMessage(EnumChatFormatting.BLUE + alias + EnumChatFormatting.WHITE + " removed.");
            huzuni.protectionManager.remove(username);
        } else {
            huzuni.addChatMessage(EnumChatFormatting.BLUE + alias + EnumChatFormatting.WHITE + " added.");
            huzuni.protectionManager.add(username, alias);
        }
    }

}
