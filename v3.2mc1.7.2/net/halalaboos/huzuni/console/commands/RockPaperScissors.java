package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.console.Command;
import net.minecraft.util.EnumChatFormatting;

import java.util.Random;

public class RockPaperScissors implements Command {
    final Random random = new Random();

    @Override
    public String[] getAliases() {
        return new String[] {"rps"};
    }

    @Override
    public String[] getHelp() {
        return new String[] {"rps <rock / paper / scissors>"};
    }

    @Override
    public String getDescription() {
        return "A simple rock paper scissors game against a highly intelligent life form. (java.util.Random instance)";
    }

    @Override
    public void run(String input, String[] args) {
        // dummy
        if (args == null) {
            throw new NullPointerException("Fucking rutard.");
        } else {
            Type inputtedType = getFromName(args[0]);
            if (inputtedType == null)
                throw new NullPointerException("Fucking rutard.");
            else {
                Type botType = getRandomType();
                huzuni.addChatMessage("Bot chose '" + botType.properName + "'");
                eval(inputtedType, botType);
            }
        }
    }

    /**
     * Evaluates if the input beat the bot.
     */
    public void eval(Type inputType, Type botType) {
        if (inputType == botType) {
            huzuni.addChatMessage(EnumChatFormatting.YELLOW + "Tie!");
            return;
        }
        switch (inputType) {
            case ROCK:
                if (botType == Type.SCISSORS)
                    huzuni.addChatMessage(EnumChatFormatting.DARK_GREEN + "You win!");
                else
                    huzuni.addChatMessage(EnumChatFormatting.RED + "You lose!");
                break;
            case PAPER:
                if (botType == Type.ROCK)
                    huzuni.addChatMessage(EnumChatFormatting.DARK_GREEN + "You win!");
                else
                    huzuni.addChatMessage(EnumChatFormatting.RED + "You lose!");
                break;
            case SCISSORS:
                if (botType == Type.PAPER)
                    huzuni.addChatMessage(EnumChatFormatting.DARK_GREEN + "You win!");
                else
                    huzuni.addChatMessage(EnumChatFormatting.RED + "You lose!");
                break;
        }
    }

    /**
     * @return Random Rock Paper Scissors Type.
     */
    private Type getRandomType() {
        return Type.values()[new Random().nextInt(300) / 100];
    }

    /**
     * @return Rock Paper Scissors type from the name specified.
     */
    private Type getFromName(String input) {
        for (Type type : Type.values()) {
            if (input.equalsIgnoreCase(type.properName)) {
                return type;
            }
        }
        return null;
    }

    /**
     * Holds each type of answer for our professionally programmed game 'Rock Paper Scissors'.
     */
    private enum Type {
        ROCK("Rock"), PAPER("Paper"), SCISSORS("Scissors");

        private String properName;

        Type(String properName) {
            this.properName = properName;
        }
    }
}
