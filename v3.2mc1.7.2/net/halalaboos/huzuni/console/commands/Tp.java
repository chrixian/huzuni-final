package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.console.Command;
import net.minecraft.network.play.client.C03PacketPlayer;

public class Tp implements Command {

    private double illegalStance = 1.0D;

    @Override
    public String[] getAliases() {
        return new String[] {"teleport", "tp"};
    }

    @Override
    public String[] getHelp() {
        return new String[] {"teleport <x> <y> <z>"};
    }

    @Override
    public String getDescription() {
        return "Teleports you to the specified coordinates.";
    }

    @Override
    public void run(String originalString, String[] args) {
        double xPos = mc.thePlayer.posX + Double.parseDouble(args[0]);
        double yPos = mc.thePlayer.posY + Double.parseDouble(args[1]);
        double zPos = mc.thePlayer.posZ + Double.parseDouble(args[2]);
        mc.thePlayer.setLocationAndAngles(mc.thePlayer.posX, yPos, mc.thePlayer.posZ, mc.thePlayer.rotationYaw, mc.thePlayer.rotationPitch);
        mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, yPos - illegalStance, yPos, mc.thePlayer.posZ, true));
        huzuni.addChatMessage("Teleported successfully!");
    }

}
