/**
 *
 */
package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.console.Command;
import net.minecraft.network.play.client.C03PacketPlayer;

/**
 * @author Halalaboos
 * @since Jul 18, 2013
 */
public class Vclip implements Command {

    private double illegalStance = 1.0D;

    /**
     * @see net.halalaboos.huzuni.console.Command#getAliases()
     */
    @Override
    public String[] getAliases() {
        return new String[] {"vclip", "vc", "up"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getHelp()
     */
    @Override
    public String[] getHelp() {
        return new String[] {"vclip <number>"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getDescription()
     */
    @Override
    public String getDescription() {
        return "Teleports you up / down.";
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#run(java.lang.String, java.lang.String[])
     */
    @Override
    public void run(String input, String[] args) {
        final double increment = Double.parseDouble(args[0]);
        final double yPos = mc.thePlayer.posY + increment;
        mc.thePlayer.setLocationAndAngles(mc.thePlayer.posX, yPos, mc.thePlayer.posZ, mc.thePlayer.rotationYaw, mc.thePlayer.rotationPitch);
        mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, yPos - illegalStance, yPos, mc.thePlayer.posZ, true));
        huzuni.addChatMessage("Teleported " + (increment >= 0 ? "up" : "down") + " " + Math.abs(increment) + " blocks.");
    }

}
