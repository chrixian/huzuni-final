package net.halalaboos.huzuni.events.game;

import com.darkmagician6.eventapi.events.Event;

public class EventBreakBlock implements Event {
    public final int x, y, z, side;
    protected float currentBlockDamage;
    protected int blockHitDelay = 0;
    
    public EventBreakBlock(int x, int y, int z, int side, float currentBlockDamage, int blockHitDelay) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.side = side;
        this.currentBlockDamage = currentBlockDamage;
        this.blockHitDelay = blockHitDelay;
    }

    public int getBlockHitDelay() {
		return blockHitDelay;
	}

	public void setBlockHitDelay(int blockHitDelay) {
		this.blockHitDelay = blockHitDelay;
	}

	public float getCurrentBlockDamage() {
        return currentBlockDamage;
    }

    public void setCurrentBlockDamage(float currentBlockDamage) {
        this.currentBlockDamage = currentBlockDamage;
    }


}
