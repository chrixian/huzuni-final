package net.halalaboos.huzuni.events.game;

import com.darkmagician6.eventapi.events.Event;

public class EventClick implements Event {

    public final int mouseButton;

    public EventClick(int mouseButton) {
        this.mouseButton = mouseButton;
    }

}
