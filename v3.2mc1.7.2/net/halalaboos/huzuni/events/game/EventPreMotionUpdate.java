package net.halalaboos.huzuni.events.game;

import com.darkmagician6.eventapi.events.premade.EventCancellable;

import net.minecraft.client.entity.EntityClientPlayerMP;

public class EventPreMotionUpdate extends EventCancellable {

    public final EntityClientPlayerMP player;

    public EventPreMotionUpdate(EntityClientPlayerMP player) {
        this.player = player;
    }
}
