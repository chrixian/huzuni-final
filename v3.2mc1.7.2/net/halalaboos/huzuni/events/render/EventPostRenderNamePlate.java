package net.halalaboos.huzuni.events.render;

import com.darkmagician6.eventapi.events.Event;
import net.minecraft.entity.Entity;

public class EventPostRenderNamePlate implements Event {

    public final Entity entity;
    private String titleRendered;

    public EventPostRenderNamePlate(Entity entity, String titleRendered) {
        this.entity = entity;
        this.titleRendered = titleRendered;
    }

    public String getTitleRendered() {
        return titleRendered;
    }

    public void setTitleRendered(String titleRendered) {
        this.titleRendered = titleRendered;
    }

}
