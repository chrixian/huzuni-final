package net.halalaboos.huzuni.events.render;

import com.darkmagician6.eventapi.events.Event;

public class EventPreRenderWorld implements Event {

    public final float partialTicks;

    public EventPreRenderWorld(float partialTicks) {
        this.partialTicks = partialTicks;
    }

    public double interpolate(double prev, double cur) {
        return prev + ((cur - prev) * partialTicks);
    }
}
