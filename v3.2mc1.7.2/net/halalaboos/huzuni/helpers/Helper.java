package net.halalaboos.huzuni.helpers;

/**
 * Simple interface used to get the instance of an helper.
 */
public interface Helper {
	GameHelper gameHelper = new GameHelper();
    StringHelper stringHelper = new StringHelper();
    HuzuniHelper huzuniHelper = new HuzuniHelper();
    EntityHelper entityHelper = new EntityHelper();
    BlockHelper blockHelper = new BlockHelper();
    InventoryHelper inventoryHelper = new InventoryHelper();
}
