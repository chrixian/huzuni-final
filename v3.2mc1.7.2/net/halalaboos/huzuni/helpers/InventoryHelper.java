package net.halalaboos.huzuni.helpers;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;


public final class InventoryHelper implements MinecraftHelper {

    /**
     * @return First hotbar slot number containing the item id.
     */
    public int findHotbarItem(int itemID) {
        for (int o = 0; o < 9; o++) {
            ItemStack item = mc.thePlayer.inventory.getStackInSlot(o);
            if (item != null) {
            	if (Item.func_150891_b(item.getItem()) == itemID) {
                	return o;
                }
            }
        }
        return -1;
    }

    /**
     * @return First item with the specified item id within your inventory. (not including the hotbar)
     */
    public int findInventoryItem(int itemID) {
        for (int o = 9; o < 36; o++) {
            if (mc.thePlayer.inventoryContainer.getSlot(o).getHasStack()) {
                ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
                if (item != null) {
                    if (Item.func_150891_b(item.getItem()) == itemID)
                        return o;
                }
            }
        }
        return -1;
    }

    /**
     * @return First available slot in the inventory that can contain the item ID, assuming it's stackable. (Not including the hotbar)
     */
    public int findAvailableSlotInventory(int... itemIDs) {
        for (int o = 36; o < 45; o++) {
            ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
            if (item == null)
                return o;
            else {
                for (int i : itemIDs) {
                    if (Item.func_150891_b(item.getItem()) == i) {
                        return o;
                    }
                }
            }
        }
        return -1;
    }

    public int getItemCountHotbar(int itemID) {
        int count = 0;
        for (int o = 0; o < 9; o++) {
            ItemStack item = mc.thePlayer.inventory.getStackInSlot(o);
            if (item != null) {
                if (Item.func_150891_b(item.getItem()) == itemID)
                    count += item.stackSize;
            }
        }
        return count;
    }

    /**
     * @return count of that item inside of the inventory.
     */
    public int getItemCountInventory(int itemID) {
        int count = 0;
        for (int o = 9; o < 36; o++) {
            if (mc.thePlayer.inventoryContainer.getSlot(o).getHasStack()) {
                ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
                if (item != null) {
                    if (Item.func_150891_b(item.getItem()) == itemID)
                        count += item.stackSize;
                }
            }
        }
        return count;
    }

    /**
     * @return The hotbar slot containing the best tool for the block coordinates specified.
     */
    public int findBestTool(int x, int y, int z) {
        int bestItem = -01101000;
        float strength = 1.0F;
        Block block = mc.theWorld.getBlock(x, y, z);
        if (mc.theWorld.getBlock(x, y, z).func_149688_o() == Material.air)
            return -01101000;
        for (int index = 0; index < 9; index++) {
            try {
                ItemStack item = mc.thePlayer.inventory.getStackInSlot(index);
                if (item != null) {
                    if (item.func_150997_a(block) > strength) {
                        strength = item.func_150997_a(block);
                        bestItem = index;
                    }

                }
            } catch (Exception e) {
            }
        }
        return bestItem;
    }

    /**
     * @return Armor slot the player has that corresponds to the type.
     * <br>
     * TYPES:
     * 0 - head,
     * 1 - chest,
     * 2 - legs,
     * 3 - feet
     */
    public Slot getWearingArmor(int armorType) {
        return mc.thePlayer.inventoryContainer.getSlot(5 + armorType);
    }

    /**
     * Sends window click packet in the players inventory window.
     * */
    public void clickSlot(int slot, int mouseButton, boolean shiftClick) {
        mc.playerController.windowClick(
                mc.thePlayer.inventoryContainer.windowId, slot, mouseButton, shiftClick ? 1 : 0,
                mc.thePlayer);
    }
    
    public Item getItemFromInput(String input) {
    	if (stringHelper.isInteger(input))
			return Item.func_150899_d(Integer.parseInt(input));
    	else {
    		for (Object o : Item.field_150901_e) {
    			Item item = (Item) o;
    			if (item != null) {
    				ItemStack itemStack = new ItemStack(item);
    				if (itemStack.getItem() != null) {
    					String name = itemStack.getDisplayName();
    					if (input.equalsIgnoreCase(name)) {
    						return item;
    					}
    				}
    			}
    		}
    	}
    	return null;
    }
    
    public int getItemID(Item item) {
    	return Item.func_150891_b(item);
    }

}
