package net.halalaboos.huzuni.hooks;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import in.brud.huzuni.ui.window.ChatWindow;

import com.darkmagician6.eventapi.Dispatcher;
import com.darkmagician6.eventapi.events.premade.EventCancellable;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.events.game.EventRecieveMessage;
import net.halalaboos.lib.ui.GlobalContainer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiNewChat;
import net.minecraft.util.IChatComponent;

public class GuiNewChatHook extends GuiNewChat {

	private final Minecraft mc;
				
	public GuiNewChatHook(Minecraft mc) {
		super(mc);
		this.mc = mc;
	}

	// Message recieved
	@Override
	public void func_146234_a(IChatComponent chatComp, int time) {
		String message = chatComp.func_150254_d();
		EventCancellable event = new EventRecieveMessage(message);
		Dispatcher.call(event);
		if (!event.isCancelled()) {
			Huzuni.windowManager.getChatWindow().onMessageRecieve(message);
			super.func_146234_a(chatComp, time);
		}
	}
	
	// Render
	@Override
	public void func_146230_a(int p_146230_1_) {
		if (Settings.isCustomChat() && Settings.isHuzuniEnabled()) {
			setupOverlayDefaultScale();
			Huzuni.windowManager.getIngameManager().render();
	        mc.entityRenderer.setupOverlayRendering();
		} else {
			super.func_146230_a(p_146230_1_);
		}
    }
	

    private void setupOverlayDefaultScale() {
        double scale = 2;
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0.0D, mc.displayWidth / scale, mc.displayHeight / scale, 0.0D, 1000.0D, 3000.0D);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glLoadIdentity();
        GL11.glTranslatef(0.0F, 0.0F, -2000.0F);
        GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight);
    }

	
	// Mouse click
	@Override
	public IChatComponent func_146236_a(int x, int y) {
		if (Settings.isCustomChat() && Settings.isHuzuniEnabled() && Mouse.next() && Mouse.getEventButtonState()) {
			Huzuni.windowManager.getIngameManager().onMousePressed((int) ((Mouse.getEventX() * (mc.displayWidth / 2) / mc.displayWidth)), (((mc.displayHeight / 2) - Mouse.getEventY() * (mc.displayHeight / 2) / mc.displayHeight)) - 1, 0);
			return null;
		} else {
			return super.func_146236_a(x, y);
		}
	}
}
