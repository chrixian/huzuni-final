/**
 * 
 */
package net.halalaboos.huzuni.mods.misc;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.console.Command;
import net.halalaboos.huzuni.events.game.EventTick;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.Module;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.morbidlib.utils.timer.AccurateTimer;
import com.darkmagician6.morbidlib.utils.timer.Timer;

/**
 * @author Halalaboos
 *
 * @since Sep 21, 2013
 */
public class AntiAFK extends Module implements Listener {
	
	private final Timer timer = new AccurateTimer();
	
	private double time = 8000D;
	
	/**
	 * @param name
	 * @param author
	 */
	public AntiAFK() {
		super("Anti AFK", "Halalaboos");
		setDescription("Automagically jumps when you reach a time threshold.");
		registerCommand(new Command() {

			@Override
			public String[] getAliases() {
				return new String[] { "antiafk", "afk" };
			}

			@Override
			public String[] getHelp() {
				return new String[] { "antiafk set <time>" };
			}

			@Override
			public String getDescription() {
				return "Adjust the antiafk module.";
			}

			@Override
			public void run(String input, String[] args) {
				if (args[0].equalsIgnoreCase("set")) {
					if (Helper.stringHelper.isDouble(args[1])) {
						double newTime = Double.parseDouble(args[1]);
						time = newTime * 1000L;
						huzuni.addChatMessage("AntiAFK auto-jumping time set to " + newTime + " seconds.");
					} else
						huzuni.addChatMessage("Improper syntax. EX: .antiafk set 5");
				} else
					huzuni.addChatMessage("Improper syntax. EX: .antiafk set 5");
			}
			
		});
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Module#onEnable()
	 */
	@Override
	protected void onEnable() {
		registry.registerListener(this);
		Huzuni.instance.addChatMessage("Type '.antiafk' to configure the timing.");
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Module#onDisable()
	 */
	@Override
	protected void onDisable() {
		registry.unregisterListener(this);
	}

	@EventTarget
	public void onTick(EventTick event) {
		if (timer.hasReach((float) time)) {
			mc.thePlayer.motionY += 0.45F;
			timer.reset();
		}
	}
}
