/**
 * 
 */
package net.halalaboos.huzuni.mods.misc;

import net.halalaboos.huzuni.events.game.EventSendMessage;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.VipModule;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

/**
 * @author Halalaboos
 *
 */
public class DolanSpeak extends VipModule implements Listener {

	private final String[][] dolanRegex = new String[][] {
			{"uck", "uk"},
			{"ck", "k"},
			{"cra", "qwa"},
			{"cro", "qwo"},
			{"ing", "in"},
			{"you", "u"},
            {"aw", "ow"},
            {" i ", " me "},
            {"what"}, {"wut"},
            {"ith", "if"},
            {"ie", "ei"},
            {"ng", "n"},
            {" is ", " am "},
			{"rr", "r"},
			{"per", "pa"},
			{"th", "d"},
			{"akes", "ax"},
			{"cks", "x"},
			{"anks", "anx"},
			{"ice", "is"},
			{"ll", "l"},
			{"ance", "ans"},
			{"are", "r"},
			{"ou", "u"},
			{"nna", "na"},
			{"ell", "el"},
			{"what", "wat"},
			{"eath", "edd"},
			{"one", "1"},
			{"ere", "ire"},
			{"grief", "greif"},
            {"my", "me"},
            {"the", "da"}
	};
	
	/**
	 * @param name
	 * @param author
	 */
	public DolanSpeak() {
		super("Dolan Speak", "Halalaboos");
		setCategory(Category.MISC);
		setDescription("Gives you the powers of the gods.");
		
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Module#onEnable()
	 */
	@Override
	protected void onEnable() {
		registry.registerListener(this);
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Module#onDisable()
	 */
	@Override
	protected void onDisable() {
		registry.unregisterListener(this);
	}
	
	@EventTarget
	public void onSendMessage(EventSendMessage event) {
		if (!event.getMessage().startsWith("/")) {
			event.setMessage(toDolanSpeak(event.getMessage()));
			/*for (String[] regex : dolanRegex) {
				event.setMessage(event.getMessage().replaceAll("(?i)" + regex[0], regex[1]));
			}*/
		}
	}
	
	/**
	 * Old really bad method we used in poohbear to convert your text to dolan-esque text.
	 * */
	private String toDolanSpeak(String s) {
		if (s.startsWith("/"))
			return s;

		s = s.replaceAll("(?i) and ", " & ")
				.replaceAll("(?i) are ", " r ").replaceAll("(?i)that", "tht")
				.replaceAll("(?i)this", "dis").replaceAll("(?i)the", "da")
				.replaceAll("(?i)er ", "a ").replaceAll("(?i)er", "ir")
				.replaceAll("(?i)eou", "u").replaceAll("(?i)se", "s")
				.replaceAll("(?i)ce", "s").replaceAll("(?i)ci", "s")
				.replaceAll("(?i)ai", "a").replaceAll("(?i)ie", "ei")
				.replaceAll("(?i)cr", "kr").replaceAll("(?i)ft", "f")
				.replaceAll("(?i)ke", "k").replaceAll("(?i)kr", "qw")
				.replaceAll("(?i)ll", "l").replaceAll("(?i)what", "wat")
				.replaceAll("(?i)aw", "ow").replaceAll("(?i) i ", " me ")
				.replaceAll("(?i) am ", " is ").replaceAll("(?i)cks", "x")
				.replaceAll("(?i)ks", "x").replaceAll("(?i)ck", "k")
				.replaceAll("(?i)ng", "n").replaceAll("(?i)mb", "m")
				.replaceAll("(?i)ot", "it").replaceAll("(?i)nn", "n")
				.replaceAll("(?i)gh ", " ").replaceAll("(?i)check", "czech")
				.replaceAll("(?i)ou", "u").replaceAll("(?i)es ", "s ")
				.replaceAll("(?i)your", "ur").replaceAll("(?i)jesus", "jebus")
				.replaceAll("(?i)ith", "if").replaceAll("(?i)ph", "f");
		return s;
	}


}
