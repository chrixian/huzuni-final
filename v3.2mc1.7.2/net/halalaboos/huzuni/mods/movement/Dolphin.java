package net.halalaboos.huzuni.mods.movement;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.events.game.EventPreMotionUpdate;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.minecraft.block.material.Material;

public class Dolphin extends DefaultModule implements Listener {

    public Dolphin() {
        super("Dolphin", -1);
        setCategory(Category.MOVEMENT);
        setDescription("Swims for you in the water.");
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);

    }

    @Override
    protected void onToggle() {
    }

    @EventTarget
    public void onPreMotionUpdate(EventPreMotionUpdate event) {
        if ((mc.thePlayer.isInWater() || mc.thePlayer.isInsideOfMaterial(Material.lava)) && !mc.gameSettings.keyBindSneak.func_151470_d() && !mc.gameSettings.keyBindJump.func_151470_d()) {
            mc.thePlayer.motionY += 0.039;
        }
    }

}
