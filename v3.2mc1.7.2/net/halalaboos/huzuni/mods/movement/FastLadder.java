package net.halalaboos.huzuni.mods.movement;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.events.game.EventMovement;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.VipModule;

public class FastLadder extends VipModule implements Listener {

	public FastLadder() {
		super("Fast Ladder", "Tyga");
		setDescription("Makes you climb ladders faster.");
		setCategory(Category.MOVEMENT);
	}

	@Override
	protected void onEnable() {
		registry.registerListener(this);
	}

	@Override
	protected void onDisable() {
		registry.unregisterListener(this);
	}

    @EventTarget
    public void onPlayerMove(EventMovement event) {
        float multiplier = 2F;
        if(mc.thePlayer.isOnLadder()) {
            event.motionY *= multiplier;
        }

    }
}
