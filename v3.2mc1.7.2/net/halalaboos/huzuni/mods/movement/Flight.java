package net.halalaboos.huzuni.mods.movement;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.events.game.EventMovement;
import net.halalaboos.huzuni.events.game.EventPreMotionUpdate;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.lib.io.config.ConfigListener;
import net.halalaboos.lib.io.config.DefaultConfigListener;

import org.lwjgl.input.Keyboard;

public class Flight extends DefaultModule implements Listener {

	private DefaultConfigListener<Float> speed = new DefaultConfigListener<Float>("Flight Speed", 2F);
	
    public Flight() {
        super("Flight", Keyboard.KEY_F);
        setCategory(Category.MOVEMENT);
        setDescription("Allows you to fly.");
        Settings.addListener(speed);
        Settings.put("Flight Speed", 2F);
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @Override
    protected void onToggle() {
        if (isEnabled())
            mc.thePlayer.capabilities.isFlying = true;
        else
            mc.thePlayer.capabilities.isFlying = false;
    }

    @EventTarget
    public void onPreMotionUpdate(EventPreMotionUpdate event) {
        mc.thePlayer.capabilities.isFlying = true;
        if (mc.thePlayer.fallDistance > 3)
            mc.thePlayer.onGround = true;
    }

    @EventTarget
    public void onPlayerMove(EventMovement event) {
        int multiplier = Keyboard.isKeyDown(Keyboard.KEY_LCONTROL) ? 2 : 1;
    	event.motionX *= speed.getValue() * multiplier;
        event.motionY *= speed.getValue() * multiplier;
        event.motionZ *= speed.getValue() * multiplier;
    }

}
