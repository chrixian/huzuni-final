package net.halalaboos.huzuni.mods.player;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.events.game.EventTick;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.lib.io.config.ConfigListener;
import net.halalaboos.lib.io.config.DefaultConfigListener;

public class Fastplace extends DefaultModule implements Listener {

	private DefaultConfigListener<Float> speed = new DefaultConfigListener<Float>("Fastplace Speed", 2F);
	
    public Fastplace() {
        super("Fastplace", -1);
        setCategory(Category.PLAYER);
        setDescription("Place blocks faster.");
        Settings.addListener(speed);
        Settings.put("Fastplace Speed", 4F);
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @Override
    protected void onToggle() {
    }

    @EventTarget
    public void onTick(final EventTick event) {
    	float speed = this.speed.getValue();
        if (mc.rightClickDelayTimer > (4 - (byte) speed)) {
            mc.rightClickDelayTimer = (4 - (byte) speed);
        }
    }

}
