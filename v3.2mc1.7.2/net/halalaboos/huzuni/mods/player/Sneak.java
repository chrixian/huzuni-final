package net.halalaboos.huzuni.mods.player;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.events.game.EventPreMotionUpdate;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;

import org.lwjgl.input.Keyboard;

import java.util.Random;

public class Sneak extends DefaultModule implements Listener {
    private final Random random = new Random();

    public Sneak() {
        super("Sneak", Keyboard.KEY_Z);
        setCategory(Category.PLAYER);
        setDescription("Automatically sneaks for you.");
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
        mc.gameSettings.keyBindSneak.pressed = false;
    }

    @Override
    protected void onToggle() {
    }

    @EventTarget
    public void onPreMotionUpdate(EventPreMotionUpdate event) {
        mc.gameSettings.keyBindSneak.pressed = true;
    }
}
