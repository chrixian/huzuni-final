package net.halalaboos.huzuni.mods.player;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.events.game.EventBreakBlock;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.lib.io.config.ConfigListener;
import net.halalaboos.lib.io.config.DefaultConfigListener;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import org.lwjgl.input.Keyboard;

public class Speedmine extends DefaultModule implements Listener {

	private DefaultConfigListener<Float> speed = new DefaultConfigListener<Float>("Speedmine Speed", 0.2F);
	
    public Speedmine() {
        super("Speedmine", Keyboard.KEY_V);
        setCategory(Category.PLAYER);
        setDescription("Mine blocks faster.");
        Settings.addListener(speed);
        Settings.put("Speedmine Speed", 0.2F);
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @Override
    protected void onToggle() {
    }

    @EventTarget
    public void onBreakBlock(EventBreakBlock event) {
        Block block = mc.theWorld.getBlock(event.x, event.y, event.z);
        if (block.func_149688_o() == Material.air)
            return;
        event.setCurrentBlockDamage(event.getCurrentBlockDamage() + (block.func_149737_a(this.mc.thePlayer, this.mc.thePlayer.worldObj, event.x, event.y, event.z) * speed.getValue()));
        event.setBlockHitDelay(0);
    }

}
