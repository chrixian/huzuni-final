/**
 * 
 */
package net.halalaboos.huzuni.mods.pvp;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.events.game.EventPostMotionUpdate;
import net.halalaboos.huzuni.events.game.EventPreMotionUpdate;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.lib.io.config.DefaultConfigListener;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.StringUtils;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.eventapi.types.Priority;
import com.darkmagician6.morbidlib.utils.timer.AccurateTimer;
import com.darkmagician6.morbidlib.utils.timer.Timer;

/**
 * @author Halalaboos
 *
 * @since Nov 23, 2013
 */
public class TriggerBot extends DefaultModule implements Listener {
    
	private final Timer timer = new AccurateTimer();

	private boolean hit = false;
	
	private DefaultConfigListener<Float> speed = new DefaultConfigListener<Float>("Kill Aura Speed", 8F);
	private DefaultConfigListener<Float> reach = new DefaultConfigListener<Float>("Kill Aura Reach", 3.8F);
	
	/**
	 * @param name
	 * @param keyCode
	 */
	public TriggerBot() {
		super("Trigger Bot");
		setDescription("Attacks entity you're looking at.");
		setCategory(Category.PVP);
		Settings.addListener(speed);
		Settings.addListener(reach);
	}

	/**
	 * @see net.halalaboos.huzuni.mods.DefaultModule#onToggle()
	 */
	@Override
	protected void onToggle() {
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Module#onEnable()
	 */
	@Override
	protected void onEnable() {
		registry.registerListener(this);
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Module#onDisable()
	 */
	@Override
	protected void onDisable() {
		registry.unregisterListener(this);
	}
	
    
    @EventTarget(Priority.LOWEST)
    public void onPreMotionUpdate(EventPreMotionUpdate event) {
    	Entity entity = mc.objectMouseOver.entityHit;
    	if (entity instanceof EntityLivingBase) {
    		EntityLivingBase entityLiving = (EntityLivingBase) entity;
    		if (entity instanceof EntityPlayer)
                if (Huzuni.friendManager.contains(StringUtils.stripControlCodes(entity.getCommandSenderName())))
                    return;
	    	if (isHittableNotNull(entityLiving, true))
	            entityHelper.faceEntity(entityLiving);
	        if (isHittableNotNull(entityLiving, false)) {
	            mc.thePlayer.setSprinting(false);
	            hit = true;
	        } else
	        	hit = false;
    	} else
    		hit = false;
    }

    @EventTarget(Priority.HIGHEST)
    public void onPostMotionUpdate(EventPostMotionUpdate event) {
    	if (hit) {
            if (timer.hasReach(1000F / getSpeed())) {
            	Entity entity = mc.objectMouseOver.entityHit;
            	mc.thePlayer.swingItem();
                mc.playerController.attackEntity(mc.thePlayer, entity);
                timer.reset();
            }
        }
    }
    
    /**
     * @return Reach distance for triggerbot.
     */
    public final float getReach() {
        return reach.getValue();
    }

    /**
     * @return Kill Aura speed in attacks per second.
     */
    public final float getSpeed() {
        return speed.getValue();
    }
    
    /**
     * @return True if the entity is alive and within distance.
     */
    public boolean isHittableNotNull(EntityLivingBase entity, boolean justLook) {
        return entityHelper.isAliveNotUs(entity) && entityHelper.isWithinDistance(entity, justLook ? getReach() + 2.5F : getReach()) && mc.thePlayer.canEntityBeSeen(entity);
    }
}
