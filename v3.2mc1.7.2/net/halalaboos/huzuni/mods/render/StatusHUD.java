/**
 *
 */
package net.halalaboos.huzuni.mods.render;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.events.render.EventRenderGui;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.mods.Module;
import net.halalaboos.huzuni.utils.TextureUtils;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import java.util.Collection;

/**
 * @author Halalaboos
 * @since Sep 2, 2013
 */
public class StatusHUD extends Module implements Listener, Helper {

    private final RenderItem itemRenderer = new RenderItem();

    protected static final ResourceLocation inventory = new ResourceLocation("textures/gui/container/inventory.png");

    /**
     * @param name
     */
    public StatusHUD() {
        super("Status HUD", "Halalaboos");
        setDescription("Render your armor and potion effects in-game.");
    }

    /**
     * @see net.halalaboos.huzuni.mods.Module#onEnable()
     */
    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    /**
     * @see net.halalaboos.huzuni.mods.Module#onDisable()
     */
    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @EventTarget
    public void render(EventRenderGui event) {
        int left = gameHelper.getScreenWidth() / 2 - 110,
                right = left + 9 * 20 + 20,
                top = gameHelper.getScreenHeight() - 39,
                bottom = top + 20;
        renderItemStack(inventoryHelper.getWearingArmor(0).getStack(), left, top);
        renderItemStack(inventoryHelper.getWearingArmor(1).getStack(), left, bottom);
        renderItemStack(inventoryHelper.getWearingArmor(2).getStack(), right, top);
        renderItemStack(inventoryHelper.getWearingArmor(3).getStack(), right, bottom);

        int increment = 20;
        int renderY = gameHelper.getScreenHeight() - increment;

        for (PotionEffect potionEffect : (Collection<PotionEffect>) mc.thePlayer.getActivePotionEffects()) {
            Potion potion = Potion.potionTypes[potionEffect.getPotionID()];
            String amplifier = "";
            if (potionEffect.getAmplifier() == 1) {
                amplifier = " II";
            } else if (potionEffect.getAmplifier() == 2) {
                amplifier = " III";
            } else if (potionEffect.getAmplifier() == 3) {
                amplifier = " IV";
            }
            String renderText = I18n.getStringParams(potionEffect.getEffectName()) + " " + amplifier;

            int renderX = gameHelper.getScreenWidth() - mc.fontRenderer.getStringWidth(renderText) - 2;

            if (potion.hasStatusIcon()) {
                int iconIndex = potion.getStatusIconIndex();
                mc.getTextureManager().bindTexture(inventory);
                GL11.glColor3f(1, 1, 1);
                TextureUtils.renderTexture(256, 256, gameHelper.getScreenWidth() - 20, renderY - 6, 18, 18, 0 + iconIndex % 8 * 18, 198 + iconIndex / 8 * 18, 18, 18);
                renderX -= 20;
            }

            mc.fontRenderer.drawString(renderText, renderX, renderY - 5, 0xFFFFFF);
            mc.fontRenderer.drawString(Potion.getDurationString(potionEffect), gameHelper.getScreenWidth() - mc.fontRenderer.getStringWidth(Potion.getDurationString(potionEffect)) - (potion.hasStatusIcon() ? 20 : 2), renderY + 5, 0xFFFFFF);
            renderY -= increment;
        }

    }

    private void renderItemStack(ItemStack item, int x, int y) {
        if (item != null) {
            GL11.glPushMatrix();
            GL11.glDisable(GL11.GL_BLEND);
            RenderHelper.enableGUIStandardItemLighting();
            GL11.glEnable(GL12.GL_RESCALE_NORMAL);
            itemRenderer.zLevel = 200.0F;
            itemRenderer.renderItemIntoGUI(mc.fontRenderer, this.mc.getTextureManager(), item, x + 2, y + 2);
            itemRenderer.renderItemOverlayIntoGUI(this.mc.fontRenderer, this.mc.getTextureManager(), item, x + 2, y + 2);
            itemRenderer.zLevel = 0.0F;
            RenderHelper.disableStandardItemLighting();
            GL11.glDisable(GL12.GL_RESCALE_NORMAL);
            GL11.glPopMatrix();
        }
    }

}
