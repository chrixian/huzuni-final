/**
 * 
 */
package net.halalaboos.huzuni.mods.world;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.morbidlib.utils.timer.AccurateTimer;
import com.darkmagician6.morbidlib.utils.timer.Timer;

import net.halalaboos.huzuni.events.game.EventPlaceBlock;
import net.halalaboos.huzuni.events.game.EventTick;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.VipDefaultModule;
import net.minecraft.block.Block;
import net.minecraft.block.BlockChest;
import net.minecraft.client.gui.inventory.GuiChest;
import net.minecraft.inventory.ContainerChest;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityChest;

/**
 * @author Halalaboos
 *
 */
public class ChestStealer extends VipDefaultModule implements Listener {
	
	protected final Timer timer = new AccurateTimer();
	
	protected GuiChest guiChest;
	
	protected IInventory chest;
	
	protected int windowId, index;
	
	/**
	 * @param name
	 */
	public ChestStealer() {
		super("Chest Stealer");
		setDescription("Steals everything from chests when you open them.");
	}

	/**
	 * @see net.halalaboos.huzuni.mods.DefaultModule#onToggle()
	 */
	@Override
	protected void onToggle() {
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Module#onEnable()
	 */
	@Override
	protected void onEnable() {
		registry.registerListener(this);
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Module#onDisable()
	 */
	@Override
	protected void onDisable() {
		registry.unregisterListener(this);
	}

	@EventTarget
	public void onTick(EventTick event) {
		if (guiChest != null && chest != null) {
			if (timer.hasReach(125L)) {
				for (; index < chest.getSizeInventory(); index++) {
					ItemStack item = chest.getStackInSlot(index);
					if (item == null)
						continue;
					clickSlot(windowId, index, true);
					timer.reset();
					index++;
					return;
				}
				mc.displayGuiScreen(null);
				chest = null;
				guiChest = null;
			}
		} else {
			if (mc.currentScreen instanceof GuiChest) {
				guiChest = (GuiChest) mc.currentScreen;
				chest = ((ContainerChest) guiChest.field_147002_h).getLowerChestInventory();
				index = 0;
				windowId = ((ContainerChest) guiChest.field_147002_h).windowId;
			}
		}
	}
	
	private void clickSlot(int windowId, int slot, boolean shiftClick) {
        mc.playerController.windowClick(windowId, slot, 0, shiftClick ? 1 : 0, mc.thePlayer);
    }
}
