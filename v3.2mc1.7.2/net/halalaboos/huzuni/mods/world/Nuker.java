package net.halalaboos.huzuni.mods.world;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.eventapi.types.Priority;

import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.console.Command;
import net.halalaboos.huzuni.events.game.EventBreakBlock;
import net.halalaboos.huzuni.events.game.EventClick;
import net.halalaboos.huzuni.events.game.EventPlaceBlock;
import net.halalaboos.huzuni.events.game.EventPostMotionUpdate;
import net.halalaboos.huzuni.events.game.EventPreMotionUpdate;
import net.halalaboos.huzuni.events.render.EventRenderWorld;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.utils.RenderUtils;
import net.halalaboos.lib.io.config.DefaultConfigListener;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C0APacketAnimation;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.MovingObjectPosition.MovingObjectType;
import net.minecraft.util.Vec3;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;

import static org.lwjgl.opengl.GL11.*;

public class Nuker extends DefaultModule implements Listener, Helper {

    // Holds our current block's coordinates.
    private Vector3f blockCoordinates;

    // Our current damage on a block.
    private float currentBlockDamage;

    // Delay between hitting blocks.
    private byte blockHitDelay = 0;

    // Selected block to nuke.
    private Block selectedBlock = null;

    private int side;

    private int mode = 0;
    
    private int blockID = 0;

	private DefaultConfigListener<Float> speed = new DefaultConfigListener<Float>("Nuker Speed", 3F);

	private DefaultConfigListener<Float> radius = new DefaultConfigListener<Float>("Nuker Radius", 4F);

    public Nuker() {
        super("Nuker", Keyboard.KEY_L);
        setCategory(Category.WORLD);
        setDescription("Nukes blocks around you.");
        Settings.put("Nuker Radius", 4F);
        Settings.addListener(radius);
        Settings.put("Nuker Speed", 3F);
        Settings.addListener(speed);

        this.registerCommand(new Command() {

            @Override
            public String[] getAliases() {
                return new String[] {"nuker", "nucker", "nuk"};
            }

            @Override
            public String[] getHelp() {
                return new String[] {"nuker mode <instant / click / id>"};
            }

            @Override
            public String getDescription() {
                return "Changes the nuker modes.";
            }

            @Override
            public void run(String input, String[] args) {
                if (args[0].startsWith("m")) {
                    if (args[1].startsWith("in")) {
                        mode = 1;
                        huzuni.addChatMessage("Mode set to instant.");
                    } else if (args[1].startsWith("c")) {
                        mode = 2;
                        huzuni.addChatMessage("Mode set to click.");

                    } else if (args[1].startsWith("id")) {
                        mode = 0;
                        huzuni.addChatMessage("Mode set to id.");
                    }
                }
            }

        });
    }

    @Override
    public String getRenderName() {
        return super.getRenderName() + " " + getMode();
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
        huzuni.addChatMessage("Type '.nuker' for the nuker settings.");
        if (mode == 0)
        	huzuni.addChatMessage("Right click a block to select it and begin nuking!");
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @Override
    protected void onToggle() {
    	selectedBlock = null;
    	blockID = 0;
    }

    @EventTarget
    public void onPlaceBlock(EventPlaceBlock event) {
    	if (mode == 0) {
    		Block block = mc.theWorld.getBlock(event.x, event.y, event.z);
            this.selectedBlock = block;
            blockID = blockHelper.getBlockID(block);
            event.setCancelled(true);
    	}
    }
    
    @EventTarget
    public void onBreakBlock(EventBreakBlock event) {
    	if (mode == 2) {
    		nuke(event.x, event.y, event.z);
    	}
    }

    @EventTarget(Priority.HIGH)
    public void onRender3D(EventRenderWorld event) {
        if (mode != 0)
            return;
        // If we're nuking a block, we'll render a box on it.
        if (this.blockCoordinates != null) {
            double renderX = blockCoordinates.x - RenderManager.renderPosX;
            double renderY = blockCoordinates.y - RenderManager.renderPosY;
            double renderZ = blockCoordinates.z - RenderManager.renderPosZ;
            glPushMatrix();
            AxisAlignedBB boundingBox = AxisAlignedBB.getBoundingBox(renderX, renderY, renderZ, renderX + 1, renderY + (1 - currentBlockDamage), renderZ + 1);
            glColor4f(1 - currentBlockDamage, currentBlockDamage, 0, 0.2F);
            RenderUtils.drawBox(boundingBox);
            glColor4f(0, 0, 0, 0.3F);
            RenderUtils.drawOutlinedBox(boundingBox);
            glPopMatrix();
        }
    }

    @EventTarget(Priority.LOW)
    public void onPreMotionUpdate(EventPreMotionUpdate event) {
        // If we're on instant mode, INSTANTLY break the blocks. Otherwise check if we're using ID mode and return if we're not.
        if (mode == 1) {
            nuke(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ);
            return;
        } else if (mode != 0)
            return;
        if (isAir(selectedBlock))
        	return;
        // Defined up here so we're not accessing stuff too much.
        // --
        int radius = (int) getRadius();
        int shouldSkip = shouldSkipCurrentBlock(blockCoordinates, selectedBlock);
        // --

        // If we're not supposed to be nuking the current block
        if (shouldSkip != 0) {
            // If we also need to notify the server about us no longer breaking the block. (This sends the stop-in-the-middle break packet.)
            if (shouldSkip == 3 && currentBlockDamage != 0 && !mc.playerController.isInCreativeMode()) {
                sendBreakPacket(blockCoordinates, 1, -1);
                blockCoordinates = null;
            }
            // Find a new block.
            this.blockCoordinates = find(selectedBlock);
            currentBlockDamage = 0;
        } else {
            // Face the block!
            blockHelper.faceBlock(blockCoordinates.x + 0.5F, blockCoordinates.y + 0.5F, blockCoordinates.z + 0.5F);
        }
    }

    @EventTarget
    public void onPostMotionUpdate(EventPostMotionUpdate event) {
        // If we're not on ID nuking mode, we skip this.
        if (mode != 0)
            return;
        
        if (isAir(selectedBlock))
        	return;
        // Stop us from nuking TOO much..
        if (blockHitDelay > 0) {
            blockHitDelay--;
            return;
        }
        int shouldSkip = shouldSkipCurrentBlock(blockCoordinates, selectedBlock);
        if (shouldSkip == 0) {
            mc.thePlayer.sendQueue.addToSendQueue(new C0APacketAnimation(mc.thePlayer, 1));
            nukeSelectedBlock(blockCoordinates, mc.playerController.isInCreativeMode());
        }
    }

    private void nukeSelectedBlock(Vector3f selectedBlock, boolean isCreativeMode) {
        if (currentBlockDamage == 0) {
            sendBreakPacket(selectedBlock, 0, side);
            if (isCreativeMode || this.selectedBlock.func_149737_a(mc.thePlayer, mc.theWorld, (int) selectedBlock.x, (int) selectedBlock.y, (int) selectedBlock.z) >= 1) {
            	mc.theWorld.func_147468_f((int) selectedBlock.x, (int) selectedBlock.y, (int) selectedBlock.z);
            	// mc.playerController.onPlayerDestroyBlock((int) selectedBlock.x, (int) selectedBlock.y, (int) selectedBlock.z, side);
                currentBlockDamage = 0;
                if (isCreativeMode)
                    blockHitDelay = (byte) (5 - getSpeed());
                return;
            }
        }

        // Add onto the damage
        currentBlockDamage += this.selectedBlock.func_149737_a(mc.thePlayer, mc.theWorld, (int) selectedBlock.x, (int) selectedBlock.y, (int) selectedBlock.z);
        mc.theWorld.func_147443_d(this.mc.thePlayer.func_145782_y(), (int) selectedBlock.x, (int) selectedBlock.y, (int) selectedBlock.z, (int) (this.currentBlockDamage * 10.0F) - 1);

        // If we've broken the block
        if (currentBlockDamage >= 1) {
            // FINISH HIM
            sendBreakPacket(selectedBlock, 2, side);
        	mc.theWorld.func_147468_f((int) selectedBlock.x, (int) selectedBlock.y, (int) selectedBlock.z);
            // mc.playerController.onPlayerDestroyBlock((int) selectedBlock.x, (int) selectedBlock.y, (int) selectedBlock.z, side);
            blockHitDelay = (byte) (5 - getSpeed());
        }
    }

    /**
     * Nukes the specific blocks around the position specified.
     */
    private void nuke(double posX, double posY, double posZ, int... blocks) {
        int radius = (int) getRadius();
        for (int i = radius; i >= -radius; i--) {
            for (int j = -radius; j <= radius; j++) {
                for (int k = radius; k >= -radius; k--) {
                    int possibleX = (int) (posX + i),
                            possibleY = (int) (posY + j),
                            possibleZ = (int) (posZ + k);
                    Block block = mc.theWorld.getBlock(possibleX, possibleY, possibleZ);
                    Vector3f blockVector = new Vector3f(possibleX, possibleY, possibleZ);
                    // If the block at the position is not air
                    if (!isAir(block) && canReach(blockVector, radius)) {
                    	nuke(blockVector, 0);
                    }
                }
            }
        }
    }

    /**
     * Finds blocks around the player that is the block.
     */
    private Vector3f find(Block wantedBlockID) {
        int radius = (int) getRadius();
        Vector3f finalVector = null;
        side = 0;
        // 3D Loop.
        for (int i = radius; i >= -radius; i--) {
            for (int j = -radius; j <= radius; j++) {
                for (int k = radius; k >= -radius; k--) {
                    int possibleX = (int) (mc.thePlayer.posX + i),
                            possibleY = (int) (mc.thePlayer.posY + j),
                            possibleZ = (int) (mc.thePlayer.posZ + k);
                    Block block = mc.theWorld.getBlock(possibleX, possibleY, possibleZ);
                    Vector3f blockVector = new Vector3f(possibleX, possibleY, possibleZ);
                    // If the block at the position is not air
                    if (!isAir(block) && canReach(blockVector, radius)) {
                        if (block == wantedBlockID) {
                            MovingObjectPosition result = rayTrace(blockVector);
                            // We'll compare the vectors and find the closest vector to the player.
                            if (finalVector == null) {
                                finalVector = blockVector;
                                side = result != null ? result.sideHit : 1;
                            } else if (blockHelper.getDistance(blockVector) < blockHelper.getDistance(finalVector)) {
                                finalVector = blockVector;
                                side = result != null ? result.sideHit : 1;
                            }
                        }
                    }
                }
            }
        }
        return finalVector;
    }

    /**
     * Completely obliterates the block at the position.
     */
    private void nuke(Vector3f position, int side) {
        sendBreakPacket(position, 0, side);
        if (mc.playerController.isNotCreative())
            sendBreakPacket(position, 2, side);
    	mc.theWorld.func_147468_f((int) position.x, (int) position.y, (int) position.z);
        // mc.playerController.onPlayerDestroyBlock((int) position.x, (int) position.y, (int) position.z, side);
    }

    /**
     * @return 3 if you cannot reach, 2 if null, 1 if the block id has changed or the block is no longer there, 0 if everythings ok.
     */
    private byte shouldSkipCurrentBlock(Vector3f block, Block wantedID) {
        if (blockCoordinates == null)
            return 2;
        else {
            Block blockID = getBlockFromVector(block);
            return (byte) ((isAir(blockID) || blockID != wantedID) ? 1 : canReach(block, getRadius()) ? 0 : 3);
        }
    }

    private String getMode() {
        switch (mode) {
            case 0:
                return selectedBlock == null ? "[None]" : "[" + blockID + "]";
            case 1:
                return "(Instant)";
            case 2:
                return "(Click)";
            default:
                return "";
        }
    }

    private MovingObjectPosition rayTrace(Vector3f vector) {
        Vec3 player = mc.thePlayer.worldObj.getWorldVec3Pool().getVecFromPool(mc.thePlayer.posX, mc.thePlayer.posY + mc.thePlayer.getEyeHeight(), mc.thePlayer.posZ),
                block = mc.thePlayer.worldObj.getWorldVec3Pool().getVecFromPool(vector.x + 0.5F, vector.y + 0.5F, vector.z + 0.5F);

        return mc.theWorld.clip(player, block);
    }

    private boolean canReach(Vector3f vector, float distance) {
        return blockHelper.canReach(vector.x, vector.y, vector.z, distance);
    }
    
    private boolean canReach(int x, int y, int z, float distance) {
        return blockHelper.canReach(x, y, z, distance);
    }

    private Block getBlockFromVector(Vector3f vector) {
        return mc.theWorld.getBlock((int) vector.x, (int) vector.y, (int) vector.z);
    }

    /**
     * Send packet method.
     */
    private void sendBreakPacket(Vector3f position, int mode, int side) {
        mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(mode, (int) position.getX(), (int) position.getY(), (int) position.getZ(), side));
    }
    
    private void sendBreakPacket(int x, int y, int z, int mode, int side) {
        mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(mode, x, y, z, side));
    }

    private float getRadius() {
        return radius.getValue();
    }

    private float getSpeed() {
        return speed.getValue();
    }
    
    private boolean isAir(Block block) {
    	return block == null ? true : block.func_149688_o() == Material.air;
    }

}
