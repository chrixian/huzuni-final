package net.halalaboos.huzuni.rendering;

import static org.lwjgl.opengl.GL11.*;

public class DisplayList {

	private int id;
	
	public DisplayList() {
		genId();
	}
	
	public void genId() {
		genId(1);
	}
	public void genId(int range) {
		id = glGenLists(range);
	}
	
	public void startCompiling() {
		glNewList(id, GL_COMPILE);
	}
	
	public void end() {
		glEndList();
	}
	
	public void draw() {
        glCallList(id);
    }
	
}
