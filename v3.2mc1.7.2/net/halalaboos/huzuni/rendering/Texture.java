package net.halalaboos.huzuni.rendering;

import net.halalaboos.huzuni.utils.TextureUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import java.awt.*;

/**
 * Simple texture class. Uses Mincraft resource location shiz, all textures MUST be packed as so: 'assets/minecraft/<your texture here>'
 */
public final class Texture {
    private ResourceLocation texture;

    public Texture(String textureURL) {
        texture = new ResourceLocation(textureURL);
        Minecraft.getMinecraft().getTextureManager().bindTexture(texture);
    }

    public void renderTexture(double x, double y, double width, double height, Color c) {
        if (texture != null) {
        	Minecraft.getMinecraft().getTextureManager().bindTexture(texture);
            GL11.glColor4f((float) c.getRed() / 255F, (float) c.getGreen() / 255F, (float) c.getBlue() / 255F, (float) c.getAlpha() / 255f);
            TextureUtils.renderTexture(x, y, width, height);
        }
    }

    public void bindTexture() {
        if (texture != null) {
        	Minecraft.getMinecraft().getTextureManager().bindTexture(texture);
        }
    }

    public void setTextue(String textureURL) {
        texture = new ResourceLocation(textureURL);
        Minecraft.getMinecraft().getTextureManager().bindTexture(texture);
    }

}
