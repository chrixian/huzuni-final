/**
 *
 */
package net.halalaboos.huzuni.ui.clickable.theme;

import net.halalaboos.huzuni.helpers.MinecraftHelper;
import net.halalaboos.huzuni.utils.TextureUtils;
import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.components.Button;
import net.halalaboos.lib.ui.components.Dropdown;
import net.halalaboos.lib.ui.components.Label;
import net.halalaboos.lib.ui.components.ProgressBar;
import net.halalaboos.lib.ui.components.Slider;
import net.halalaboos.lib.ui.components.SlotComponent;
import net.halalaboos.lib.ui.components.TextArea;
import net.halalaboos.lib.ui.components.TextField;
import net.halalaboos.lib.ui.containers.Window;
import net.halalaboos.lib.ui.theme.DefaultTheme;
import net.halalaboos.lib.ui.theme.ComponentRenderer;
import net.halalaboos.lib.ui.theme.Theme;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import java.awt.*;
import java.text.DecimalFormat;

/**
 * @author Halalaboos
 * @since Aug 30, 2013
 */
public class HuzuniTheme extends DefaultTheme implements MinecraftHelper {

	// TODO texture based GUI. (Already had it done, just removed it.. Cause I'm bad with textures.. :'()
    public HuzuniTheme() {
    	super();
    }

}
