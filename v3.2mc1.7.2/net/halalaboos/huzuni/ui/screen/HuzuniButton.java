package net.halalaboos.huzuni.ui.screen;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.rendering.font.MinecraftFontRenderer;
import net.halalaboos.huzuni.utils.Timer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;

import org.lwjgl.opengl.GL11;

import java.awt.*;

public class HuzuniButton extends GuiButton {
    private long lastScale;
    private int scaledWidth = 0;
    private int scaledHeight = 0;
    private int alpha = 10;

    public HuzuniButton(int id, int x, int y, int width, int height, String text) {
        super(id, x, y, width, height, text);
    }

    @Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY) {
        if (this.drawButton) {
            Color black = (new Color(0, 0, 0, this.enabled ? alpha : alpha/2));
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            this.mouseOver = mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;
            int hoverState = this.getRenderMode(this.mouseOver);
            updateButton();
            this.drawRect(this.x - scaledWidth, this.y - scaledHeight, this.x + this.width + scaledWidth, this.y + this.height + scaledHeight, black.getRGB());
            this.mouseDragged(mc, mouseX, mouseY);
            int textColor = 14737632;

            if (!this.enabled)
                textColor = 0xD6D6D6;

            else if (this.mouseOver)
                textColor = 0xffffff;


            Huzuni.display.guiFontRenderer.drawCenteredStringNoShadow(this.label, this.x + this.width / 2 - 1, this.y + (this.height - 6) / 2, textColor);
        }
    }

    private void updateButton() {

        if (Timer.getSystemTime() - lastScale >= 15) {
            if (mouseOver && enabled)
                alpha += 15;
            else
                alpha -= 5;

            lastScale = Timer.getSystemTime();
        }

        if (alpha < 75)
            alpha = 75;

        if (alpha > 175) {
            alpha = 175;
        }
    }
}