package net.halalaboos.huzuni.ui.screen;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.mods.Module;
import net.halalaboos.huzuni.ui.windows.WindowManager;
import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.DefaultContainer;
import net.halalaboos.lib.ui.components.Button;
import net.halalaboos.lib.ui.containers.Window;
import net.minecraft.client.gui.GuiScreen;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import java.awt.*;

public class HuzuniGui extends GuiScreen {

    public void initGui() {
        Keyboard.enableRepeatEvents(true);
        for (Window window : Huzuni.windowManager.getWindows()) {
            for (Component component : window.getComponents()) {
                if (component instanceof Button) {
                    Module module = Huzuni.modManager.getMod(((Button) component).getTitle());
                    if (module != null)
                        ((Button) component).setHighlight(module.isEnabled());
                }
            }
        }
    }

    @Override
    public void onGuiClosed() {
        Keyboard.enableRepeatEvents(false);
        Settings.save();
        Huzuni.windowManager.save();
        Huzuni.modManager.save();
    }

    @Override
    public void drawScreen(int par1, int par2, float par3) {
        super.drawDefaultBackground();
        super.drawScreen(par1, par2, par3);
        setupOverlayDefaultScale();
        Huzuni.windowManager.render();
        mc.entityRenderer.setupOverlayRendering();
    }

    private void setupOverlayDefaultScale() {
        double scale = 2;
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0.0D, mc.displayWidth / scale, mc.displayHeight / scale, 0.0D, 1000.0D, 3000.0D);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glLoadIdentity();
        GL11.glTranslatef(0.0F, 0.0F, -2000.0F);
        GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight);
    }

    @Override
    protected void keyTyped(char c, int keyCode) {
        super.keyTyped(c, keyCode);
        if (keyCode == Keyboard.KEY_ESCAPE) {
        	mc.displayGuiScreen(null);
        	return;
        }
        Huzuni.windowManager.onKeyTyped(keyCode, c);
    }

    @Override
    protected void mouseClicked(int x, int y, int buttonID) {
        super.mouseClicked(x, y, buttonID);
        Huzuni.windowManager.onMousePressed((int) ((Mouse.getX() * (mc.displayWidth / 2) / mc.displayWidth)), (((mc.displayHeight / 2) - Mouse.getY() * (mc.displayHeight / 2) / mc.displayHeight)) - 1, buttonID);
    }

}
