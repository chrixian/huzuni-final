package net.halalaboos.huzuni.ui.screen.alts;

import in.brud.huzuni.util.PanoramaRenderer;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.ui.screen.HuzuniButton;
import net.halalaboos.huzuni.utils.RenderUtils;
import net.halalaboos.lib.io.FileUtils;
import net.halalaboos.lib.ui.BackgroundContainer;
import net.halalaboos.lib.ui.theme.DefaultTheme;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.GuiYesNo;
import net.minecraft.util.EnumChatFormatting;

import javax.swing.*;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class AltManager extends GuiScreen {
    private final File altFile = new File(Huzuni.getSaveDirectory(), "Accounts.txt");
    private final GuiScreen parentGui;
    private static String lastLoginStatus = "N/A";
    private PanoramaRenderer panoramaRenderer;

    private GuiTextField field;
    private GuiButton use;
    private GuiButton remove;

    private final BackgroundContainer container = new BackgroundContainer(new DefaultTheme());
    
    private final SlotAlts slotAlts = new SlotAlts(this, new Dimension(200, 500));
    private int alpha = 255;
    
    public AltManager(GuiScreen par1GuiScreen) {
        parentGui = par1GuiScreen;
    	container.add(slotAlts);
    }

    @Override
    public void initGui() {
        int slotWidth = 300, slotHeight = height / 2;

        panoramaRenderer = new PanoramaRenderer(width, height);
        panoramaRenderer.init();

    	slotAlts.setArea(new Rectangle(this.width / 2 - slotWidth / 2, this.height / 2 - slotHeight / 2 - 10, slotWidth, slotHeight));
    	slotAlts.setComponents(readAlts());
        slotAlts.setBackgroundColor(new Color(0, 0, 0, 50));

    	field = new GuiTextField(mc.fontRenderer, width / 2 - 110, height - 74, 220, 20);

        buttonList.add(new HuzuniButton(15, this.width / 2 - 154, this.height - 28, 152, 20, "Load File.."));
        buttonList.add(new HuzuniButton(6, this.width / 2 + 2, this.height - 28, 152, 20, "Done"));
        buttonList.add(new HuzuniButton(12, this.width / 2 - 74, this.height - 52, 70, 20, "Add"));
        buttonList.add(new HuzuniButton(5, this.width / 2 + 4, this.height - 52, 70, 20, "Direct"));

        use = new HuzuniButton(14, this.width / 2 - 154, this.height - 52, 75, 20, "Login");
        buttonList.add(use);
        use.enabled = slotAlts.getSelectedItem() >= 0;
        remove = new HuzuniButton(13, width / 2 + 4 + 76, height - 52, 75, 20, "Remove");
        buttonList.add(remove);
        remove.enabled = slotAlts.getSelectedItem() >= 0;
    }

    /**
     * Fired when a control is clicked. This is the equivalent of
     * ActionListener.actionPerformed(ActionEvent e).
     */
    @Override
    protected void actionPerformed(GuiButton par1GuiButton) {
        if (!par1GuiButton.enabled) {
            return;
        }

        switch (par1GuiButton.id) {
            case 5:
                mc.displayGuiScreen(new HuzuniLogin(this, false));
                break;
            case 6:
                mc.displayGuiScreen(parentGui);
                break;
            case 12:
                mc.displayGuiScreen(new HuzuniLogin(this, true));
                break;
            case 13:
                mc.displayGuiScreen(new GuiYesNo(this, "Delete account '" + ((String) slotAlts.getComponents().get(slotAlts.getSelectedItem())).split(":")[0] + "'", "Are you sure?", "Delete", "Back", 0));
                break;
            case 14:
                if (slotAlts.getSelectedItem() >= 0 && slotAlts.getComponents().size() > 0)
                	slotAlts.login();
                break;
            case 15:
                /**
                 * This can be done with SwingUtilities invoke method, which is a lot safer.
                 *
                 * @see {@code javax.swing.SwingUtilities#invokeLater}
                 *
                 * Make sure to read the description.
                 */

                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        JFileChooser fileChooser = new JFileChooser();
                        if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                            File selectedFile = fileChooser.getSelectedFile();
                            BufferedReader reader = null;
                            try {
                                reader = new BufferedReader(new FileReader(selectedFile));
                                for (String line; (line = reader.readLine()) != null; ) {
                                    // If it's got a colon and it's not currently inside of our alt list, we're gonna add it.
                                    if (line.contains(":")) {
                                        if (!slotAlts.getComponents().contains(line)) {
                                        	slotAlts.getComponents().add(line);
                                        }
                                    }
                                }
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            } finally {
                                if (reader != null)
                                    try {
                                        reader.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                            }
                        }
                    }
                });
                break;
            default:
                //altList.actionPerformed(par1GuiButton);
                break;
        }
    }

    public void confirmClicked(boolean confirm, int id) {
        if (confirm && id == 0) {
           slotAlts.removeAccount();
           FileUtils.writeFile(altFile, slotAlts.getComponents());
        }
        mc.displayGuiScreen(this);
    }

    /**
     * Fired when a key is typed. This is the equivalent of
     * KeyListener.keyTyped(KeyEvent e).
     */
    @Override
    protected void keyTyped(char c, int keyCode) {
        super.keyTyped(c, keyCode);
        slotAlts.onKeyTyped(keyCode, c);
        // field.textboxKeyTyped(c, keyCode);
    }

    /**
     * Called when the mouse is clicked.
     */
    @Override
    protected void mouseClicked(int par1, int par2, int par3) {
        super.mouseClicked(par1, par2, par3);
        container.onMousePressed(par1, par2, par3);
        // field.mouseClicked(par1, par2, par3);
    }

    /**
     * Draws the screen and all the components in it.
     */
    @Override
    public void drawScreen(int par1, int par2, float par3) {
        Color background = new Color(0,0,0,alpha);
        panoramaRenderer.renderSkybox(par1, par2, par3);
        RenderUtils.drawRect(0, 0, width, 30, 0x4F000000);
        Huzuni.display.titleFontRenderer.drawString("ACCOUNT LOGIN", 2, 2, -1);
        Huzuni.display.guiFontRenderer.drawString("ALTS: " + (slotAlts.getComponents().size()) + EnumChatFormatting.GRAY +  " // " + EnumChatFormatting.RESET + "Last login: " +
                mc.getSession().getUsername() + " (" + EnumChatFormatting.GRAY + this.lastLoginStatus + EnumChatFormatting.RESET + ")", 2, 20, 0xffffff);
        use.enabled = slotAlts.getSelectedItem() >= 0;
        remove.enabled = slotAlts.getSelectedItem() >= 0;
        super.drawScreen(par1, par2, par3);
        //        int slotWidth = 300, slotHeight = height / 2;
        // field.drawTextBox();
        container.render(par1, par2);
        panoramaRenderer.renderFade();
    }

    /**
     * Called from the main game loop to update the screen.
     */
    @Override
    public void updateScreen() {
        super.updateScreen();
        if(alpha > 0)
            alpha -= 15;
        panoramaRenderer.panoramaTick();
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
       FileUtils.writeFile(altFile, slotAlts.getComponents());
    }

    public List<String> readAlts() {
        if (altFile.exists()) {
            return new CopyOnWriteArrayList(FileUtils.readFile(altFile));
        }
        return new ArrayList<String>();
    }

    public static String getLastLoginStatus() {
        return lastLoginStatus;
    }

    public static void setLastLoginStatus(String lastLoginStatus) {
        AltManager.lastLoginStatus = lastLoginStatus;
    }

    public File getAltFile() {
        return altFile;
    }

	public SlotAlts getSlotAlts() {
		return slotAlts;
	}
}
