package net.halalaboos.huzuni.ui.screen.alts;

import in.brud.huzuni.element.HuzuniPassField;
import in.brud.huzuni.element.HuzuniTextField;
import in.brud.huzuni.util.PanoramaRenderer;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.ui.screen.HuzuniButton;
import net.halalaboos.huzuni.utils.LoginUtils;
import net.halalaboos.huzuni.utils.RenderUtils;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.Session;

import org.lwjgl.input.Keyboard;

import javax.swing.*;

public class HuzuniLogin extends GuiScreen {

    private GuiTextField username;
    private GuiTextField password;
    private AltManager prevScreen;
    private boolean addToList;
    private static String lastLogin = "Try to login...";
    private PanoramaRenderer panoramaRenderer;

    /**
     * An account login screen, is also used to add accounts to the alt list. Just specify.
     */
    public HuzuniLogin(AltManager prevScreen, boolean addToList) {
        this.prevScreen = prevScreen;
        this.addToList = addToList;

    }

    public void updateScreen() {
        this.username.updateCursorCounter();
        this.password.updateCursorCounter();
        panoramaRenderer.panoramaTick();
    }

    public void initGui() {

        panoramaRenderer = new PanoramaRenderer(width, height);
        panoramaRenderer.init();

        Keyboard.enableRepeatEvents(true);
        this.buttonList.add(new HuzuniButton(0, this.width / 2 - 100, this.height / 4 + 96 + 12, 200, 20, addToList ? "Add" : "Login"));
        this.buttonList.add(new HuzuniButton(1, this.width / 2 - 100, this.height / 4 + 120 + 12, 200, 20, "Cancel"));
        this.username = new HuzuniTextField(this.fontRenderer, this.width / 2 - 100, 66, 200, 20);
        this.username.setFocused(true);
        this.username.setMaxStringLength(128);
        this.username.setText("");
        this.password = new HuzuniPassField(this.fontRenderer, this.width / 2 - 100, 106, 200, 20);
        this.password.setMaxStringLength(128);
        this.password.setText("");
    }

    protected void actionPerformed(GuiButton par1GuiButton) {
        if (par1GuiButton.id == 0) {
            if (addToList) {
                mc.displayGuiScreen(prevScreen);
                prevScreen.getSlotAlts().getComponents().add(username.getText() + ":" + password.getText());
            } else {
                if (username.getText().length() > 0) {
                    if (password.getText().length() > 0) {
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                String[] login = LoginUtils.loginToMinecraft(username.getText(), password.getText());
                                // If the return code was OK, we'll swap accounts.
                                if (login.length >= 5) {
                                	mc.setSession(new Session(login[2].trim(), login[4].trim(), login[3].trim()));
                                    lastLogin = "Sucessfully logged into: " + username.getText();
                                    prevScreen.setLastLoginStatus(EnumChatFormatting.GRAY + "Success" + EnumChatFormatting.RESET);
                                } else {
                                    lastLogin = login[0];
                                    prevScreen.setLastLoginStatus(EnumChatFormatting.GRAY + login[0] + EnumChatFormatting.RESET);
                                }
                            }
                        });
                    } else {
                    	mc.setSession(new Session(username.getText(), "", ""));
                        lastLogin = "Changed name to: " + username.getText();
                        prevScreen.setLastLoginStatus(EnumChatFormatting.GRAY + "Success" + EnumChatFormatting.RESET);
                    }
                }
            }
        } else if (par1GuiButton.id == 1) {
            mc.displayGuiScreen(prevScreen);
        }
    }

    public void keyTyped(char ch, int key) {
        super.keyTyped(ch, key);
        this.username.textboxKeyTyped(ch, key);
        this.password.textboxKeyTyped(ch, key);

        if (key == Keyboard.KEY_TAB) {
            if (this.username.isFocused()) {
                this.username.setFocused(false);
                this.password.setFocused(true);
            } else {
                this.username.setFocused(true);
                this.password.setFocused(false);
            }
        }

        if(key == Keyboard.KEY_RETURN) {
            this.actionPerformed((GuiButton) this.buttonList.get(0));
        }

        if (key == 13) {
            this.actionPerformed((GuiButton) this.buttonList.get(0));
        }

        ((GuiButton) this.buttonList.get(0)).enabled = this.username.getText().length() > 3;
    }

    protected void mouseClicked(int par1, int par2, int par3) {
        super.mouseClicked(par1, par2, par3);
        this.username.mouseClicked(par1, par2, par3);
        this.password.mouseClicked(par1, par2, par3);
    }

    public void drawScreen(int par1, int par2, float par3) {
        panoramaRenderer.renderSkybox(par1, par2, par3);
        RenderUtils.drawRect(0, 0, width, 30, 0x4F000000);
        Huzuni.display.guiFontRenderer.drawString(addToList ? EnumChatFormatting.GRAY + ("DO IT... NOW") : ("STATUS: " + EnumChatFormatting.GRAY + lastLogin + EnumChatFormatting.RESET), 2, 20, 0xffffff);
        Huzuni.display.titleFontRenderer.drawString((addToList ? "ADD AN ALT" : "DIRECT LOGIN").toUpperCase(), 2, 2, -1);
        Huzuni.display.guiFontRenderer.drawString("Username", this.width / 2 - 100, 53, 10526880);
        Huzuni.display.guiFontRenderer.drawString("Password", this.width / 2 - 100, 94, 10526880);
        this.username.drawTextBox();
        this.password.drawTextBox();
        super.drawScreen(par1, par2, par3);
        ((GuiButton) this.buttonList.get(0)).enabled = this.username.getText().length() > 3;
        panoramaRenderer.renderFade();
    }
}
