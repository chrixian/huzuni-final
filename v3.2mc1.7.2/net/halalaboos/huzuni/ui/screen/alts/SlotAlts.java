package net.halalaboos.huzuni.ui.screen.alts;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.SwingUtilities;

import com.darkmagician6.morbidlib.utils.timer.AccurateTimer;
import com.darkmagician6.morbidlib.utils.timer.Timer;

import net.halalaboos.huzuni.utils.LoginUtils;
import net.halalaboos.huzuni.utils.RenderUtils;
import net.halalaboos.lib.ui.components.SlotComponent;
import net.halalaboos.lib.ui.utils.GLUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.Session;

public class SlotAlts extends SlotComponent<String> {
	private final Color highlightColor = new Color(0F, 0.5F, 1F, 0.75F),
	defaultColor = new Color(0F, 0F, 0F, 0.35F);
	private final AltManager guiAlts;

	private int selectedItem = -1;
	private Timer timer = new AccurateTimer();
	
	public SlotAlts(AltManager guiAlts, Dimension dimensions) {
		super(dimensions);
		this.guiAlts = guiAlts;
	}

	@Override
	public void render(int index, String account, Rectangle boundaries,
			boolean mouseOver, boolean mouseDown) {
		String username = "";
		String password = "";
		if (account.split(":").length >= 1) {
			username = account.split(":")[0];
			password = account.split(":")[1].replaceAll(".", "*");	
		}
		int usernameX = boundaries.x;
		int passwordX = boundaries.x;

		GLUtils.setColor(GLUtils.getColorWithAffects(defaultColor, mouseOver, mouseDown));

        if(selectedItem == index)
		    GLUtils.drawFilledRect(boundaries.x, boundaries.y, boundaries.x + boundaries.width, boundaries.y + boundaries.height);

		GLUtils.drawStringWithShadow(username, usernameX + 2, boundaries.y + 2, 0xFFFFFF);
		GLUtils.drawStringWithShadow(password, passwordX + 2, boundaries.y + 12, 0xCCCCCC);

	}

	@Override
	public void onClicked(int index, String account, Rectangle boundaries,
			Point mouse, int buttonID) {
		if (selectedItem == index) {
			if (timer.getCurrentTime() - timer.get() < 500) {
				login();
			}
		} else {
			selectedItem = index;
		}
		timer.reset();
	}

	@Override
	public void onReleased(int index, String account, Point mouse) {
	}

	@Override
	public int getElementHeight() {
		return 22;
	}
	
	public void login() {
		if (selectedItem != -1) {
			String account = components.get(selectedItem);
			final String username = account.split(":")[0];
			final String password = account.split(":")[1];
			SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    String[] login = LoginUtils.loginToMinecraft(username, password);
                    // If the return code was OK, we'll swap accounts.
                    if (login.length >= 5) {
                    	Minecraft.getMinecraft().setSession(new Session(login[2].trim(), login[4].trim(), login[3].trim()));
                    	guiAlts.setLastLoginStatus(EnumChatFormatting.GRAY + "Success" + EnumChatFormatting.RESET);
                    } else {
                    	guiAlts.setLastLoginStatus(EnumChatFormatting.GRAY +  login[0] + EnumChatFormatting.RESET);
                    }
        			selectedItem = -1;
                }
            });
		}
	}

	public void removeAccount() {
		if (selectedItem != -1) {
			this.components.remove(selectedItem);
			selectedItem = -1;
		}
	}

	public int getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(int selectedItem) {
		this.selectedItem = selectedItem;
	}
	
}
