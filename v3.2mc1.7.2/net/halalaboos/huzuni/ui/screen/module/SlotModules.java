package net.halalaboos.huzuni.ui.screen.module;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import com.darkmagician6.morbidlib.utils.timer.AccurateTimer;
import com.darkmagician6.morbidlib.utils.timer.Timer;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.mods.Keybindable;
import net.halalaboos.huzuni.mods.Module;
import net.halalaboos.lib.ui.components.SlotComponent;
import net.halalaboos.lib.ui.utils.GLUtils;

public class SlotModules extends SlotComponent<Module> {
	private final Color highlightColor = new Color(0F, 0.5F, 0.7F, 0.75F),
	defaultColor = new Color(0F, 0F, 0F, 0.35F);

	private int selectedItem = -1;
	private Timer timer = new AccurateTimer();
	
	public SlotModules(Dimension dimensions) {
		super(dimensions);
	}

	@Override
	public void render(int index, Module mod, Rectangle boundaries,
			boolean mouseOver, boolean mouseDown) {
		String enabled = mod.isEnabled() ? "On" : "Off";
		int centerX = boundaries.x + boundaries.width / 2 - GLUtils.getStringWidth(mod.getName()) / 2;
		
		GLUtils.setColor(GLUtils.getColorWithAffects(selectedItem == index ? highlightColor : defaultColor, mouseOver, mouseDown));
		GLUtils.drawFilledRect(boundaries.x, boundaries.y, boundaries.x + boundaries.width, boundaries.y + boundaries.height);
		GLUtils.drawStringWithShadow(mod.getName(), boundaries.x + 2, boundaries.y + 2, 0xFFFFFF);
        GLUtils.drawStringWithShadow(mod.getDescription(), boundaries.x + 2, boundaries.y + 12, 0x5FFFFFFF);
        GLUtils.drawStringWithShadow(enabled, boundaries.x + boundaries.width - GLUtils.getStringWidth(enabled)
         - 4, boundaries.y + 2, mod.isEnabled() ? 0xFF3399FF : 0xFFFFFF);
		if (mod instanceof Keybindable) {
			Keybindable keybind = (Keybindable) mod;
			String keybindName = keybind.getKeyCode() == -1 ? "Key: None" : "Key:" + keybind.getKeyName();
			GLUtils.drawStringWithShadow(keybindName, boundaries.x + 2, boundaries.y + 22, 0x7FFFFFFF);
		} else {
			GLUtils.drawStringWithShadow("Not keybindable!", boundaries.x + 2, boundaries.y + 22, 0x2FFFFFFF);
		}
	}
	
	@Override
	public void onKeyTyped(int keyCode, char c) {
		super.onKeyTyped(keyCode, c);
		if (selectedItem != -1) {
			Module mod = components.get(selectedItem);
			if (mod instanceof Keybindable) {
				((Keybindable) mod).setKeyCode(keyCode);
				selectedItem = -1;
			}
		}
	}

	@Override
	public void onClicked(int index, Module mod, Rectangle boundaries,
			Point mouse, int buttonID) {
	}

	@Override
	public void onReleased(int index, Module mod, Point mouse) {
		if (selectedItem == index) {
			if (timer.getCurrentTime() - timer.get() < 500)
				toggle();
			else
				selectedItem = -1;
		} else {
			selectedItem = index;
		}
		timer.reset();
	}

	@Override
	public int getElementHeight() {
		return 32;
	}
	
	public void toggle() {
		if (selectedItem != -1) {
			components.get(selectedItem).toggle();
			selectedItem = -1;
		}
	}
	
	public boolean isKeybindableSelected() {
		return isSelected() ? components.get(selectedItem) instanceof Keybindable : false;
	}
	
	public boolean isSelected() {
		return selectedItem >= 0 && selectedItem < components.size();
	}

	public int getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(int selectedItem) {
		this.selectedItem = selectedItem;
	}

	public void clearKeybind() {
		if (isKeybindableSelected()) {
			Module mod = components.get(selectedItem);
			((Keybindable) mod).setKeyCode(-1);
			selectedItem = -1;
		}
		
	}
	
}
