/**
 * 
 */
package net.halalaboos.huzuni.ui.windows.friends;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.lib.ui.components.SlotComponent;
import net.halalaboos.lib.ui.utils.GLUtils;
import net.minecraft.client.gui.GuiPlayerInfo;
import net.minecraft.util.StringUtils;
/**
 * @author Halalaboos
 *
 * @since Oct 5, 2013
 */
public class SlotFriends extends SlotComponent<GuiPlayerInfo> {
	
	private final Color enabledColor = new Color(0F, 0.5F, 1F, 0.75F),
	disabledColor = new Color(0.3F, 0.3F, 0.3F, 0.75F);

	/**
	 * @param dimensions
	 */
	public SlotFriends(Dimension dimensions) {
		super(dimensions);
		
	}
	
	@Override
	public void render(int index, GuiPlayerInfo player, Rectangle boundaries,
			boolean mouseOver, boolean mouseDown) {
		String name = StringUtils.stripControlCodes(player.name);
		GLUtils.setColor(GLUtils.getColorWithAffects(Huzuni.friendManager.contains(name) ? enabledColor : disabledColor, mouseOver, mouseDown));
		GLUtils.drawFilledRect(boundaries.x, boundaries.y, boundaries.x + boundaries.width, boundaries.y + boundaries.height);
		GLUtils.drawStringWithShadow(name, boundaries.x + 2, boundaries.y + 2, 0xFFFFFF);
	}

	@Override
	public void onClicked(int index, GuiPlayerInfo player,
			Rectangle boundaries, Point mouse, int buttonID) {
	}

	@Override
	public void onReleased(int index, GuiPlayerInfo player, Point mouse) {
		String name = StringUtils.stripControlCodes(player.name);
		if (Huzuni.friendManager.contains(name))
			Huzuni.friendManager.remove(name);
		else
			Huzuni.friendManager.add(name);
	}

	@Override
	public int getElementHeight() {
		return 11;
	}
}
