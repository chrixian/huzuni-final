/**
 * 
 */
package net.halalaboos.huzuni.ui.windows.keybinds;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import net.halalaboos.huzuni.mods.Module;
import net.halalaboos.lib.ui.components.SlotComponent;
import net.halalaboos.lib.ui.containers.Window;
import net.halalaboos.lib.ui.utils.GLUtils;
import net.halalaboos.huzuni.mods.Keybindable;

/**
 * @author Halalaboos
 *
 * @since Oct 6, 2013
 */
public class SlotKeybinds extends SlotComponent<Module> {
	private final Color highlightColor = new Color(0F, 0.5F, 1F, 0.75F),
	defaultColor = new Color(0.3F, 0.3F, 0.3F, 0.75F);
	private Module selectedModule = null;
	
	/**
	 * @param dimensions
	 */
	public SlotKeybinds(Dimension dimensions) {
		super(dimensions);
		
	}

	/**
	 * @see net.halalaboos.lib.ui.components.SlotComponent#render(int, java.lang.Object, java.awt.Rectangle, boolean, boolean)
	 */
	@Override
	public void render(int index, Module module, Rectangle boundaries,
			boolean mouseOver, boolean mouseDown) {
		Keybindable keybindable = (Keybindable) module;		
		boolean highlight = selectedModule == module;
		GLUtils.setColor(GLUtils.getColorWithAffects(highlight ? highlightColor : defaultColor, mouseOver, mouseDown));
		GLUtils.drawFilledRect(boundaries.x, boundaries.y, boundaries.x + boundaries.width, boundaries.y + boundaries.height);
		GLUtils.drawString(module.getName(), boundaries.x + 2, boundaries.y + 2, 0xFFFFFF);
		if (!keybindable.getKeyName().equals("-1"))
			GLUtils.drawString(keybindable.getKeyName(), boundaries.x + boundaries.width - GLUtils.getStringWidth(keybindable.getKeyName()), boundaries.y + 2, 0xFFFFFF);
	}

	/**
	 * @see net.halalaboos.lib.ui.components.SlotComponent#onClicked(int, java.lang.Object, java.awt.Rectangle, java.awt.Point, int)
	 */
	@Override
	public void onClicked(int index, Module module, Rectangle boundaries,
			Point mouse, int buttonID) {
	}

	/**
	 * @see net.halalaboos.lib.ui.components.SlotComponent#onReleased(int, java.lang.Object, java.awt.Point)
	 */
	@Override
	public void onReleased(int index, Module module, Point mouse) {
		if (selectedModule == module)
			selectedModule = null;
		else
			selectedModule = module;
	}

	@Override
	public void onKeyTyped(int keyCode, char c) {
		super.onKeyTyped(keyCode, c);
		if (selectedModule != null) {
			((Keybindable) selectedModule).setKeyCode(keyCode);
			selectedModule = null;
		}
	}
	
	/**
	 * @see net.halalaboos.lib.ui.components.SlotComponent#getElementHeight()
	 */
	@Override
	public int getElementHeight() {
		return 12;
	}
	
	public void clearKeybind() {
		if (selectedModule != null) {
			((Keybindable) selectedModule).setKeyCode(-1);
			selectedModule = null;
		}
	}

}
