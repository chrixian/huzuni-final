/**
 * 
 */
package net.halalaboos.huzuni.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import net.halalaboos.huzuni.client.DonatorLoader;

/**
 * @author Halalaboos
 *
 * @since Sep 30, 2013
 */
public class ThreadDownloadDonators extends Thread {

	public ThreadDownloadDonators() {
		
	}
	
	@Override
	public void run() {
		BufferedReader reader = null;
		try {
			URL url = new URL(DonatorLoader.donorURL);
			reader = new BufferedReader(new InputStreamReader(url.openStream()));
			for (String s; (s = reader.readLine()) != null; ) {
				DonatorLoader.getDonators().add(s.toLowerCase());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (reader != null)
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
	
}
