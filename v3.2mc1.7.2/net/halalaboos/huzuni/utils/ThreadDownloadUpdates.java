/**
 * 
 */
package net.halalaboos.huzuni.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import net.halalaboos.huzuni.helpers.Helper;

/**
 * @author Halalaboos
 *
 * @since Sep 30, 2013
 */
public class ThreadDownloadUpdates extends Thread {

	public ThreadDownloadUpdates() {
		
	}
	
	@Override
	public void run() {
		try {
			final BufferedReader reader = new BufferedReader(
					new InputStreamReader(
							new URL("http://halalaboos.net/client/updates")
									.openStream()));
			for (String s; (s = reader.readLine()) != null;)
				Helper.huzuniHelper.getUpdates().add(s.trim());
			reader.close();
		} catch (IOException e) {
			Helper.huzuniHelper.getUpdates().add("\2474Could not connect to server.");
			e.printStackTrace();
		}

	}
	
}
