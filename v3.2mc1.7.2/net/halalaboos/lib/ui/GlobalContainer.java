/**
 * 
 */
package net.halalaboos.lib.ui;

import java.awt.Point;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.lwjgl.input.Mouse;

import com.darkmagician6.morbidlib.utils.timer.AccurateTimer;
import com.darkmagician6.morbidlib.utils.timer.Timer;

import net.halalaboos.lib.ui.theme.DefaultTheme;
import net.halalaboos.lib.ui.theme.Theme;
import net.halalaboos.lib.ui.utils.GLUtils;

/**
 * @author Halalaboos
 *
 */
public class GlobalContainer {
	
	protected List<Container> containers = new CopyOnWriteArrayList<Container>();
	
	protected Theme theme = new DefaultTheme();
	
	protected Point mouse = new Point(0, 0), savedMouse = new Point(0, 0);
	
	protected Container mousedContainer, downContainer, selectedContainer;
	
	protected int savedButtonId = -1;
	
	protected boolean layering = true;
	
	protected Timer timer = new AccurateTimer();
	
	public void onKeyTyped(int keyCode, char c) {
		if (downContainer == null && selectedContainer != null) {
			selectedContainer.onKeyTyped(keyCode, c);
		}
	}
	
	public void onMousePressed(int x, int y, int buttonId) {
		savedMouse.setLocation(x, y);
		savedButtonId = buttonId;
		for (int i = containers.size() - 1; i >= 0; i--) {
			Container container = containers.get(i);
			if (container.isActive() && !shouldIgnore(container)) {
				if (container.onMousePressed(x, y, buttonId)) {
					selectedContainer = mousedContainer;
					downContainer = mousedContainer;
					doLayering(downContainer);
					break;
				}
			}
		}
	}
	
	public void onMouseReleased(int x, int y, int buttonId) {
		if (downContainer != null) {
			downContainer.onMouseReleased(x, y, buttonId);
			downContainer = null;
		}
	}
	
	public void render() {
		mouse.setLocation(GLUtils.getMouseX(), GLUtils.getMouseY());
		calculateMousedContainer();
		calculateMouseDown();
		for (Container container : containers) {
			if (container.isActive() && !shouldIgnore(container)) {
				boolean mouseDown = downContainer == container;
				boolean mouseOver = downContainer == null ? mousedContainer == container : mousedContainer == container && mouseDown;
				container.setMouseDown(mouseDown);
				container.setMouseOver(mouseOver);
				container.update(mouse);
				theme.renderContainer(container, mouse);
				container.renderComponents(theme, mouse);
			}
		}
		if (timer.hasReach(1250F)) {
			if (mousedContainer != null && mousedContainer.getTooltip() != null) {
				theme.renderToolTip(mousedContainer.getTooltip(), mouse);
			} else
				timer.reset();
		}
	}
	
	protected boolean shouldIgnore(Container container) {
		return false;
	}
	
	/**
	 * Handles logic for your mouse being held down.
	 * */
	protected void calculateMouseDown() {
		if (savedButtonId != -1) {
			if (!Mouse.isButtonDown(savedButtonId)) {
				onMouseReleased(mouse.x, mouse.y, savedButtonId);
				savedButtonId = -1;
			}
		}
	}
	
	/**
	 * Originally made to calculate which container your mouse is over, it now constantly assigns
	 * */
	protected void calculateMousedContainer() {
		if (mousedContainer != null) {
			Container lastContainer = mousedContainer;
			mousedContainer = findWithinPoint(mouse);
			if (lastContainer != mousedContainer) 
				timer.reset();
		} else
			mousedContainer = findWithinPoint(mouse);
	}
	
	/**
	 * Finds the container with the point inside of it.
	 * */
	protected Container findWithinPoint(Point point) {
		for (int i = containers.size() - 1; i >= 0; i--) {
			Container container = containers.get(i);
			if (container.isActive() && !shouldIgnore(container)) {
				if (container.isPointInside(mouse)) {
					return container;
				}
			}
		}
		return null;
	}
	
	protected void doLayering(Container container) {
		if (layering) {
			containers.remove(container);
			containers.add(container);
		}
	}
	
	public void add(Container container) {
		containers.add(container);
	}
	
	public void remove(Container container) {
		containers.remove(container);
	}

	public Theme getTheme() {
		return theme;
	}

	public void setTheme(Theme theme) {
		this.theme = theme;
	}

	public boolean isLayering() {
		return layering;
	}

	public void setLayering(boolean layering) {
		this.layering = layering;
	}

	public List<Container> getContainers() {
		return containers;
	}
	
}
