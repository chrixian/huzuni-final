/**
 *
 */
package net.halalaboos.lib.ui.components;

import net.halalaboos.lib.ui.DefaultComponent;
import net.halalaboos.lib.ui.utils.GLUtils;

import java.awt.*;

/**
 * @author Halalaboos
 * @since Aug 30, 2013
 */
public class Label extends DefaultComponent {
    private String text;
    private Color textColor = Color.WHITE;

    /**
     * @param dimensions
     */
    public Label(String text) {
        super(new Dimension(GLUtils.getStringWidth(text) + 2, 12));
        this.text = text;
    }

    /**
     * @see net.halalaboos.lib.ui.Component#update(java.awt.Rectangle, boolean, boolean)
     */
    @Override
    public void update(Point mouse) {
    }

    /**
     * @see net.halalaboos.lib.ui.Component#onClicked(java.awt.Rectangle, java.awt.Point, int)
     */
    @Override
    public boolean onMousePressed(int x, int y, int buttonId) {
    	return mouseOver;
    }

    /**
     * @see net.halalaboos.lib.ui.Component#onReleased(boolean)
     */
    @Override
    public void onMouseReleased(int x, int y, int buttonId) {
        if (mouseOver)
            invokeActionListeners();
    }

    /**
     * @see net.halalaboos.lib.ui.DefaultComponent#onTyped(int)
     */
    @Override
    public void onKeyTyped(int keyCode, char c) {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Color getTextColor() {
        return textColor;
    }

    public void setTextColor(Color textColor) {
        this.textColor = textColor;
    }

}
