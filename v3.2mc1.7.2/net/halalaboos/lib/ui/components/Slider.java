/**
 *
 */
package net.halalaboos.lib.ui.components;

import net.halalaboos.lib.ui.DefaultComponent;
import net.halalaboos.lib.ui.modes.Draggable;
import net.halalaboos.lib.ui.modes.Scrollable;
import net.halalaboos.lib.ui.utils.GLUtils;

import java.awt.*;
import java.text.DecimalFormat;

import org.lwjgl.input.Mouse;

/**
 * @author Halalaboos
 * @since Aug 16, 2013
 */
public class Slider extends DefaultComponent implements Draggable, Scrollable {
    private float pointSize = 7F;

    private int pointPadding = 1;

    protected DecimalFormat formatter = new DecimalFormat("#.#");

    protected String label;

    protected String valueWatermark = "";

    protected float minVal, maxVal, sliderPercentage, incrementVal;

    protected boolean dragging = false;

    protected Color backgroundColor = new Color(0.3F, 0.3F, 0.3F, 0.75F),
            pointColor = new Color(0F, 0.5F, 1F, 0.75F);
    
    /**
     * @param dimensions
     */
    public Slider(String label, float minVal, float startVal, float maxVal) {
        super(new Dimension(110, 13));
        this.label = label;
        this.minVal = minVal;
        this.maxVal = maxVal;
        this.sliderPercentage = (startVal - minVal) / (maxVal - minVal);
        this.incrementVal = -1F;
        if (GLUtils.getStringWidth(label + formatter.format(getValue())) > this.area.width)
            this.area.width = GLUtils.getStringWidth(label + formatter.format(getValue())) + 2;
    }

    public Slider(String label, float minVal, float startVal, float maxVal, float incrementVal) {
        this(label, minVal, startVal, maxVal);
        this.incrementVal = incrementVal;
    }

    /**
     * @see net.halalaboos.lib.ui.Component#render(boolean)
     */
    @Override
    public void update(Point mouse) {
        constrictSlider();
        handleDragging(mouse);
        handleScrollWheel(mouse);
    }

    /**
     * @return the value that the slider represents.
     */
    public float getValue() {
        float calculatedValue = (sliderPercentage * (maxVal - minVal));
        if (incrementVal == -1)
            return calculatedValue + minVal;
        return ((calculatedValue) - /* This is literally how much the calculated value is off from being incremented by the increment value. */((calculatedValue) % incrementVal)) + minVal;
    }

    /**
     * @return the calculated width of the slider.
     */
    protected float getWidthForPoint() {
        float maxPointForRendering = (float) (area.width - pointSize - pointPadding),
                beginPoint = (pointPadding);
        return maxPointForRendering - beginPoint;
    }

    /**
     * @see net.halalaboos.lib.ui.Component#onClicked(java.awt.Point, int)
     */
    @Override
    public boolean onMousePressed(int x, int y, int buttonId) {
    	if (mouseOver && buttonId == 0) {
    		dragging = true;
    	}
    	return mouseOver;
    }

    protected float getPositionForPoint() {
        return ((float) sliderPercentage * getWidthForPoint());
    }

    /**
     * @see net.halalaboos.lib.ui.Component#onReleased(boolean)
     */
    @Override
    public void onMouseReleased(int x, int y, int buttonId) {
        invokeActionListeners();
    }

    /**
     * @see net.halalaboos.lib.ui.DefaultComponent#onTyped(int)
     */
    @Override
    public void onKeyTyped(int keyCode, char c) {
    }

    /**
     * @see net.halalaboos.lib.ui.modes.Draggable#handleDragging(java.awt.Rectangle, java.awt.Point)
     */
    @Override
    public void handleDragging(Point mouse) {
        if (dragging && Mouse.isButtonDown(0)) {
            //get the difference (in pixels) from the cursor and the beginning of the slider boundaries ( (mouse X) / (slider X) ) and subtract the width of the slider.
            float differenceWithMouseAndSliderBase = (float) (mouse.x - renderableArea.x) - (pointSize / 2F);
            //converted into 0.0F ~ 1.0F percentage
            sliderPercentage = differenceWithMouseAndSliderBase / getWidthForPoint();
        } else
        	dragging = false;
        constrictSlider();
    }
    
    /**
     * Handles the mouse wheel and invokes all scrollable components scroll function only if your mouse is over the component.
     */
    protected void handleScrollWheel(Point mouse) {
		if (mouseOver) {
	    	if (Mouse.hasWheel()) {
				int ammount = Mouse.getDWheel();
				if (ammount > 0)
					ammount = -1;
				else if (ammount < 0)
					ammount = 1;
				else if (ammount == 0)
					return;
				onMouseScroll(mouse, ammount);
			}
		}
    }

    protected void constrictSlider() {
        if (sliderPercentage < 0.0F)
            sliderPercentage = 0.0F;
        if (sliderPercentage > 1.0F)
            sliderPercentage = 1.0F;
    }

    /**
     * @see net.halalaboos.lib.ui.modes.Draggable#getDraggableArea()
     */
    @Override
    public Rectangle getDraggableArea() {
        return area;
    }
    
    /**
     * @return slider nipple.
     * */
    public Rectangle getSliderPoint() {
        return new Rectangle((int) renderableArea.x + (int) getPositionForPoint() + pointPadding, (int) renderableArea.y + pointPadding, (int) pointSize, area.height - pointPadding * 2);
    }


    /**
     * @see net.halalaboos.lib.ui.modes.Draggable#isDragging()
     */
    @Override
    public boolean isDragging() {
        return dragging;
    }

    /**
     * @see net.halalaboos.lib.ui.modes.Draggable#setDragging(boolean)
     */
    @Override
    public void setDragging(boolean dragging) {
        this.dragging = dragging;
    }

    /**
     * Setter for the value.
     * */
    public void setValue(float value) {
        sliderPercentage = ((float) value - minVal) / (maxVal - minVal);
        this.invokeActionListeners();
        constrictSlider();
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public float getMinVal() {
        return minVal;
    }

    public void setMinVal(float minVal) {
        this.minVal = minVal;
    }

    public float getMaxVal() {
        return maxVal;
    }

    public void setMaxVal(float maxVal) {
        this.maxVal = maxVal;
    }

    public float getSliderPercentage() {
        return sliderPercentage;
    }

    public void setSliderPercentage(float sliderPercentage) {
        this.sliderPercentage = sliderPercentage;
    }

    public float getIncrementVal() {
        return incrementVal;
    }

    public void setIncrementVal(float incrementVal) {
        this.incrementVal = incrementVal;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public Color getPointColor() {
        return pointColor;
    }

    public void setPointColor(Color pointColor) {
        this.pointColor = pointColor;
    }

    public void setFormat(String format) {
        this.formatter = new DecimalFormat(format);
    }

    public float getPointSize() {
        return pointSize;
    }

    public void setPointSize(float pointSize) {
        this.pointSize = pointSize;
    }

    public int getPointPadding() {
        return pointPadding;
    }

    public void setPointPadding(int pointPadding) {
        this.pointPadding = pointPadding;
    }

    public String getValueWatermark() {
        return valueWatermark;
    }

    public void setValueWatermark(String valueWatermark) {
        this.valueWatermark = valueWatermark;
    }

	/**
	 * @see net.halalaboos.lib.ui.modes.Scrollable#onMouseScroll(java.awt.Point, int)
	 */
	@Override
	public void onMouseScroll(Point mouse, int amount) {
        if (incrementVal == -1) {
        	setValue(getValue() + 0.1F * amount);
        } else {
        	setValue(getValue() + incrementVal * amount);
        }
        invokeActionListeners();
	}

}
