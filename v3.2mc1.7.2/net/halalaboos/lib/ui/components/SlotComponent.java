/**
 *
 */
package net.halalaboos.lib.ui.components;

import net.halalaboos.lib.ui.DefaultComponent;
import net.halalaboos.lib.ui.modes.Draggable;
import net.halalaboos.lib.ui.modes.Scrollable;
import net.halalaboos.lib.ui.utils.GLUtils;

import java.awt.*;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.lwjgl.input.Mouse;

/**
 * @author Halalaboos
 * @since Aug 18, 2013
 */
public abstract class SlotComponent <T> extends DefaultComponent implements Scrollable, Draggable {
    private final int SLIDER_POINT_WIDTH = 6;
    private final int SLIDER_POINT_HEIGHT = 12;
    private final int SLIDER_PADDING = 1;
    
    protected int componentPadding = 1;
    protected List<T> components = new CopyOnWriteArrayList<T>();
    protected float scrollPercentage = 0;
    protected T clickedComponent;
    protected Color backgroundColor = new Color(0.3F, 0.3F, 0.3F, 0.75F),
            sliderColor = new Color(0F, 0.5F, 1F, 0.75F);
    protected boolean dragging = false, renderBackground = true, hasSlider = true;
    
    /**
     * @param dimensions
     */
    public SlotComponent(Dimension dimensions) {
        super(dimensions);

    }

    /**
     * @see net.halalaboos.lib.ui.Component#render(boolean, boolean)
     */
    @Override
    public void update(Point mouse) {
    	handleDragging(mouse);
    	handleScrollWheel(mouse);
    }

    /**
     * Handles the mouse wheel and invokes all scrollable components scroll function only if your mouse is over the component.
     */
    protected void handleScrollWheel(Point mouse) {
		if (mouseOver) {
	    	if (Mouse.hasWheel()) {
				int ammount = Mouse.getDWheel();
				if (ammount > 0)
					ammount = -1;
				else if (ammount < 0)
					ammount = 1;
				else if (ammount == 0)
					return;
				onMouseScroll(mouse, ammount);
			}
		}
    }
    
    public void renderElements(Rectangle area, boolean mouseOver, boolean mouseDown) {
        if (!components.isEmpty()) {
            int yPos = 2;
            Rectangle sliderPoint = getDraggableArea();
            Point mouse = new Point(GLUtils.getMouseX(), GLUtils.getMouseY());
            int scrolledIndex = getScrolledIndex();
            for (int i = 0; scrolledIndex + i < components.size() && i < getMaxRenderableObjects() && scrolledIndex + i >= 0; i++) {
                T component = components.get(scrolledIndex + i);
                Rectangle componentArea = new Rectangle(area.x + 2, area.y + yPos, area.width - (hasEnoughToScroll() && hasSlider ? (sliderPoint.width) : 0) - 4, getElementHeight());
                render(scrolledIndex + i, component, componentArea, (mouseOver && componentArea.contains(mouse)) && (clickedComponent == null ? true : clickedComponent == component), mouseDown && clickedComponent == component);
                yPos += componentArea.height + componentPadding;
            }
        }
    }

    /**
     * @see net.halalaboos.lib.ui.Component#onClicked(java.awt.Point, int)
     */
    @Override
    public boolean onMousePressed(int x, int y, int buttonId) {
    	if (mouseOver) {
    		if (this.getDraggableArea().contains(x, y)) {
    			dragging = true;
    		}
	        int yPos = 2;
	        if (!components.isEmpty()) {
	            Rectangle sliderPoint = getDraggableArea();
	            int scrolledIndex = getScrolledIndex();
	            for (int i = 0; scrolledIndex + i < components.size() && i < getMaxRenderableObjects() && scrolledIndex + i >= 0; i++) {
	                T component = components.get(scrolledIndex + i);
	                Rectangle componentArea = new Rectangle(renderableArea.x + 2, renderableArea.y + yPos, renderableArea.width - (hasEnoughToScroll() && hasSlider ? (sliderPoint.width) : 0) - 4, getElementHeight());
	                if (componentArea.contains(x, y)) {
	                    clickedComponent = component;
	                    onClicked(scrolledIndex + i, component, componentArea, new Point(x, y), buttonId);
	                    break;
	                }
	                yPos += componentArea.height + componentPadding;
	            }
	        }
    	}
    	return mouseOver;
    }

    /**
     * @see net.halalaboos.lib.ui.Component#onReleased(boolean)
     */
    @Override
    public void onMouseReleased(int x, int y, int buttonId) {
    	if (mouseOver && clickedComponent != null) {
           
    		int index = components.indexOf(clickedComponent);
    		
            int yPos = /* This represents 'i' */(index - getScrolledIndex()) * (getElementHeight() + componentPadding);

            Rectangle componentArea = new Rectangle(renderableArea.x + 2, renderableArea.y + yPos, renderableArea.width - (hasEnoughToScroll() && hasSlider ? (getDraggableArea().width) : 0) - 4, getElementHeight());
            
            if (componentArea.contains(x, y)) {
            	onReleased(index, clickedComponent, new Point(GLUtils.getMouseX(), GLUtils.getMouseY()));
            }
            
            clickedComponent = null;
    	}
    }

    /**
     * Fired when the element is rendered within the scrollable area.
     */
    public abstract void render(int index, T object, Rectangle boundaries, boolean mouseOver, boolean mouseDown);

    /**
     * Fired when the element is clicked.
     */
    public abstract void onClicked(int index, T object, Rectangle boundaries, Point mouse, int buttonID);

    /**
     * Fired when the element is released.
     */
    public abstract void onReleased(int index, T object, Point mouse);

    /**
     * @return Width of the element.
     */
    public abstract int getElementHeight();

    /**
     * @see net.halalaboos.lib.ui.modes.Scrollable#onMouseScroll(java.awt.Point, int)
     */
    @Override
    public void onMouseScroll(Point mouse, int amount) {
        if (clickedComponent == null) {
        	if (hasEnoughToScroll()) {
        		scrollPercentage += ((float) amount / (float) getObjectsNotRendered());
            	keepSafe();
        	}
        }
    }

    private void keepSafe() {
        if (scrollPercentage > 1)
            scrollPercentage = 1;
        if (scrollPercentage < 0)
            scrollPercentage = 0;
    }

    private int getObjectsNotRendered() {
        return components.size() - getMaxRenderableObjects();
    }

    private int getScrolledIndex() {
        int maxSize = getObjectsNotRendered();
        return (int) (scrollPercentage * maxSize);
    }

    private int getMaxRenderableObjects() {
        return (int) Math.floor(renderableArea.height / (getElementHeight() + componentPadding));
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public List<T> getComponents() {
        return components;
    }

    public void setComponents(List<T> component) {
        this.components = component;
        keepSafe();
    }

    public void add(T component) {
        components.add(component);
        keepSafe();
    }

    public void remove(T component) {
        components.remove(component);
        keepSafe();
    }

    /**
     * @see net.halalaboos.lib.ui.modes.Draggable#getDraggableArea()
     */
    @Override
    public Rectangle getDraggableArea() {
        int scrollbarY = (int) (this.scrollPercentage * getHeightForPoint());
        return new Rectangle(renderableArea.x + renderableArea.width - SLIDER_POINT_WIDTH - SLIDER_PADDING,
        		renderableArea.y + scrollbarY + SLIDER_PADDING,
                SLIDER_POINT_WIDTH,
                SLIDER_POINT_HEIGHT);
    }

    /**
     * @see net.halalaboos.lib.ui.modes.Draggable#isDragging()
     */
    @Override
    public boolean isDragging() {
        return dragging;
    }

    /**
     * @see net.halalaboos.lib.ui.modes.Draggable#setDragging(boolean)
     */
    @Override
    public void setDragging(boolean dragging) {
        this.dragging = dragging;
    }

    /**
     * @see net.halalaboos.lib.ui.modes.Draggable#handleDragging(java.awt.Rectangle, java.awt.Point)
     */
    @Override
    public void handleDragging(Point mouse) {
    	if (dragging && Mouse.isButtonDown(0)) {
        	if (!hasEnoughToScroll() || !hasSlider) {
        		dragging = false;
        		return;
        	}
            //get the difference (in pixels) from the cursor and the beginning of the slider boundaries ( (mouse X) / (slider X) ) and subtract the width of the slider.
            float differenceWithMouseAndSliderBase = (float) (mouse.y - renderableArea.y) - (SLIDER_POINT_HEIGHT / 2F);

            //converted into 0.0F ~ 1.0F percentage
            scrollPercentage = differenceWithMouseAndSliderBase / getHeightForPoint();
            this.keepSafe();
        } else 
        	dragging = false;
    }
    
    public boolean hasEnoughToScroll() {
    	return getMaxRenderableObjects() < components.size();
    }

    protected float getHeightForPoint() {
        float maxPointForRendering = (float) (renderableArea.height - SLIDER_POINT_HEIGHT - SLIDER_PADDING),
                beginPoint = (SLIDER_PADDING);
        return maxPointForRendering - beginPoint;
    }

    public Color getSliderColor() {
        return sliderColor;
    }

    public void setSliderColor(Color sliderColor) {
        this.sliderColor = sliderColor;
    }

	public int getComponentPadding() {
		return componentPadding;
	}

	public void setComponentPadding(int componentPadding) {
		this.componentPadding = componentPadding;
	}

	public boolean shouldRenderBackground() {
		return renderBackground;
	}

	public void setRenderBackground(boolean renderBackground) {
		this.renderBackground = renderBackground;
	}

	public boolean hasSlider() {
		return hasSlider;
	}

	public void setHasSlider(boolean hasSlider) {
		this.hasSlider = hasSlider;
	}
}
