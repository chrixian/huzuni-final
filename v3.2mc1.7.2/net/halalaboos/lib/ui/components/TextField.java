/**
 *
 */
package net.halalaboos.lib.ui.components;

import com.darkmagician6.morbidlib.utils.timer.Timer;

import net.halalaboos.lib.ui.DefaultComponent;
import net.halalaboos.lib.ui.utils.GLUtils;
import net.minecraft.util.ChatAllowedCharacters;

import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import java.awt.*;

/**
 * @author Halalaboos
 * @since Aug 18, 2013
 */
public class TextField extends DefaultComponent {
    private final Timer timer = new Timer();
    private String highlight = "_";

    protected int maxLength = 100;

    protected Color textColor = Color.WHITE,
            backgroundColor = new Color(0.3F, 0.3F, 0.3F, 0.75F);

    protected String text = "";

    protected boolean enabled = false;
    
    /**
     * @param dimensions
     */
    public TextField() {
        super(new Dimension(100, 12));
    }

    /**
     * @see net.halalaboos.lib.ui.Component#render(boolean, boolean)
     */
    @Override
    public void update(Point mouse) {
    }

    /**
     * @return blinking '|' thingy that appears at the end of text fields.
     */
    public String getSelection() {
        if (timer.hasReach(250)) {
            if (highlight.equals("_"))
                highlight = "";
            else
                highlight = "_";
            timer.reset();
        }
        return enabled ? highlight : "";
    }

    public String getTextForRender(Rectangle area) {
        if (GLUtils.getStringWidth(text + "_") > area.width) {
            String text = "";
            char[] chars = this.text.toCharArray();
            for (int index = chars.length - 1; index >= 0; index--) {
                if (GLUtils.getStringWidth(chars[index] + text + "_") >= area.width)
                    break;
                text = chars[index] + text;
            }
            return text;
        }
        return text;
    }

    /**
     * @see net.halalaboos.lib.ui.Component#onClicked(java.awt.Point, int)
     */
    @Override
    public boolean onMousePressed(int x, int y, int buttonId) {
    	if (!mouseOver)
    		enabled = false;
    	return mouseOver;
    }

    /**
     * @see net.halalaboos.lib.ui.Component#onReleased(boolean)
     */
    @Override
    public void onMouseReleased(int x, int y, int buttonId) {
        if (mouseOver)
        	enabled = true;
    }

    /**
     * @see net.halalaboos.lib.ui.DefaultComponent#onTyped(int)
     */
    @Override
    public void onKeyTyped(int keyCode, char c) {
    	if (enabled) {
	        if (keyCode == Keyboard.KEY_V) {
	            if (Keyboard.isKeyDown(Keyboard.KEY_LCONTROL) || Keyboard.isKeyDown(Keyboard.KEY_RCONTROL)) {
	                if (Sys.getClipboard() != null) {
	                    text += Sys.getClipboard();
	                }
	            }
	        }
	
	        if (keyCode == Keyboard.KEY_BACK) {
	            minus(1);
	        } else {
		        if (ChatAllowedCharacters.isAllowedCharacter(Keyboard.getEventCharacter()))
		            text += Keyboard.getEventCharacter();
		        if (keyCode == Keyboard.KEY_RETURN)
		            invokeActionListeners();
		
		
		        if (text.length() > maxLength && maxLength > 0)
		            text = text.substring(0, maxLength);
	        }
	        invokeKeyListeners(keyCode, c);
    	}
    }

    private void minus(int ammount) {
        if ((text.length() - ammount) >= 0)
            text = text.substring(0, text.length() - ammount);
    }

    public int getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    public Color getTextColor() {
        return textColor;
    }

    public void setTextColor(Color textColor) {
        this.textColor = textColor;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
