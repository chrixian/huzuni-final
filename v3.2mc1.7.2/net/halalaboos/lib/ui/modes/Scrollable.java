/**
 *
 */
package net.halalaboos.lib.ui.modes;

import java.awt.*;

/**
 * @author Halalaboos
 * @since Aug 16, 2013
 */
public interface Scrollable {

	/**
	 * Handle all your mouse scrolling calculations here.
	 * */
    public void onMouseScroll(Point mouse, int amount);

}
