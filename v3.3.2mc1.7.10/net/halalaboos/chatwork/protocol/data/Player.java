package net.halalaboos.chatwork.protocol.data;


public class Player implements User {

	private final int id;
	
	private int permissions = 0;
	
	private UserInfo info;
	
	public Player(int id, UserInfo info) {
		this.id = id;
		this.info = info;
	}
	
	@Override
	public long getId() {
		return id;
	}

	@Override
	public String toString() {
		return info.getUsername();
	}

	@Override
	public UserInfo getInfo() {
		return info;
	}

	@Override
	public void setInfo(UserInfo info) {
		this.info = info;
	}
}
