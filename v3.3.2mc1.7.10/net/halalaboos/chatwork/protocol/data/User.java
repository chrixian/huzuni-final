package net.halalaboos.chatwork.protocol.data;

public interface User {

	long getId();
	
	UserInfo getInfo();
	
	void setInfo(UserInfo info);
}
