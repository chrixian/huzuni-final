package net.halalaboos.chatwork.server;

public final class Permissions {

	private Permissions() {
		
	}
	
	public static final int ADMIN = 10;
	
	public static final int MOD = 9;
	
	public static final int DONGLORD = 8;
	
	public static final int POTATO = 7;

	public static final int USER = 0;
	
	public static String getPrefix(int permissions) {
		switch (permissions) {
		case ADMIN:
			return "admin";
		case MOD:
			return "mod";
		case DONGLORD:
			return "donglord";
		case POTATO:
			return "potato";
		case USER:
			return "scrub";
		default:
			return "scrub";
		}
	}
	
}
