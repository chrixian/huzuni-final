package net.halalaboos.chatwork.server.console;

import net.halalaboos.chatwork.server.ChatworkClient;

public interface Command {

	String getDescription();
	
	String[] getAliases();
	
	/**
	 * @return True if the input is valid.
	 * */
	boolean isValid(String input);
	
	void execute(ChatworkClient sender, String input, String[] args);
	
	void help(ChatworkClient sender);
	
	int getPermission();
}
