package net.halalaboos.chatwork.server.console.commands;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.chatwork.server.ChatworkClient;
import net.halalaboos.chatwork.server.console.Command;
import net.halalaboos.chatwork.server.console.CommandManager;
import net.halalaboos.chatwork.utils.StringUtils;


public class Help implements Command {

	private final int COMMAND_PER_PAGE = 8;

	@Override
	public String[] getAliases() {
		return new String[] { "help" };
	}

	@Override
	public String getDescription() {
		return "Gives you all of the help commands.";
	}

	@Override
	public void execute(ChatworkClient sender, String originalString, String[] args) {
		if (args != null) {
			if (StringUtils.isInteger(args[0]))
				listHelp(sender, Integer.parseInt(args[0]));
		} else
			listHelp(sender, 1);
	}
	
	/**
	 * @return Lists all commands for the page.
	 * */
	public void listHelp(ChatworkClient sender, int wantedPage) {
		int pages = getPages();
		List<Command> commandsOnPage = getCommandsOnPage(sender, wantedPage);
		if (commandsOnPage.isEmpty() || wantedPage <= 0) {
			sender.sendMessage("'" + wantedPage + "' is an invalid page!");
			return;
		}
		sender.sendMessage("===[ " + "Help (" + wantedPage + " / " + pages + ") ]===");
		for (Command command : commandsOnPage)
			sender.sendMessage(command.getAliases()[0] + " - " + command.getDescription());
	}

	/**
	 * @return All commands for the page.
	 * */
	private List<Command> getCommandsOnPage(ChatworkClient sender, int page) {
		List<Command> tempList = new ArrayList<Command>();
		int pageCount = 1, commandCount = 0;

		for (int i = 0; i < CommandManager.getCommands().size(); i++) {
			Command command = CommandManager.getCommands().get(i);
			if (command != this && sender.getInfo().getPermissions() >= command.getPermission()) {
				if (commandCount >= COMMAND_PER_PAGE) {
					pageCount++;
					commandCount = 0;
				}
				if (pageCount == page)
					tempList.add(command);
				commandCount++;
			}
		}
		return tempList;
	}

	/**
	 * @return Amount of pages of commands we have.
	 * */
	private int getPages() {
		boolean isDividedEvenly = false;
		return (int) Math.ceil((float) (CommandManager.getCommands().size() - 1) / (float) COMMAND_PER_PAGE - (isDividedEvenly ? 1 : 0));
	}

	@Override
	public boolean isValid(String input) {
		return false;
	}

	@Override
	public void help(ChatworkClient sender) {
	}

	@Override
	public int getPermission() {
		return 0;
	}

}
