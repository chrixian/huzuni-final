package net.halalaboos.huzuni;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.halalaboos.huzuni.util.FileUtils;

public class NameProtect {


	private static final File saveFile = new File(Huzuni.SAVE_DIRECTORY, "NameProtect.txt");

	private static final List<String[]> names = new ArrayList<String[]>();
	
	private NameProtect() {
		
	}
	
	public static void load() {
		names.clear();
		List<String> lines = FileUtils.readFile(saveFile);
		for (String line : lines) {
			String[] name = new String[] { line.substring(0, line.indexOf(":")), line.substring(line.indexOf(":") + 1) };
			names.add(name);
		}
	}
	
	public static void save() {
		List<String> lines = new ArrayList<String>();
		for (String[] name : names)
			lines.add(name[0] + ":" + name[1]);
		FileUtils.writeFile(saveFile, lines);
	}
	
	public static void add(String name, String alias) {
		for (String[] name1 : names)
			if (name1[0].equalsIgnoreCase(name)) {
				name1[1] = alias;
				return;
			}
		names.add(new String[] { name, alias });
	}
	
	public static String[] getName(String nameOrAlias) {
		for (String[] name : names)
			if (name[0].equalsIgnoreCase(nameOrAlias) || name[1].equalsIgnoreCase(nameOrAlias))
				return name;
		return null;
	}
	
	public static void remove(String name) {
		for (String[] name1 : names)
			if (name1[0].equalsIgnoreCase(name) || name1[1].equalsIgnoreCase(name))
				names.remove(name1);
	}
	
	public static String replace(String text) {
		for (String[] name : names)
			text = text.replaceAll("(?i)" + name[0], name[1]);
		return text;
	}

	public static List<String[]> getNames() {
		return names;
	}
}
