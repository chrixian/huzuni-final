package net.halalaboos.huzuni;

import net.halalaboos.chatwork.Chatwork;
import net.halalaboos.huzuni.command.CommandManager;
import net.halalaboos.huzuni.gui.GuiManager;
import net.halalaboos.huzuni.plugin.PluginManager;
import net.halalaboos.huzuni.plugin.keybinds.WindowOpener;
import net.halalaboos.huzuni.plugin.plugins.render.StatusHUD;
import net.halalaboos.huzuni.plugin.plugins.MiddleClickFriends;
import net.halalaboos.huzuni.plugin.plugins.world.Xray;
import net.halalaboos.huzuni.plugin.value.ValueManager;
import net.halalaboos.huzuni.util.ThreadCheckVersion;
import net.halalaboos.huzuni.util.ThreadCheckVip;
import net.halalaboos.huzuni.util.ThreadDownloadDonators;
import net.halalaboos.huzuni.util.ThreadReadChangelog;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.util.Session;

public final class Wrapper {

	private Wrapper() {
		/**
		 * EntityClientHook hook
		 * - PlayerControllerMP.func_147493_a replaced with our hook
		 *
		 * Packet Events
		 * - NetworkManager.processReceivedPackets for READING packets.
		 * - NetHandlerPlayClient.addToSendQueue for SENDING packets.
		 *
		 * Motion 
		 * - EntityClientPlayerMP.sendMotionUpdate for both pre and post.
		 *
		 * Xray
		 * - Block.getRenderBlockPass for changing the render block pass
		 * - RenderBlocks.renderStandardBlock for rendering xray blocks
		 * - Tessellator.setColorOpaque_F for setting block alpha
		 *
		 * Nametags
		 * - Render.func_147906_a (renderNamePlate)
		 * - RenderPlayer removed distance check
		 *
		 * Renderables
		 * - EntityRenderer
		 *
		 * Huzuni button
		 * - GuiIngameMenu
		 * 
		 * InventoryHelper
		 * - GuiInventory
		 * - GuiCrafting
		 * 
		 * NoSlowdown
		 * EntityPlayerSP
		 * */
	}

	/**
	 * Inside of Minecraft.startGame
	 * */
	public static void onGameStart() {
		Huzuni.setupDefaultConfig();
		PluginManager.setupPlugins();
		Huzuni.addKeybind(new WindowOpener());
		Huzuni.loadConfig();
		ValueManager.load();

		PluginManager.loadPlugins();
		CommandManager.loadCommands();

		GuiManager.loadWindows();

		Huzuni.loadFriends();
		Huzuni.loadMacros();
		Huzuni.loadKeybinds();
		NameProtect.load();
		Xray.instance.load();
		new ThreadCheckVip().start();
		new ThreadDownloadDonators().start();
		new ThreadCheckVersion().start();
		new ThreadReadChangelog().start();
	}

	/**
	 * Inside of Minecraft.shutdownMinecraftApplet
	 * */
	public static void onGameEnd() {
		Huzuni.CONFIG.put("First use", false);
		Huzuni.saveConfig();
		ValueManager.save();
		PluginManager.savePlugins();
		GuiManager.saveWindows();
		Huzuni.saveFriends();
		Huzuni.saveMacros();
		Huzuni.saveKeybinds();
		NameProtect.save();
		Xray.instance.save();
	}

	/**
	 * Inside of Minecraft.setServerData
	 * */
	public static void onJoinServer(ServerData serverData) {
		if (serverData != null) {
			Chatwork.instance.getInfo().setLocation(serverData.serverIP);
			if (Chatwork.instance.isConnected())
				Chatwork.instance.writeInfo();
		}
	}
	
	/**
	 * Inside of Minecraft.loadWorld
	 * */
	public static void onLoadWorld(WorldClient world) {
		if (Huzuni.CONFIG.getBoolean("First use")) {
			Huzuni.addChatMessage("Welcome to huzuni!");
			Huzuni.addChatMessage("Press right shift to open the gui.");
			Huzuni.addChatMessage("Type '.help' for commands!");
			Huzuni.CONFIG.put("First use", false);
		}
	}
	
	/**
	 * Inside of Minecraft constructor
	 * Inside of Minecraft.setSession
	 * */
	public static void onChangeSession(Session session) {
		Chatwork.instance.getInfo().setIngameUsername(session.getUsername());
		if (Chatwork.instance.isConnected())
			Chatwork.instance.writeInfo();
	}
	
	/**
	 * Inside of Minecraft.runTick
	 * */
	public static void onKeyTyped(int keyCode, char c) {
		if (Huzuni.isEnabled()) {
			String macro = Huzuni.MACROS[keyCode];
			if (macro != null && !macro.isEmpty())
				CommandManager.processCommand(macro);
			Huzuni.runKeybinds(keyCode);
			GuiManager.getIngameTheme().onKeyTyped(keyCode);
		}
	}

	public static void onMouseClicked(int buttonID) {
		if (Huzuni.isEnabled()) {
			if (MiddleClickFriends.instance.isEnabled()) {
				MiddleClickFriends.instance.onClick(buttonID);
			}
		}
	}

	/**
	 * Inside of GuiIngame.renderGameOverlay
	 * */
	public static void renderIngame(Minecraft mc, int screenWidth, int screenHeight) {
		if (Huzuni.isEnabled()) {
			GuiManager.getIngameTheme().render(mc, screenWidth, screenHeight);
			if(StatusHUD.instance.isEnabled()) {
				StatusHUD.instance.renderHUD(screenWidth, screenHeight);
			}
			Minimap.render(mc, screenWidth, screenHeight, true, true);
		}
	}
}
