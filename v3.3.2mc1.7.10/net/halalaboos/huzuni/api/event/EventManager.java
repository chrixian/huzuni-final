package net.halalaboos.huzuni.api.event;

import java.util.ArrayList;
import java.util.List;

public abstract class EventManager <T extends Event, L extends Listener> {

	private List<L> listeners = new ArrayList<L>();
	
	public void registerListener(L listener) {
		if (!listeners.contains(listener))
			listeners.add(listener);
	}
	
	public void unregisterListener(L listener) {
		if (listeners.contains(listener))
			listeners.remove(listener);
	}
	
	public void clearListeners() {
		listeners.clear();
	}
	
	public void fire(T event) {
		for (int i = 0; i < listeners.size(); i++) {
			try {
				invoke(listeners.get(i), event);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	protected abstract void invoke(L listener, T event);
	
}
