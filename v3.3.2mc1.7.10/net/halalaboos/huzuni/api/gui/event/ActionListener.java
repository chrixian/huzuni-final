package net.halalaboos.huzuni.api.gui.event;

import net.halalaboos.huzuni.api.gui.Component;

public interface ActionListener <T extends Component> {

	void onAction(T component);
	
}
