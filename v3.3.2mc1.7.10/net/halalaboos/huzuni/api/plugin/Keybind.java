package net.halalaboos.huzuni.api.plugin;

public interface Keybind {

	String getName();
	
	String getDescription();
	
	void setKeybind(int keyCode);
	
	int getKeycode();
	
	boolean isBound();
	
	String getKeyName();
	
	void pressed();
	
	boolean isPressed();
}
