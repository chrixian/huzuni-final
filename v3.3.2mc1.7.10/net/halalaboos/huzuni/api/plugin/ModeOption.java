package net.halalaboos.huzuni.api.plugin;

import org.w3c.dom.Element;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.util.Timer;

public class ModeOption extends Option {

	private String[] modes;
	
	private int selected = 0;
	
	private final Timer timer = new Timer();
	
	private boolean carot = true;
	
	public ModeOption(String name, String... modes) {
		super(name);
		this.modes = modes;
	}

	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
		return this.isPointInside(x, y);
	}

	@Override
	public void mouseReleased(int x, int y, int buttonId) {
		if (this.isPointInside(x, y)) {
			swapModes();
		}
	}

	@Override
	public void render(Theme theme, int index, float[] area, boolean mouseOver,
			boolean mouseDown) {
		if (mouseOver)
			toggleCarot();
		theme.renderSlot(0, 0, index, area, false, mouseOver, mouseDown);
		String render = carot && mouseOver ? ">>" + modes[selected] + "<<" : modes[selected];
		Huzuni.drawString(render, area[0] + (area[2] / 2) - (Huzuni.getStringWidth(render) / 2), area[1] + 2, 0xFFFFFF);
	}
	
	public boolean toggleCarot() {
		if (timer.hasReach(450)) {
			carot = !carot;
			timer.reset();
		}
        return carot;
	}
	
	private void swapModes() {
		selected++;
		if (selected >= modes.length)
			selected = 0;
		if (selected < 0)
			selected = modes.length - 1;
	}

	public String[] getModes() {
		return modes;
	}

	public void setModes(String[] modes) {
		this.modes = modes;
	}

	public int getSelected() {
		return selected;
	}

	public void setSelected(int selected) {
		this.selected = selected;
	}

	@Override
	public void load(Element element) {
		int value = Integer.parseInt(element.getAttribute(name.replaceAll(" ", "_")));
		this.selected = value;
	}

	@Override
	public void save(Element element) {
		element.setAttribute(name.replaceAll(" ", "_"), "" + selected);
	}

	

}
