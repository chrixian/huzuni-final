package net.halalaboos.huzuni.command;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.command.commands.*;
import net.minecraft.util.EnumChatFormatting;

public final class CommandManager {

	private static final List<Command> commands = new ArrayList<Command>();
	
	private CommandManager() {
		
	}
	
	public static void loadCommands() {
		registerCommand(new Toggle());
		registerCommand(new Mods());
		registerCommand(new Help());
		registerCommand(new Add());
		registerCommand(new Kill());
		registerCommand(new MassMessage());
		registerCommand(new Remove());
		registerCommand(new GetIP());
		registerCommand(new GetCoords());
		registerCommand(new Vclip());
		registerCommand(new RemoteView());
		registerCommand(new Enchant());
		registerCommand(new Lenny());
		registerCommand(new MacroCmd());
		registerCommand(new Commands());
		registerCommand(new Drop());
		registerCommand(new Say());
		registerCommand(new AccountCheck());
		registerCommand(new TimeSpeed());
		registerCommand(new AllOff());
		registerCommand(new CraftingHelp());
		registerCommand(new ItemEnhancer());
		registerCommand(new Invsee());
		registerCommand(new Damage());
	}

	public static void registerCommand(Command command) {
		commands.add(command);
	}

	public static void unregisterCommand(Command command) {
		commands.remove(command);
	}

	public static List<Command> getCommands() {
		return commands;
	}

	public static boolean processCommand(String input) {
		String commandName = input.contains(" ") ? input.split(" ")[0] : input;
		for (Command command : getCommands()) {
			for (String alias : command.getAliases()) {
				if (alias.toLowerCase().equals(commandName.toLowerCase())) {
					if (input.contains(" ")) {
						if (input.split(" ")[1].equalsIgnoreCase("aliases")) {
							listAliases(command);
							return true;
						}
					}
					tryCommand(command, input);
					return true;
				}
			}
		}
		Huzuni.addChatMessage("'" + input + "' is not recognized as a command.");
		return false;
	}

	private static void listAliases(Command command) {
		String aliasList = "Available aliases: ";
		String[] aliases = command.getAliases();
		for (int i = 0; i < aliases.length; i++) {
			String alias = aliases[i];
			aliasList+= EnumChatFormatting.GOLD + alias + EnumChatFormatting.RESET + (i != aliases.length ? ", " : "");
		}
		Huzuni.addChatMessage(aliasList);
	}

	public static void tryCommand(Command command, String input) {
		try {
			String[] args = input.contains(" ") ? input.substring(input.indexOf(" ") + 1).split(" ") : null;
			command.run(input, args);
		} catch (Exception e) {
			listHelp(command);
		}
	}

	private static void listHelp(String reason, Command command) {
		Huzuni.addChatMessage(reason);
		for (String helpMessage : command.getHelp())
			Huzuni.addChatMessage(helpMessage);
	}

	private static void listHelp(Command command) {
		for (String helpMessage : command.getHelp())
			Huzuni.addChatMessage(helpMessage);
	}
}