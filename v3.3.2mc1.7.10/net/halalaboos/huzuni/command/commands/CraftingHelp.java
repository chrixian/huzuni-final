package net.halalaboos.huzuni.command.commands;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.chatwork.utils.StringUtils;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.util.GameUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.event.ClickEvent;
import net.minecraft.event.HoverEvent;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.item.crafting.ShapelessRecipes;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

public class CraftingHelp implements Command {

	private final Minecraft mc = Minecraft.getMinecraft();
	
	@Override
	public String[] getAliases() {
		return new String[] { "craftable" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "craftable", "craftable <item>" };
	}

	@Override
	public String getDescription() {
		return "Lists all craftable items with your inventory.";
	}

	@Override
	public void run(String input, String[] args) {
		if (args != null) {
			Item item = GameUtils.getItemFromName(StringUtils.getAfter(input, 1));
			if (item != null) {
				ItemStack itemStack = new ItemStack(item);
				IRecipe recipe = GameUtils.findRecipe(itemStack);
				if (recipe != null)
					Huzuni.addChatMessage("You " + (GameUtils.isCraftable(recipe) ? "are able to craft '" : "are not able to craft '") + itemStack.getDisplayName() + "'.");
				else
					Huzuni.addChatMessage("'" + itemStack.getDisplayName() + "' is not craftable!");
			} else
				Huzuni.addChatMessage("Cannot find '" + StringUtils.getAfter(input, 1) + "'.");
			return;
		}
		String craftable = "";
		for (IRecipe recipe : (List<IRecipe>) CraftingManager.getInstance().getRecipeList())
			if (GameUtils.isCraftable(recipe))
				craftable += "\2476" + recipe.getRecipeOutput().getDisplayName() + "\247f, ";
		Huzuni.addChatMessage("Craftable items: " + (craftable.isEmpty() ? "none" : craftable.substring(0, craftable.length() - 2)));
	}
}
