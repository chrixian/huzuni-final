package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;

public class Drop implements Command {

	@Override
	public String[] getAliases() {
		return new String[] { "drop" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "drop <hotbar/inventory>" };
	}

	@Override
	public String getDescription() {
		return "Drops all hotbar items";
	}

	@Override
	public void run(String input, String[] args) {
		if (args != null && args.length > 0) {
			String mode = args[0].toLowerCase();
			if (mode.startsWith("h")) {
				new DropThread(false).start();
			} else if (mode.startsWith("i")) {
				new DropThread(true).start();

			}
		} else {
			new DropThread(true).start();
		}
	}
	
	private class DropThread extends Thread {

		private final boolean inventory;
		
		public DropThread(boolean inventory) {
			this.inventory = inventory;
		}
		
        public void run() {
            for (int o = inventory ? 0 : 36; o < 45; o++) {
                ItemStack item = Minecraft.getMinecraft().thePlayer.inventoryContainer.getSlot(o).getStack();
                if (item == null)
                    continue;
                else {
                    clickSlot(o, false);
                    clickSlot(-999, false);
                }
            }
            Huzuni.addChatMessage("Dropped!");
        }

        private void clickSlot(int slot, boolean shiftClick) {
        	Minecraft.getMinecraft().playerController.windowClick(Minecraft.getMinecraft().thePlayer.inventoryContainer.windowId, slot, 0, shiftClick ? 1 : 0, Minecraft.getMinecraft().thePlayer);
            try {
                Thread.sleep(35L);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
