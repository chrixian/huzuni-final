package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.plugin.PluginManager;
import net.minecraft.util.EnumChatFormatting;

/**
 * @author brudin
 * @version 1.0
 * @since 4/10/14
 */
public class Mods implements Command {

	/**
	 * Renamed to 'mods' instead of 'plugins' to avoid confusion
	 * since people are dumb and would think 'woa this makese server plugin haha!!'
	 *
	 * Though I should make an actual .pl that would list the server plugins.
	 * That would be neato!
	 */

	@Override
	public String[] getAliases() {
		return new String[] {"mods", "hacks"};
	}

	@Override
	public String[] getHelp() {
		return new String[] { "<none>" };
	}

	@Override
	public String getDescription() {
		return "Lists all of the mods.";
	}

	@Override
	public void run(String input, String[] args) {

		String allPlugins = "Mods (" + PluginManager.getDefaultPlugins().size() + "): ";

		for(DefaultPlugin plugin : PluginManager.getDefaultPlugins()) {
			allPlugins += (plugin.isEnabled() ? EnumChatFormatting.GREEN : EnumChatFormatting.RED) +
					plugin.getName() + EnumChatFormatting.RESET + ", ";
		}

		Huzuni.addChatMessage(allPlugins.substring(0, allPlugins.length() - 2));
	}
}
