package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.NameProtect;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.util.StringUtils;

public class Remove implements Command {

    @Override
    public String[] getAliases() {
        return new String[] { "remove", "del" };
    }

    @Override
    public String[] getHelp() {
        return new String[] { "remove <username>" };
    }

    @Override
    public String getDescription() {
        return "Removes users from the friends list.";
    }

    @Override
    public void run(String input, String[] args) {
    	String username = StringUtils.getAfter(input, 1);
        String[] name = NameProtect.getName(username);
        if (name != null) {
            NameProtect.remove(username);
            NameProtect.save();
 	        Huzuni.addChatMessage("'" + name[0] + "' is no longer referred to as '" + name[1] + "'!");
            removeFriend(name[0].toLowerCase());
        } else
        	removeFriend(username);
    }
    
    private void removeFriend(String username) {
    	 boolean successful = Huzuni.isFriend(username);
         if (successful) {
 	        Huzuni.removeFriend(username);
 	        Huzuni.addChatMessage("Friend '" + username + "' " + (successful ? "removed." : "is not in your friends list!"));
 	        Huzuni.saveFriends();
         }
    }
}
