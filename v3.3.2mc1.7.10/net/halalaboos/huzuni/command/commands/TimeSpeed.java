package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.util.StringUtils;
import net.minecraft.client.Minecraft;

public class TimeSpeed implements Command {

	@Override
	public String[] getAliases() {
		return new String[] { "timespeed", "speed", "ts", "timer" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "timespeed <speed|reset>" };
	}

	@Override
	public String getDescription() {
		return "Sets your time speed.";
	}

	@Override
	public void run(String input, String[] args) {
		if(args[0].equalsIgnoreCase("reset")) {
			Minecraft.getMinecraft().timer.timerSpeed = 1F;
			Huzuni.addChatMessage("Timer speed reset.");
		} else {
			double speed = Double.parseDouble(StringUtils.getAfter(input, 1));
			speed = speed > 5 ? 5 : speed < 0.1 ? 0.1 : speed;
			Minecraft.getMinecraft().timer.timerSpeed = (float) speed;
			Huzuni.addChatMessage("Time speed set to " + speed + ".");
		}
	}

}
