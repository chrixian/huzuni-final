package net.halalaboos.huzuni.event.packet;

import net.halalaboos.huzuni.api.event.Event;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;

/**
 * @author brudin
 * @version 1.0
 * @since 3/25/14
 */
public class PacketEvent extends Event {

	private Packet packet;
	private final PacketType packetType;
	private NetworkManager networkManager;

	public PacketEvent(PacketType packetType, Packet packet) {
		this(packetType, packet, null);
	}

	public PacketEvent(PacketType packetType, Packet packet, NetworkManager networkManager) {
		this.packet = packet;
		this.networkManager = networkManager;
		this.packetType = packetType;
	}

	public Packet getPacket() {
		return this.packet;
	}

	public void setPacket(final Packet packet) {
		this.packet = packet;
	}

	public PacketType getPacketType() {
		return packetType;
	}

	public final void sendPacket(final Packet packet) {
		networkManager.scheduleOutboundPacket(packet);
	}

	public enum PacketType {
		READ,
		SENT
	}
}
