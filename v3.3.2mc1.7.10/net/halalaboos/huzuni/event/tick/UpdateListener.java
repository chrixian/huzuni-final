package net.halalaboos.huzuni.event.tick;

import net.halalaboos.huzuni.api.event.Listener;

public interface UpdateListener extends Listener {

	void onPreUpdate(MotionUpdateEvent event);
	
	void onPostUpdate(MotionUpdateEvent event);

}
