package net.halalaboos.huzuni.gui;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import pw.brudin.huzuni.theme.click.BrudinTheme;
import pw.brudin.huzuni.theme.ingame.*;
import net.halalaboos.huzuni.*;
import net.halalaboos.huzuni.api.gui.Component;
import net.halalaboos.huzuni.api.gui.render.*;
import net.halalaboos.huzuni.api.plugin.Plugin;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.gui.clickable.*;
import net.halalaboos.huzuni.gui.clickable.button.PluginButton;
import net.halalaboos.huzuni.gui.clickable.slider.ValueSlider;
import net.halalaboos.huzuni.gui.clickable.theme.*;
import net.halalaboos.huzuni.gui.clickable.window.Window;
import net.halalaboos.huzuni.gui.ingame.*;
import net.halalaboos.huzuni.gui.ingame.themes.*;
import net.halalaboos.huzuni.gui.xray.XrayWindow;
import net.halalaboos.huzuni.plugin.PluginManager;
import net.halalaboos.huzuni.plugin.value.Value;
import net.halalaboos.huzuni.plugin.value.ValueManager;
import net.halalaboos.huzuni.util.FileUtils;

public final class GuiManager {

	private static final Theme[] windowThemes = new Theme[] {
		new HuzuniTheme(),
		new BmcTheme(),
		new BrudinTheme(),
		new DarkTheme(),
		new BubbleBlastTheme(),
		new WhiteoutTheme()
	};
	
	private static final IngameTheme[] ingameThemes = new IngameTheme[] {
		new Default(),
		new OldSchool(),
		new Informative(),
		new Nyan(),
		new Paged(),
		new PigPlus(),
		new Morbid(),
		new Skidded(),
		new Simple(),
		new Iridium(),
		new Compact(),
		new Reliant(),
		new Hysteria(),
		new None()
	};
	
	private static final ContainerManager<Window> windowManager = new ContainerManager<Window>(windowThemes[0]);
	
	private static int selectedTheme = 0;
	
	public static final File WINDOW_FILE = new File(Huzuni.SAVE_DIRECTORY, "Windows.txt");
	
	private GuiManager() {
		
	}
	

	public static void resetWindows() {
		windowManager.getContainers().clear();
		loadDefaultWindows();
		HubWindow hubWindow = new HubWindow();
		windowManager.add(hubWindow);
		XrayWindow xrayWindow = new XrayWindow();
		windowManager.add(xrayWindow);
		windowManager.add(new ColorWindow());
		hubWindow.copyWindows(windowManager.getContainers());
		layoutWindows();
	}
	
	public static boolean isCustomWindow(Window window) {
		return (window instanceof ColorWindow) || (window instanceof HubWindow) || (window instanceof XrayWindow);
	}
	
	private static void loadDefaultWindows() {
		for (Category type : Category.values()) {
			Window window = new Window(type.formalName);
			for (Plugin plugin : PluginManager.getPlugins()) {
				if (plugin.getCategory() != null && plugin.getCategory().equals(type)) {
					final PluginButton button = new PluginButton(plugin);
					window.add(button);
				}
			}
			window.layout();
			if (window.getComponents().size() > 0)
				windowManager.add(window);
		}
	}
	
	public static void layoutWindows() {
		int x = 2, y = 2, amountPerRow = 4, count = 0;
		float[] heights = new float[amountPerRow];
		for (Window window : windowManager.getContainers()) {
			count++;			
			window.setX(x);
			window.setY(y + heights[count - 1]);
			heights[count - 1] = window.getY() + window.getHeight() + window.getTabHeight();
			x = (int) (window.getX() + window.getWidth() + 2);
			if (count >= amountPerRow) {
				count = 0;
				x = 2;
			}
		}
	}
	
	public static void loadWindows() {
		windowManager.setTheme(getSelectedWindowTheme());
		selectedTheme = Huzuni.CONFIG.getInt("Ingame Theme");
		List<String> lines = FileUtils.readFile(WINDOW_FILE);
		if (lines.isEmpty()) {
			loadDefaultWindows();
			HubWindow hubWindow = new HubWindow();
			windowManager.add(hubWindow);
			XrayWindow xrayWindow = new XrayWindow();
			windowManager.add(xrayWindow);
			windowManager.add(new ColorWindow());
			hubWindow.copyWindows(windowManager.getContainers());
			layoutWindows();
		} else {
			loadDefaultWindows();
			HubWindow hubWindow = new HubWindow();
			windowManager.add(hubWindow);
			XrayWindow xrayWindow = new XrayWindow();
			windowManager.add(xrayWindow);
			windowManager.add(new ColorWindow());
			Window window = null;
			for (int i = 0; i < lines.size(); i++) {
				String line = lines.get(i);
				if (line.startsWith("WINDOW\247i")) {
					String[] split = line.split("\247i");
					if (window != null)
						window.layout();
					window = getWindow(split[1]);
					if (!windowManager.contains(window))
						windowManager.add(window);
					window.setX(Integer.parseInt(split[2]));
					window.setY(Integer.parseInt(split[3]));
					window.setActive(Boolean.parseBoolean(split[4]));
				}
				/*if (line.startsWith("BUTTON\247i")) {
					String[] split = line.split("\247i");
					Plugin plugin = PluginManager.getPlugin(split[1]);
					if (window != null && plugin != null)
						window.add(new PluginButton(plugin));
				}
				if (line.startsWith("SLIDER\247")) {
					String[] split = line.split("\247i");
					Value value = ValueManager.getValue(split[1]);
					if (window != null && value != null)
						window.add(new ValueSlider(value));
				}*/
			}
			if (window != null)
				window.layout();
			hubWindow.copyWindows(windowManager.getContainers());
		}
	}
	
	public static void saveWindows() {
		List<String> lines = new ArrayList<String>();
		for (Window window : windowManager.getContainers()) {
			lines.add("WINDOW\247i" + window.getTitle() + "\247i" + (int) window.getX() + "\247i" + (int) window.getY() + "\247i" + window.isActive());
			for (Component component : window.getComponents()) {
				/*if (component instanceof PluginButton) {
					PluginButton button = (PluginButton) component;
					lines.add("BUTTON\247i" + button.getPlugin().getName());
				}
				if (component instanceof ValueSlider) {
					ValueSlider slider = (ValueSlider) component;
					lines.add("SLIDER\247i" + slider.getValueObj().getName());
				}*/
			}
		}
		FileUtils.writeFile(WINDOW_FILE, lines);
	}
	
	public static Window getWindow(String title) {
		for (Window window : windowManager.getContainers()) {
			if (window.getTitle().equalsIgnoreCase(title))
				return window;
		}
		return new Window(title);
	}
	
	public static ContainerManager<Window> getWindowManager() {
		return windowManager;
	}
	
	public static List<Window> getWindows() {
		return windowManager.getContainers();
	}
	
	public static List<Window> getUserWindows() {
		List<Window> windows = new ArrayList<Window>();
		for (Window window : windowManager.getContainers()) {
			if (!isCustomWindow(window))
				windows.add(window);
		}
		return windows;
	}

	public static IngameTheme getIngameTheme() {
		try {
			return ingameThemes[selectedTheme];
		} catch(Exception e) {
			selectedTheme = 0;
			return ingameThemes[selectedTheme];
		}
	}

	private static Theme getSelectedWindowTheme() {
		return windowThemes[getSelectedWindowThemeId()];
	}
	
	public static int getSelectedWindowThemeId() {
		return Huzuni.CONFIG.getInt("Window Theme");
	}
	
	public static void setSelectedWindowTheme(int theme) {
		Huzuni.CONFIG.put("Window Theme", theme);
		windowManager.setTheme(windowThemes[theme]);
	}

	public static Theme[] getWindowThemes() {
		return windowThemes;
	}

	public static IngameTheme[] getIngameThemes() {
		return ingameThemes;
	}

	public static void nextTheme() {
        int maxSize = ingameThemes.length;
		selectedTheme++;
		if (selectedTheme >= maxSize)
			selectedTheme = 0;
		if (selectedTheme < 0)
			selectedTheme = maxSize - 1;
		Huzuni.CONFIG.put("Ingame Theme", selectedTheme);

    }
	
}
