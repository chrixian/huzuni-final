package net.halalaboos.huzuni.gui.clickable;

import java.awt.Rectangle;
import java.util.List;

import net.halalaboos.huzuni.api.gui.Component;
import net.halalaboos.huzuni.api.gui.Container;
import net.halalaboos.huzuni.api.gui.layout.Layout;

public class FittedLayout implements Layout {
    
	private final int COMPONENT_PADDING = 1;

    @Override
    public Rectangle layout(Container container, List<Component> components) {
        float y = 0;
        int height = (int) (container.getHeight() / components.size());
        for (Component component : components) {
        	component.layout(COMPONENT_PADDING, y + COMPONENT_PADDING, container.getWidth() - COMPONENT_PADDING * 2, height - COMPONENT_PADDING * 2);
            y += height + COMPONENT_PADDING;
        }
        return new Rectangle(0, 0, (int) container.getWidth(), (int) container.getHeight());
    }

}