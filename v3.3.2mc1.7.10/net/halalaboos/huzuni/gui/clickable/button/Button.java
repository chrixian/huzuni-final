package net.halalaboos.huzuni.gui.clickable.button;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.clickable.DefaultComponent;

public class Button extends DefaultComponent {

	private String title;
	
	private String command;
	
	private boolean highlight;
	
	public Button(String title) {
		super();
		this.title = title;
		this.setWidth(100);
		this.setHeight(12);
		if (Huzuni.getStringWidth(title) + 2 > getWidth())
			this.setWidth(Huzuni.getStringWidth(title) + 2);
	}
	
	public Button(String title, String command) {
		this(title);
		this.command = command;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public boolean isHighlight() {
		return highlight;
	}

	public void setHighlight(boolean highlight) {
		this.highlight = highlight;
	}
}
