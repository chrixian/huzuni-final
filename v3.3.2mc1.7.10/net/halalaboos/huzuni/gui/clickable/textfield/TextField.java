package net.halalaboos.huzuni.gui.clickable.textfield;



import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.clickable.DefaultComponent;
import net.halalaboos.huzuni.util.Timer;
import net.minecraft.util.ChatAllowedCharacters;

public class TextField extends DefaultComponent {
	
	private final Timer timer = new Timer();
   
	private String carot = "_";

	private int maxLength = 100;
    
	private String text = "";

	private boolean selected = false;
	
	public TextField() {
		super();
		setWidth(100F);
		setHeight(12F);
	}

	public TextField(String text) {
		this();
		this.text = text;
	}

	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
		boolean mouseAround = super.mouseClicked(x, y, buttonId);
		if (mouseAround && buttonId == 0)
			selected = true;
		else
			selected = false;
		return mouseAround;
	}

	@Override
	public void mouseReleased(int x, int y, int buttonId) {
		if (buttonId == 0) {
			this.invokeActionListeners();
		}
	}
	
	@Override
	public void keyTyped(int keyCode, char c) {
		if (selected) {
			if (keyCode == Keyboard.KEY_V) {
				if (Keyboard.isKeyDown(Keyboard.KEY_LCONTROL) || Keyboard.isKeyDown(Keyboard.KEY_RCONTROL)) {
					if (Sys.getClipboard() != null) {
						text += Sys.getClipboard();
					}
				}
			}

			if (keyCode == Keyboard.KEY_BACK) {
				minus(1);
			} else {
				if (ChatAllowedCharacters.isAllowedCharacter(Keyboard.getEventCharacter()))
					text += Keyboard.getEventCharacter();
				if (keyCode == Keyboard.KEY_RETURN)
					invokeActionListeners();


				if (text.length() > maxLength && maxLength > 0)
					text = text.substring(0, maxLength);
			}
			invokeKeyListeners(keyCode, c);
		}
	}

	private void minus(int ammount) {
		if ((text.length() - ammount) >= 0)
			text = text.substring(0, text.length() - ammount);
	}
	
	public String getCarot() {
		if (timer.hasReach(250)) {
			if (carot.equals("_"))
                carot = "";
            else
                carot = "_";
			timer.reset();
		}
        return selected ? carot : "";
	}
	
	public String getTextForRender(float width) {
        if (Huzuni.getStringWidth(text + "_") > width) {
            String text = "";
            char[] chars = this.text.toCharArray();
            for (int index = chars.length - 1; index >= 0; index--) {
                if (Huzuni.getStringWidth(chars[index] + text + "_") >= width)
                    break;
                text = chars[index] + text;
            }
            return text;
        }
        return text;
    }

	public int getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}
