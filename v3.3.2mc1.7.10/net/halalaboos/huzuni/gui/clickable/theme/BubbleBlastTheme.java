package net.halalaboos.huzuni.gui.clickable.theme;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.clickable.button.Button;
import net.halalaboos.huzuni.gui.clickable.theme.DefaultTheme;
import net.halalaboos.huzuni.util.GLUtils;

import org.lwjgl.opengl.GL11;

public class BubbleBlastTheme extends DefaultTheme {

	@Override
	public String getName() {
		return "Bubble blast";
	}

	/**
	 * Draws a gradient rectangle.
	 *
	 * @param x
	 *          The start X
	 * @param y
	 *          The start Y
	 * @param x2
	 *          The end X
	 * @param y2
	 *          The end Y
	 * @param col1
	 *          The first color
	 * @param col2
	 *          The last color
	 */
	private void drawGradientRect(float x, float y, float x2, float y2, int col1, int col2) {
		float f = (float)(col1 >> 24 & 0xFF) / 255F;
		float f1 = (float)(col1 >> 16 & 0xFF) / 255F;
		float f2 = (float)(col1 >> 8 & 0xFF) / 255F;
		float f3 = (float)(col1 & 0xFF) / 255F;

		float f4 = (float)(col2 >> 24 & 0xFF) / 255F;
		float f5 = (float)(col2 >> 16 & 0xFF) / 255F;
		float f6 = (float)(col2 >> 8 & 0xFF) / 255F;
		float f7 = (float)(col2 & 0xFF) / 255F;

		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glShadeModel(GL11.GL_SMOOTH);

		GL11.glPushMatrix();
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glVertex2d(x2, y);
		GL11.glVertex2d(x, y);

		GL11.glColor4f(f5, f6, f7, f4);
		GL11.glVertex2d(x, y2);
		GL11.glVertex2d(x2, y2);
		GL11.glEnd();
		GL11.glPopMatrix();

		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
	}

	private void drawRect(float g, float h, float i, float j, int col1) {
		float f = (float)(col1 >> 24 & 0xFF) / 255F;
		float f1 = (float)(col1 >> 16 & 0xFF) / 255F;
		float f2 = (float)(col1 >> 8 & 0xFF) / 255F;
		float f3 = (float)(col1 & 0xFF) / 255F;

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);

		GL11.glPushMatrix();
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glVertex2d(i, h);
		GL11.glVertex2d(g, h);
		GL11.glVertex2d(g, j);
		GL11.glVertex2d(i, j);
		GL11.glEnd();
		GL11.glPopMatrix();

		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
	}
	
	private void renderButton(float x, float y, float x1, float y1, boolean on, boolean hover, boolean down, boolean transparent) {
		int gradientTop, gradientBottom;
		if (on) {
			gradientTop = Huzuni.getColorTheme().getRGB();
			gradientBottom = Huzuni.getColorTheme().darker().getRGB();
		} else {
			gradientTop = 0xFF676767;
			gradientBottom = 0xFF4E4F4E;
		}
		GLUtils.drawBorderedRect(x, y, x1, y1, 0.5F, 0, gradientBottom);
		drawGradientRect(x + 0.5F, y + 0.5F, x1 - 0.5F, y1 - 0.5F, gradientTop, gradientBottom);
		if (hover)
			drawRect(x + 0.5F, y + 0.5F, x1 - 0.5F, y1 - 0.5F, 0x1F000000);
	}

	@Override
	protected void renderButtonRect(float x, float y, float x1, float y1,
			boolean highlight, boolean mouseOver, boolean mouseDown) {
		renderButton(x, y, x1, y1, highlight, mouseOver, mouseDown, true);
	}

	@Override
	protected void renderWindowRect(float x, float y, float x1, float y1, float tabHeight, float lineWidth) {
		drawGradientRect(x, y, x1, y + tabHeight, 0xFF2D3036, 0xFF222524);
		drawGradientRect(x, y + tabHeight, x1, y1, 0xFF282929, 0xFF282929);
	}

	@Override
	protected int getButtonTextColor(Button button) {
		return 0xFFFFFF;
	}

	@Override
	protected int getWindowTextColor() {
		return 0xFFFFFF;
	}
	
	@Override
	protected int getTooltipTextColor() {
		return 0xFFFFFF;
	}
}
