package net.halalaboos.huzuni.gui.clickable.theme;

import java.awt.Color;
import java.text.DecimalFormat;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.gui.Component;
import net.halalaboos.huzuni.api.gui.Container;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.gui.clickable.button.Button;
import net.halalaboos.huzuni.gui.clickable.button.PluginButton;
import net.halalaboos.huzuni.gui.clickable.dropdown.Dropdown;
import net.halalaboos.huzuni.gui.clickable.label.Label;
import net.halalaboos.huzuni.gui.clickable.menu.Menu;
import net.halalaboos.huzuni.gui.clickable.slider.Slider;
import net.halalaboos.huzuni.gui.clickable.slot.SlotComponent;
import net.halalaboos.huzuni.gui.clickable.textfield.TextField;
import net.halalaboos.huzuni.gui.clickable.window.Window;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.MathUtils;
import net.minecraft.util.EnumChatFormatting;

public class HuzuniTheme implements Theme {

	private final Color defaultColor = new Color(0.3F, 0.3F, 0.3F, 0.75F), background = new Color(0F, 0F, 0F, 0.75F);
	
    protected DecimalFormat formatter = new DecimalFormat("#.#");
	
	@Override
	public void renderComponent(float xOffset, float yOffset, Component component) {
		if (component instanceof PluginButton) {
			PluginButton button = (PluginButton) component;

			GLUtils.glColor(GLUtils.getColorWithAffects(button.isHighlight() ? Huzuni.getColorTheme() : defaultColor, button.isMouseOver() && !button.isDown(), button.isMouseDown()));
			GLUtils.drawRect(button.getX() + xOffset, button.getY() + yOffset, button.getX() + button.getWidth() - (button.hasDropdown() ? PluginButton.DROPDOWN_WIDTH : 0) + xOffset, button.getY() + button.getHeight() + yOffset);
			if (button.hasDropdown()) {
				GLUtils.glColor(GLUtils.getColorWithAffects(button.isDown() ? Huzuni.getColorTheme() : defaultColor, button.isMouseOver() && !button.isDown(), false));
				GLUtils.drawRect(button.getX() + button.getWidth() - PluginButton.DROPDOWN_WIDTH + 1 + xOffset, button.getY() + yOffset, button.getX() + button.getWidth() + xOffset, button.getY() + button.getHeight() + yOffset);
			}
			Huzuni.drawString(button.getTitle(), button.getX() + (button.getWidth() / 2) - (Huzuni.getStringWidth(button.getTitle()) / 2) + xOffset, button.getY() + (button.getHeight() / 2) - (Huzuni.getStringHeight(button.getTitle()) / 2) + yOffset + 1, 0xFFFFFF);
			if (button.hasDropdown())
				Huzuni.drawString("..", button.getX() + button.getWidth() - PluginButton.DROPDOWN_WIDTH + 1 + xOffset, button.getY() + (button.getHeight() / 2) - (Huzuni.getStringHeight(button.getTitle()) / 2) + yOffset + 2, 0x40FFFFFF);
			
			if (button.isDown()) {
    			GLUtils.glColor(GLUtils.getColorWithAffects(background, button.isMouseOver(), button.isMouseDown()));
    			GLUtils.drawRect(button.getX() + xOffset, button.getY() + yOffset + button.getHeight() + 1, button.getX() + button.getWidth() + xOffset, button.getY() + yOffset + button.getHeight() + button.getDownHeight() + 3);
    			button.renderComponents(this, xOffset, yOffset, button.isMouseOver(), button.isMouseDown());
    			if (button.hasEnoughToScroll()) {
    				float[] sliderPoint = button.getSliderPoint();
    				GLUtils.glColor(GLUtils.getColorWithAffects(Huzuni.getColorTheme(), button.isMouseOver() && MathUtils.inside(GLUtils.getMouseX(), GLUtils.getMouseY(), sliderPoint), false));
    				GLUtils.drawRect(sliderPoint[0], sliderPoint[1], sliderPoint[0] + sliderPoint[2], sliderPoint[1] + sliderPoint[3]);
    			}
			}
		} else if (component instanceof Button) {
			Button button = (Button) component;
			
			GLUtils.glColor(GLUtils.getColorWithAffects(button.isHighlight() ? Huzuni.getColorTheme() : defaultColor, button.isMouseOver(), button.isMouseDown()));
			GLUtils.drawRect(button.getX() + xOffset, button.getY() + yOffset, button.getX() + button.getWidth() + xOffset, button.getY() + button.getHeight() + yOffset);
			Huzuni.drawString(button.getTitle(), button.getX() + (button.getWidth() / 2) - (Huzuni.getStringWidth(button.getTitle()) / 2) + xOffset, button.getY() + (button.getHeight() / 2) - (Huzuni.getStringHeight(button.getTitle()) / 2) + yOffset + 1, 0xFFFFFF);
		} else if (component instanceof Slider) {
			Slider slider = (Slider) component;
			
			GLUtils.glColor(GLUtils.getColorWithAffects(defaultColor, slider.isMouseOver(), false));
			GLUtils.drawRect(slider.getX() + xOffset, slider.getY() + yOffset, slider.getX() + slider.getWidth() + xOffset, slider.getY() + slider.getHeight() + yOffset);

			Huzuni.drawString(slider.getLabel(), slider.getX() + 2 + xOffset, slider.getY() + (slider.getHeight() / 2) - (Huzuni.getStringHeight(slider.getLabel()) / 2) + yOffset + 2, 0xFFFFFF);
			String value = formatter.format(slider.getValue()) + slider.getValueWatermark();
			Huzuni.drawString(value, slider.getX() + (slider.getWidth()) - (Huzuni.getStringWidth(value)) - 2 + xOffset, slider.getY() + (slider.getHeight() / 2) - (Huzuni.getStringHeight(value) / 2) + yOffset + 2, 0xFFFFFF);
			
			float[] sliderPoint = slider.getSliderPoint();
			GLUtils.glColor(GLUtils.getColorWithAffects(Huzuni.getColorTheme(), slider.isMouseOver(), slider.isMouseDown()));
			GLUtils.drawRect(sliderPoint[0] + xOffset, sliderPoint[1] + yOffset, sliderPoint[0] + sliderPoint[2] + xOffset, sliderPoint[1] + sliderPoint[3] + yOffset);
			
		} else if (component instanceof TextField) {
			TextField textField = (TextField) component;
			
			GLUtils.glColor(GLUtils.getColorWithAffects(defaultColor, textField.isMouseOver() || textField.isSelected(), textField.isMouseDown()));
			GLUtils.drawRect(textField.getX() + xOffset, textField.getY() + yOffset, textField.getX() + textField.getWidth() + xOffset, textField.getY() + textField.getHeight() + yOffset);
			String text = textField.getTextForRender(textField.getWidth()) + textField.getCarot();
			Huzuni.drawString(text, textField.getX() + xOffset + 2, textField.getY() + (textField.getHeight() / 2) - (Huzuni.getStringHeight(text) / 2) + yOffset + 2, 0xFFFFFF);
		} else if (component instanceof SlotComponent) {
			SlotComponent slotComponent = (SlotComponent) component;
			
			float[] sliderPoint = slotComponent.getSliderPoint();
			GLUtils.glColor(GLUtils.getColorWithAffects(defaultColor, false, false));
			GLUtils.drawRect(slotComponent.getX() + xOffset, slotComponent.getY() + yOffset, slotComponent.getX() + slotComponent.getWidth() + xOffset, slotComponent.getY() + slotComponent.getHeight() + yOffset);
			if (slotComponent.hasEnoughToScroll() && slotComponent.hasSlider()) {
				GLUtils.glColor(GLUtils.getColorWithAffects(Huzuni.getColorTheme(), slotComponent.isMouseOver() && MathUtils.inside(GLUtils.getMouseX(), GLUtils.getMouseY(), sliderPoint), slotComponent.isDragging()));
				GLUtils.drawRect(sliderPoint[0], sliderPoint[1], sliderPoint[0] + sliderPoint[2], sliderPoint[1] + sliderPoint[3]);
			}
			slotComponent.renderComponents(this, xOffset, yOffset, slotComponent.isMouseOver(), slotComponent.isMouseDown());
		} else if (component instanceof Dropdown) {
			Dropdown dropdown = (Dropdown) component;
			
			GLUtils.glColor(GLUtils.getColorWithAffects(defaultColor, dropdown.isDown() ? true : dropdown.isMouseOver(), dropdown.isDown() ? false : dropdown.isMouseDown()));
            GLUtils.drawRect(dropdown.getX() + xOffset, dropdown.getY() + yOffset, dropdown.getX() + dropdown.getWidth() + xOffset, dropdown.getY() + dropdown.getHeight() + yOffset);

            Huzuni.drawStringWithShadow(dropdown.getTitle() + " (" + dropdown.getSelectedComponentName() + ")", dropdown.getX() + xOffset + 2, dropdown.getY() + yOffset + dropdown.getHeight() / 2 - Huzuni.getStringHeight(dropdown.getTitle()) / 2 + 2, Color.WHITE.getRGB());
            
			if (dropdown.isDown()) {    			
    			GLUtils.glColor(GLUtils.getColorWithAffects(background, dropdown.isMouseOver(), dropdown.isMouseDown()));
    			GLUtils.drawRect(dropdown.getX() + xOffset, dropdown.getY() + yOffset + dropdown.getHeight() + 1, dropdown.getX() + dropdown.getWidth() + xOffset, dropdown.getY() + yOffset + dropdown.getHeight() + dropdown.getDownHeight() + 3);
    			dropdown.renderComponents(this, xOffset, yOffset, dropdown.isMouseOver(), dropdown.isMouseDown());
    			if (dropdown.hasEnoughToScroll()) {
    				float[] sliderPoint = dropdown.getSliderPoint();
    				GLUtils.glColor(GLUtils.getColorWithAffects(Huzuni.getColorTheme(), dropdown.isMouseOver() && MathUtils.inside(GLUtils.getMouseX(), GLUtils.getMouseY(), sliderPoint), false));
    				GLUtils.drawRect(sliderPoint[0], sliderPoint[1], sliderPoint[0] + sliderPoint[2], sliderPoint[1] + sliderPoint[3]);
    			}
			}
		} else if (component instanceof TextField) {
			TextField textField = (TextField) component;
			String text = textField.getTextForRender(textField.getWidth()) + textField.getCarot();
			GLUtils.glColor(GLUtils.getColorWithAffects(defaultColor, textField.isMouseOver(), textField.isMouseDown()));
			GLUtils.drawRect(textField.getX() + xOffset, textField.getY() + yOffset, textField.getX() + textField.getWidth() + xOffset, textField.getY() + textField.getHeight() + yOffset);
			Huzuni.drawString(text, textField.getX() + xOffset, textField.getY() + (textField.getHeight() / 2) - (Huzuni.getStringHeight(text) / 2) + yOffset + 2, 0xFFFFFF);
		} else if (component instanceof Label) {
			Label label = (Label) component;
			Huzuni.drawString(label.getText(), label.getX() + xOffset, label.getY() + yOffset + 2, label.getColor().getRGB());
		}
			
	}

	@Override
	public void renderContainer(Container container) {
		if (container instanceof Window) {
			Window window = (Window) container;
			GLUtils.drawRect(window.getX(), window.getY(), window.getX() + window.getWidth(), window.getY() + window.getHeight() + window.getTabHeight(), -1627389952);
			Huzuni.drawStringWithShadow(EnumChatFormatting.BOLD + window.getTitle(), window.getX() + 2, window.getY() + 2 + window.getTabHeight() / 2 - Huzuni.getStringHeight(EnumChatFormatting.BOLD + window.getTitle()) / 2, 0xFFFFFF);
		} else if (container instanceof Menu) {
			Menu menu = (Menu) container;
			GLUtils.drawRect(menu.getX(), menu.getY(), menu.getX() + menu.getWidth(), menu.getY() + menu.getHeight(), -1627389952);
			Huzuni.drawStringWithShadow(EnumChatFormatting.BOLD + menu.getTitle(), menu.getX() + 2, menu.getY() + 2, 0xFFFFFF);
		}
	}

	@Override
	public void renderTooltip(String tooltip) {
		float mouseX = GLUtils.getMouseX(), mouseY = GLUtils.getMouseY() - 8, width = Huzuni.getStringWidth(tooltip);
		GLUtils.glColor(GLUtils.getColorWithAffects(defaultColor, false, false));
		GLUtils.drawRect(mouseX, mouseY, mouseX + width + 4, mouseY + 12);
		Huzuni.drawStringWithShadow(tooltip, mouseX + 2, mouseY + 2, 0xFFFFFF);
	}
	
	@Override
	public void renderSlot(float xOffset, float yOffset, int index, float[] area, boolean highlight,
			boolean mouseOver, boolean mouseDown) {
		GLUtils.glColor(GLUtils.getColorWithAffects(highlight ? Huzuni.getColorTheme() : defaultColor, mouseOver, mouseDown));
		GLUtils.drawRect(area[0], area[1], area[0] + area[2], area[1] + area[3]);
	}

	@Override
	public String getName() {
		return "Huzuni";
	}

}
