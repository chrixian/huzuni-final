/**
 *
 */
package net.halalaboos.huzuni.gui.ingame;

import net.minecraft.client.Minecraft;

/**
 * @author Halalaboos
 * @since Aug 3, 2013
 */
public interface IngameTheme {

	Minecraft mc = Minecraft.getMinecraft();

    /**
     * Renders inside the in-game GUI.
     */
    public void render(Minecraft mc, int screenWidth, int screenHeight);

    /**
     * @return the display name for this theme.
     */
    public String getName();

    /**
     * Invoked when a key is pressed in game.
     *
     * @param keyCode
     */
    public void onKeyTyped(int keyCode);
}
