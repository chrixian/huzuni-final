package net.halalaboos.huzuni.gui.ingame.themes;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.ingame.IngameTheme;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.plugin.PluginManager;
import net.minecraft.client.Minecraft;
import net.minecraft.util.EnumChatFormatting;

/**
 * @author Halalaboos
 * @since Aug 3, 2013
 */
public class Default implements IngameTheme {

	/**
	 * @see net.halalaboos.huzuni.gui.ingame.IngameTheme#render(net.minecraft.client.Minecraft, int, int)
	 */
	@Override
	public void render(Minecraft mc, int screenWidth, int screenHeight) {
		mc.fontRenderer.drawStringWithShadow(Huzuni.TITLE + "\247r v" + Huzuni.VERSION + " " + EnumChatFormatting.GRAY + Huzuni.UPDATE_NAME, 2, 2, 0xFFFFFF);

		int yPos = 12;
		for (DefaultPlugin plugin : PluginManager.getDefaultPlugins()) {
			if (plugin.isEnabled() && plugin.getVisible()) {
				mc.fontRenderer.drawStringWithShadow(plugin.getRenderName(), 2, yPos, plugin.getColor().getRGB());
				yPos += 10;
			}
		}
	}

	/**
	 * @see net.halalaboos.huzuni.gui.ingame.IngameTheme#getName()
	 */
	@Override
	public String getName() {
		return "Default";
	}

	/**
	 * @see net.halalaboos.huzuni.gui.ingame.IngameTheme#onKeyTyped(int)
	 */
	@Override
	public void onKeyTyped(int keyCode) {
	}

}
