package net.halalaboos.huzuni.gui.ingame.themes;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.ingame.IngameTheme;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.plugin.PluginManager;
import net.minecraft.client.Minecraft;

public class Morbid implements IngameTheme {

    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
        mc.fontRenderer.drawStringWithShadow(Huzuni.TITLE + " \2470[\247fv" + Huzuni.VERSION + "\2470]", 2, 2, 0xFF4400);

        int yPos = 2;
        for (DefaultPlugin plugin : PluginManager.getDefaultPlugins()) {
            if (plugin.isEnabled()) {
                if (plugin.getVisible()) {
                    int with = screenWidth - (mc.fontRenderer.getStringWidth(plugin.getRenderName()) + 10);
                    mc.fontRenderer.drawStringWithShadow("\2470[\247r" + plugin.getRenderName() + "\2470]", with, yPos, 0xFFFFFF);
                    yPos += 12;
                }
            }
        }
    }

    @Override
    public String getName() {
        return "Morbid";
    }

    @Override
    public void onKeyTyped(int keyCode) {
    }

}
