/**
 * 
 */
package net.halalaboos.huzuni.gui.screen;

import java.text.DecimalFormat;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;

import org.lwjgl.opengl.GL11;

/**
 * @author Halalaboos
 * 
 * @since Sep 13, 2013
 */
public class HuzuniSlider extends GuiButton {
	protected static final DecimalFormat decimalFormat = new DecimalFormat("#.##");
	
	protected float sliderValue = 1.0F;
	protected boolean dragging;
	protected String setting;
	protected float minVal, maxVal;
	
	public HuzuniSlider(int id, int x, int y, String setting, float value,
			String displayString) {
		super(id, x, y, 150, 20, displayString);
		this.sliderValue = 0;
		this.setting = setting;
		this.minVal = 0;
		this.maxVal = 1;
		updateText(value);
	}
	
	public HuzuniSlider(int id, int x, int y, String setting,
			String displayString, float minVal, float defaultVal, float maxVal) {
		this(id, x, y, setting, defaultVal, displayString);
		this.minVal = minVal;
		this.maxVal = maxVal;
		this.sliderValue = (defaultVal - minVal) / (maxVal - minVal);
	}

	@Override
	public int getHoverState(boolean p_146114_1_) {
		return 0;
	}

	/**
	 * Fired when the mouse button is dragged. Equivalent of
	 * MouseListener.mouseDragged(MouseEvent e).
	 */
	@Override
	protected void mouseDragged(Minecraft par1Minecraft, int par2, int par3) {
		if (this.drawButton) {
			if (this.dragging) {
				this.sliderValue = (float) (par2 - (this.x + 4))
						/ (float) (this.width - 8);

				if (this.sliderValue < 0.0F) {
					this.sliderValue = 0.0F;
				}

				if (this.sliderValue > 1.0F) {
					this.sliderValue = 1.0F;
				}
				float sliderValue = getSliderValue();
				updateText(sliderValue);
			}

			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			this.drawTexturedModalRect(this.x
					+ (int) (this.sliderValue * (float) (this.width - 8)),
					this.y, 0, 66, 4, 20);
			this.drawTexturedModalRect(this.x
					+ (int) (this.sliderValue * (float) (this.width - 8)) + 4,
					this.y, 196, 66, 4, 20);
		}
	}

	@Override
	public boolean mousePressed(Minecraft par1Minecraft, int par2, int par3) {
		if (super.mousePressed(par1Minecraft, par2, par3)) {
			this.sliderValue = (float) (par2 - (this.x + 4))
					/ (float) (this.width - 8);

			if (this.sliderValue < 0.0F) {
				this.sliderValue = 0.0F;
			}

			if (this.sliderValue > 1.0F) {
				this.sliderValue = 1.0F;
			}
			this.dragging = true;
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public void mouseReleased(int par1, int par2) {
		this.dragging = false;
	}
	
	public float getSliderValue() {
		return (sliderValue * (maxVal - minVal)) + minVal;
	}
	
	private void updateText(float value) {
		this.displayString = setting + ": " + decimalFormat.format(value);
	}
}