package net.halalaboos.huzuni.gui.screen.alt;

import javax.swing.SwingUtilities;

import com.mojang.authlib.exceptions.AuthenticationException;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.gui.clickable.slot.SlotComponent;
import net.halalaboos.huzuni.util.LoginUtils;
import net.halalaboos.huzuni.util.Timer;
import net.minecraft.client.Minecraft;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.Session;

public class SlotAlts extends SlotComponent<String> {
	
	private final AltManager guiAlts;

	private int selectedItem = -1;
	private Timer timer = new Timer();
	
	public SlotAlts(AltManager guiAlts) {
		super(0, 0);
		this.guiAlts = guiAlts;
	}

	@Override
	protected void render(Theme theme, int index, String account, float[] area, boolean highlight, boolean mouseOver,
			boolean mouseDown) {
		String username = "";
		String password = "";
		if (account.split(":").length >= 1) {
			username = account.split(":")[0];
			password = account.split(":")[1].replaceAll(".", "*");	
		}
		int usernameX = (int) area[0];
		int passwordX = (int) area[0];
		
		theme.renderSlot(this.getOffsetX(), this.getOffsetY(), index, area, selectedItem == index, mouseOver, mouseDown);

		Huzuni.drawStringWithShadow(username, usernameX + 2, area[1] + 2, 0xFFFFFF);
		Huzuni.drawStringWithShadow(password, passwordX + 2, area[1] + 12, 0xCCCCCC);
	}
	
	@Override
	public int getComponentHeight() {
		return 22;
	}
	
	public void login() {
		if (selectedItem != -1) {
			String account = getComponents().get(selectedItem);
			final String username = account.split(":")[0];
			final String password = account.split(":")[1];
			SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    try {
						Session session = LoginUtils.loginToMinecraft(username, password);
	                    Minecraft.getMinecraft().setSession(session);
	                    guiAlts.setLastLoginStatus(EnumChatFormatting.GRAY + "Success" + EnumChatFormatting.RESET);
					} catch (AuthenticationException e) {
                    	guiAlts.setLastLoginStatus(EnumChatFormatting.GRAY +  e.getMessage() + EnumChatFormatting.RESET);
					}
                    // If the return code was OK, we'll swap accounts.
        			selectedItem = -1;
                }
            });
		}
	}

	public void removeAccount() {
		if (selectedItem != -1) {
			this.getComponents().remove(selectedItem);
			selectedItem = -1;
		}
	}

	public int getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(int selectedItem) {
		this.selectedItem = selectedItem;
	}

	@Override
	protected void onReleased(int index, String component, int mouseX,
			int mouseY) {
	}

	@Override
	protected void onClicked(int index, String component, float[] area,
			int mouseX, int mouseY, int buttonId) {
		if (selectedItem == index) {
			if (timer.getTimePassed() < 500)
				login();
		} else
			selectedItem = index;
		
		timer.reset();
	}
	
}