package net.halalaboos.huzuni.gui.screen.keybinds;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Keybind;
import net.halalaboos.huzuni.gui.clickable.ContainerManager;
import net.halalaboos.huzuni.gui.clickable.menu.Menu;
import net.halalaboos.huzuni.gui.clickable.theme.HuzuniTheme;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;

public class KeybindManager extends GuiScreen {
	
	private final GuiScreen parentGui;
    private GuiTextField findField;
    private GuiButton clearKey;
    
    private final ContainerManager<Menu> container = new ContainerManager<Menu>(new HuzuniTheme());
    private final Menu menu = new Menu("Keybinds");
    private final SlotKeybinds slotKeybinds = new SlotKeybinds();
    
    
    public KeybindManager(GuiScreen parentGui) {
        this.parentGui = parentGui;
        menu.add(slotKeybinds);
    	container.add(menu);
    	slotKeybinds.setComponents(Huzuni.KEYBINDS);
    }
    
    @Override
    public void initGui() {
        int slotWidth = 300, slotHeight = (height - 114);
    	
    	 menu.setX(this.width / 2 - slotWidth / 2);
         menu.setY(30);
         menu.setWidth(slotWidth);
         menu.setHeight(slotHeight);
         slotKeybinds.setX(2);
         slotKeybinds.setY(2);
         slotKeybinds.setWidth(slotWidth - 4);
         slotKeybinds.setHeight(slotHeight - 16);
         
    	findField = new GuiTextField(mc.fontRenderer, width / 2 - 75 - 35, height - 60, 220, 20);
    	findField.setFocused(true);
    	
    	buttonList.add(new GuiButton(0, this.width / 2 + 2, this.height - 28, 152, 20, "Done"));
        
        clearKey = new GuiButton(2, this.width / 2 - 154, this.height - 28, 152, 20, "Clear key");
        buttonList.add(clearKey);
        clearKey.enabled = slotKeybinds.isKeybindableSelected();
    }
    
    @Override
    protected void actionPerformed(GuiButton par1GuiButton) {
        if (!par1GuiButton.enabled) {
            return;
        }

        switch (par1GuiButton.id) {
            case 0:
                mc.displayGuiScreen(parentGui);
                break;
            case 2:
                if (slotKeybinds.isSelected())
                	slotKeybinds.setKeybind(-1);
                break;
            default:

            	break;
        }
    }
    
    @Override
    protected void keyTyped(char c, int keyCode) {
        super.keyTyped(c, keyCode);
        if (!findField.isFocused()) {
        	container.keyTyped(keyCode, c);
        }
        findField.textboxKeyTyped(c, keyCode);
        
        if (findField.isFocused()) {
        	if (!findField.getText().isEmpty()) {
	        	List<Keybind> tempList = new ArrayList<Keybind>();
	        	for (Keybind keybind : Huzuni.KEYBINDS)
	        		if (keybind.getName().toLowerCase().contains(findField.getText().toLowerCase()))
	        			tempList.add(keybind);
	        	slotKeybinds.setComponents(tempList);
        	} else
        		slotKeybinds.setComponents(Huzuni.KEYBINDS);
        }

    }
    
    @Override
    protected void mouseClicked(int par1, int par2, int par3) {
        super.mouseClicked(par1, par2, par3);
        container.mouseClicked(par1, par2, par3);
        findField.mouseClicked(par1, par2, par3);
    }
    
    @Override
    public void drawScreen(int par1, int par2, float par3) {
    	this.drawDefaultBackground();
        drawCenteredString(mc.fontRenderer, "Keybinds", width / 2, 16, 0xFFFFFF);
        clearKey.enabled = slotKeybinds.isKeybindableSelected();
        super.drawScreen(par1, par2, par3);
        container.render();
        findField.drawTextBox();
    }
    
	@Override
	public void updateScreen() {
		super.updateScreen();
		findField.updateCursorCounter();
	}
	
	@Override
	public void onGuiClosed() {
		super.onGuiClosed();
		Huzuni.saveKeybinds();
	}
}
