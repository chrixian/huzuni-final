package net.halalaboos.huzuni.plugin;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Plugin;
import net.minecraft.event.ClickEvent;
import net.minecraft.event.HoverEvent;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IChatComponent;
import org.lwjgl.Sys;


public abstract class VipPlugin extends DefaultPlugin {

	public VipPlugin(String name, int keyCode) {
		super(name, keyCode);
		
	}
	
	public VipPlugin(String name) {
		super(name);
		
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		if (Huzuni.isVip()) {
			super.setEnabled(enabled);
		} else {
			Huzuni.addChatMessage(getErrorMessage());
		}
	}

	private ChatComponentText getErrorMessage() {
		ChatComponentText output = new ChatComponentText(EnumChatFormatting.DARK_GREEN + "[H] " + EnumChatFormatting.RESET + "You must be VIP to use this!");
		ChatComponentText alreadyPurchased = new ChatComponentText(EnumChatFormatting.GREEN + " \247o(Already have VIP?)");
		output.getChatStyle().setChatHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ChatComponentText("\247oClick to get VIP!")));
		alreadyPurchased.getChatStyle().setChatHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ChatComponentText("\247oOur site might be down if your VIP features aren't working.")));
		output.getChatStyle().setChatClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "http://halalaboos.net/donate.html"));
		output.appendSibling(alreadyPurchased);
		return output;
	}
}
