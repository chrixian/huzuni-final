package net.halalaboos.huzuni.plugin.plugins;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import net.halalaboos.huzuni.ClickQueue;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.api.plugin.Plugin;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.GameUtils;
import net.halalaboos.huzuni.util.MathUtils;
import net.halalaboos.huzuni.util.Timer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.item.crafting.ShapelessRecipes;

public final class InventoryHelper extends Plugin {
	
	public static final InventoryHelper instance = new InventoryHelper();
	
	private static final Minecraft mc = Minecraft.getMinecraft();
	
	private static final RenderItem itemRenderer = new RenderItem();
	
    private static final List<IRecipe> craftableRecipes = new ArrayList<IRecipe>();
    
    private static final Timer timer = new Timer();
    
    private static int waitTime = 0;
    
	private InventoryHelper() {
		super("Inventory Helper", "Halalaboos");
		setCategory(Category.PLAYER);
		setDescription("A basic AutoCraft. Buggy currently.");
	}
	
	public void render(int mouseX, int mouseY, int width, int height, float delta) {
		RenderHelper.disableStandardItemLighting();
		int x = 2, y = 2;
        ItemStack mousedItem = null;
        for (IRecipe recipe : craftableRecipes) {
            GLUtils.drawBorderedRect(x, y, x + 20, y + 20, 1, 0x80FFFFFF, 0xAAFFFFFF);
        	renderItem(recipe.getRecipeOutput(), x + 2, y + 2);
        	if (MathUtils.inside(mouseX, mouseY, x, y, 20, 20))
        		mousedItem = recipe.getRecipeOutput();
        	y += 22;
        	if (y + 20 > height) {
        		y = 2;
        		x += 22;
        	}
        }
        if (mousedItem != null) {
            GLUtils.drawBorderedRect(mouseX - 2, mouseY - 9, mouseX + 2 + mc.fontRenderer.getStringWidth(mousedItem.getDisplayName()), mouseY + 2, 1, 0x80FFFFFF, 0xAAFFFFFF);
        	mc.fontRenderer.drawStringWithShadow(mousedItem.getDisplayName(), mouseX, mouseY - 7, 0xFFFFFF);
        }
	}
	
	private void renderItem(ItemStack itemStack, int x, int y) {
		GL11.glPushMatrix();
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        RenderHelper.enableGUIStandardItemLighting();
        GL11.glDepthMask(true);
        GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glTranslatef(0.0F, 0.0F, 32.0F);
		itemRenderer.zLevel = 200.0F;
		GL11.glColor3f(1F, 1F, 1F);
		itemRenderer.renderItemAndEffectIntoGUI(mc.fontRenderer, mc.getTextureManager(), itemStack, x, y);
		itemRenderer.renderItemOverlayIntoGUI(mc.fontRenderer, mc.getTextureManager(), itemStack, x, y);
		itemRenderer.zLevel = 0.0F;
		RenderHelper.disableStandardItemLighting();
        GL11.glDisable(GL12.GL_RESCALE_NORMAL);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glDepthMask(true);
        GL11.glPopMatrix();
	}

	public void updateRecipes(boolean fullCrafting) {
		craftableRecipes.clear();
        for (IRecipe recipe : (List<IRecipe>) CraftingManager.getInstance().getRecipeList())
			if (GameUtils.isCraftable(recipe)) {
				if (fullCrafting ? is3x3(recipe) || is2x2(recipe) : is2x2(recipe))
					craftableRecipes.add(recipe);
			}
	}
	
	private boolean is2x2(IRecipe recipe) {
		if (recipe instanceof ShapedRecipes) {
			ShapedRecipes shapedRecipe = (ShapedRecipes) recipe;
			return shapedRecipe.getRecipeWidth() < 3 && shapedRecipe.getRecipeHeight() < 3;
		} else if (recipe instanceof ShapelessRecipes) {
			return recipe.getRecipeSize() > 4;
		} else
			return false;
	}
	
	private boolean is3x3(IRecipe recipe) {
		if (recipe instanceof ShapedRecipes) {
			ShapedRecipes shapedRecipe = (ShapedRecipes) recipe;
			return shapedRecipe.getRecipeWidth() >= 3 || shapedRecipe.getRecipeHeight() >= 3;
		} else if (recipe instanceof ShapelessRecipes) {
			return recipe.getRecipeSize() > 4;
		} else
			return false;
	}

	public boolean mouseClicked(GuiContainer container, int mouseX, int mouseY, int width, int height, boolean fullCrafting,
			int buttonId) {
		int x = 2, y = 2;
		for (IRecipe recipe : craftableRecipes) {
        	if (MathUtils.inside(mouseX, mouseY, x, y, 20, 20)) {
        		makeItem(recipe, container, fullCrafting);
        		timer.reset();
        		waitTime = recipe.getRecipeSize() * 100;
        		return true;
        	}
        	y += 22;
        	if (y + 20 > height) {
        		y = 2;
        		x += 22;
        	}
        }
		return false;
	}
	
	private void makeItem(IRecipe recipe, GuiContainer container, boolean fullCrafting) {
		// boolean shift = container.isShiftKeyDown();
		if (recipe instanceof ShapedRecipes) {
			ShapedRecipes shapedRecipe = (ShapedRecipes) recipe;
			Slot lastSlot = null;
			ItemStack lastItem = null;
			for (int x = 0; x < 3; x++) {
				for (int y = 0; y < 3; y++) {
					ItemStack neededItem = null;
					if (x >= 0 && y >= 0 && x < shapedRecipe.getRecipeWidth() && y < shapedRecipe.getRecipeHeight()) {
						neededItem = shapedRecipe.getRecipeItems()[x + y * shapedRecipe.getRecipeWidth()];
						if (neededItem != null) {
							if (lastSlot != null && lastItem != null && lastItem.stackSize > 0) {
								if (GameUtils.compare(lastItem, neededItem)) {
									ClickQueue.add(container.field_147002_h.windowId, getCraftingSlotNumber(fullCrafting, x, y), 1, 0);
									lastItem.stackSize--;
									continue;
								} else
									ClickQueue.add(container.field_147002_h.windowId, lastSlot.slotNumber, 0, 0);
							}
							Slot itemSlot = findItem(neededItem, container, fullCrafting);
							if (itemSlot != null) {
								lastSlot = itemSlot;
								lastItem = itemSlot.getStack().copy();
								ClickQueue.add(container.field_147002_h.windowId, itemSlot.slotNumber, 0, 0);
								ClickQueue.add(container.field_147002_h.windowId, getCraftingSlotNumber(fullCrafting, x, y), 1, 0);
							}
						}
					}
				}
			}
			if (lastSlot != null && lastItem != null && lastItem.stackSize > 0)
				ClickQueue.add(container.field_147002_h.windowId, lastSlot.slotNumber, 0, 0);
		} else if (recipe instanceof ShapelessRecipes) {
			ShapelessRecipes shapelessRecipe = (ShapelessRecipes) recipe;
			int x = 0, y = 0;
			Slot lastSlot = null;
			ItemStack lastItem = null;
			for (int i = 0; i < shapelessRecipe.getRecipeItems().size(); i++) {
				ItemStack neededItem = (ItemStack) shapelessRecipe.getRecipeItems().get(i);
				if (neededItem != null) {
					if (lastSlot != null && lastItem != null && lastItem.stackSize > 0) {
						if (GameUtils.compare(lastItem, neededItem)) {
							ClickQueue.add(container.field_147002_h.windowId, getCraftingSlotNumber(fullCrafting, x, y), 1, 0);
							x++;
							if (x >= 2) {
								y++;
								x = 0;
							}
							lastItem.stackSize--;
							continue;
						} else
							ClickQueue.add(container.field_147002_h.windowId, lastSlot.slotNumber, 0, 0);
					}
					Slot itemSlot = findItem(neededItem, container, fullCrafting);
					if (itemSlot != null) {
						lastSlot = itemSlot;
						lastItem = itemSlot.getStack().copy();
						ClickQueue.add(container.field_147002_h.windowId, itemSlot.slotNumber, 0, 0);
						ClickQueue.add(container.field_147002_h.windowId, getCraftingSlotNumber(fullCrafting, x, y), 1, 0);
						x++;
						if (x >= 2) {
							y++;
							x = 0;
						}
					}
				}
			}
			if (lastSlot != null && lastItem != null && lastItem.stackSize > 0)
				ClickQueue.add(container.field_147002_h.windowId, lastSlot.slotNumber, 0, 0);
		}
	}
	
	private int getCraftingSlotNumber(boolean fullCrafting, int x, int y) {
		return fullCrafting ? 1 + x + y * 3 : 1 + x + y * 2;
	}
	
	private Slot findItem(ItemStack item, GuiContainer container, boolean fullCrafting) {
		for (int i = 9; i < 45; i++) {
			int slotNumber = fullCrafting ? i + 1 : i;
			Slot slot = container.field_147002_h.getSlot(slotNumber);
			if (slot != null && slot.getHasStack())
				if (GameUtils.compare(slot.getStack(), item))
					return slot;
		}
		return null;
	}
	
	public boolean finishedCrafting() {
		return timer.hasReach(waitTime) && !ClickQueue.hasQueue();
	}

	@Override
	protected void onEnable() {
	}

	@Override
	protected void onDisable() {
	}
	
}
