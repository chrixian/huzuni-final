package net.halalaboos.huzuni.plugin.plugins.combat;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.minecraft.block.material.Material;
import net.minecraft.network.play.client.C02PacketUseEntity;

/**
 * @author brudin
 * @version 1.0
 * @since 1:33 AM on 8/20/2014
 */
public class Criticals extends DefaultPlugin implements PacketListener {

	public Criticals() {
		super("Criticals");
		setAuthor("brudin");
		setCategory(Category.COMBAT);
		setDescription("Attempts to force criticals when attacking.");
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
	}

	@Override
	public void onPacket(PacketEvent event, PacketEvent.PacketType type) {
		if(type == PacketEvent.PacketType.SENT) {
			if(event.getPacket() instanceof C02PacketUseEntity) {
				C02PacketUseEntity packetUseEntity = (C02PacketUseEntity)event.getPacket();
				if(packetUseEntity.func_149565_c() == C02PacketUseEntity.Action.ATTACK) {
					if (shouldCritical()) {
						doCrit();
					}
				}
			}
		}
	}

	private void doCrit() {
		boolean preGround = mc.thePlayer.onGround;
		mc.thePlayer.onGround = false;
		mc.thePlayer.motionY = 0.13F;
		mc.thePlayer.onGround = preGround;
	}

	private boolean shouldCritical() {
		return !mc.thePlayer.isInWater() && mc.thePlayer.onGround && !mc.thePlayer.isOnLadder();
	}
}
