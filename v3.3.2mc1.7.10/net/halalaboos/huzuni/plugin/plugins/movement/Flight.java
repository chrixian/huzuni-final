package net.halalaboos.huzuni.plugin.plugins.movement;

import net.halalaboos.huzuni.api.plugin.ValueOption;
import net.halalaboos.huzuni.plugin.value.Value;
import net.halalaboos.huzuni.plugin.value.ValueManager;
import org.lwjgl.input.Keyboard;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;

public class Flight extends DefaultPlugin implements UpdateListener {

	private final Value speed = new Value("Flight Speed", 1, 1, 5, 0.5F);

    public Flight() {
        super("Flight", Keyboard.KEY_F);
        this.setCategory(Category.MOVEMENT);
		ValueManager.add(speed);
		this.setOptions(new ValueOption(speed));
        setDescription("Allows you to fly.");
        setAuthor("Halalaboos");
    }

    @Override
    public void onPreUpdate(MotionUpdateEvent event) {
		float value = speed.getValue() / 20;
		mc.thePlayer.capabilities.setFlySpeed(value);
        mc.thePlayer.capabilities.isFlying = true;
		if(mc.thePlayer.fallDistance > 3) {
			mc.thePlayer.onGround = true;
		}
    }

    @Override
    public void onPostUpdate(MotionUpdateEvent event) {
		if(mc.thePlayer.fallDistance > 3) {
			mc.thePlayer.onGround = false;
		}
    }

    @Override
    protected void onEnable() {
        Huzuni.registerUpdateListener(this);
    }

    @Override
    protected void onDisable() {
        Huzuni.unregisterUpdateListener(this);
		mc.thePlayer.capabilities.setFlySpeed(0.05F);
        if (mc.thePlayer != null)
            mc.thePlayer.capabilities.isFlying = false;
    }

}
