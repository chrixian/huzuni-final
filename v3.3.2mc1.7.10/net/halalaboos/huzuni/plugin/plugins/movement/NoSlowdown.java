package net.halalaboos.huzuni.plugin.plugins.movement;

import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.plugin.DefaultPlugin;

/**
 * @author brudin
 * @version 1.0
 * @since 12:33 AM on 8/6/2014
 */
public class NoSlowdown extends DefaultPlugin {

	/**
	 * EntityPlayerSP.java -> onLivingUpdate()
	 */

	public static final NoSlowdown instance = new NoSlowdown();

	private NoSlowdown() {
		super("No Slowdown");
		setAuthor("brudin");
		setCategory(Category.MOVEMENT);
		setDescription("Prevents slowing down when eating.");
	}

	@Override
	protected void onEnable() {
	}

	@Override
	protected void onDisable() {
	}
}
