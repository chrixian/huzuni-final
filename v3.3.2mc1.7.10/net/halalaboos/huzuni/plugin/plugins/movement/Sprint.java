package net.halalaboos.huzuni.plugin.plugins.movement;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;

import org.lwjgl.input.Keyboard;

/**
 * @author brudin
 * @version 1.0
 * @since 3/25/14
 */
public class Sprint extends DefaultPlugin implements UpdateListener {

	public Sprint() {
		super("Sprint", Keyboard.KEY_M);
		setCategory(Category.MOVEMENT);
		setAuthor("brudin");
		setDescription("Automatically sprints for you.");
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
		if (mc.thePlayer != null)
			mc.thePlayer.setSprinting(false);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		mc.thePlayer.setSprinting(mc.thePlayer.moveForward > 0 && !mc.thePlayer.isSneaking() && !mc.thePlayer.isCollidedHorizontally && mc.thePlayer.getFoodStats().getFoodLevel() > 6);
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {

	}
}
