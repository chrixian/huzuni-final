package net.halalaboos.huzuni.plugin.plugins.player;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.api.plugin.ValueOption;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.plugin.value.Value;
import net.halalaboos.huzuni.plugin.value.ValueManager;

public class Fastplace extends DefaultPlugin implements UpdateListener {

	private Value speed = new Value("Fastplace Speed", 0F, 2F, 4F, 1F);
	
    public Fastplace() {
        super("Fast Place", -1);
        setCategory(Category.PLAYER);
        setDescription("Place blocks faster.");
        ValueManager.add(speed);
        this.setOptions(new ValueOption(speed));
    }

    @Override
    protected void onEnable() {
        Huzuni.registerUpdateListener(this);
    }

    @Override
    protected void onDisable() {
    	Huzuni.unregisterUpdateListener(this);
    }


	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
    	float speed = this.speed.getValue();
        if (mc.rightClickDelayTimer > (4 - (byte) speed))
            mc.rightClickDelayTimer = (4 - (byte) speed);
    }

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
	}

}
