package net.halalaboos.huzuni.plugin.plugins.player;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.minecraft.network.play.client.C16PacketClientStatus;
import net.minecraft.network.play.server.S06PacketUpdateHealth;

/**
 * @author brudin
 * @version 1.0
 * @since 3/25/14
 */
public class Respawn extends DefaultPlugin implements PacketListener {

	public Respawn() {
		super("Respawn");
		setCategory(Category.PLAYER);
		setAuthor("brudin");
		setDescription("Automatically respawns you when you die.");
		setVisible(false);
	}

	@Override
	public void onPacket(PacketEvent event, PacketEvent.PacketType type) {
		if(type == PacketEvent.PacketType.READ) {
			if(event.getPacket() instanceof S06PacketUpdateHealth) {
				S06PacketUpdateHealth packet = (S06PacketUpdateHealth)event.getPacket();
				if(packet.func_149332_c() > 0.0F)
					return;
				event.setCancelled(true);
				mc.thePlayer.sendQueue.addToSendQueue(new C16PacketClientStatus(C16PacketClientStatus.EnumState.PERFORM_RESPAWN));
			}
		}
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
	}
}
