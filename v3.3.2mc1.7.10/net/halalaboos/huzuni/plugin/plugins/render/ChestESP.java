package net.halalaboos.huzuni.plugin.plugins.render;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.render.RenderEvent;
import net.halalaboos.huzuni.event.render.Renderable;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.rendering.Box;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.tileentity.TileEntityEnderChest;
import net.minecraft.util.AxisAlignedBB;

import org.lwjgl.input.Keyboard;

import static org.lwjgl.opengl.GL11.*;

/**
 * @author brudin
 * @version 1.0
 * @since 4/10/14
 */
public class ChestESP extends DefaultPlugin implements Renderable {

	private final Box normal, left, right;
	
	public ChestESP() {
		super("Chest ESP", Keyboard.KEY_Y);
		setCategory(Category.RENDER);
		setVisible(false);
		setDescription("Draws a box around chests.");
		normal = new Box(AxisAlignedBB.getBoundingBox(0, 0, 0, 1, 1, 1));
		left = new Box(AxisAlignedBB.getBoundingBox(0, 0, 0, 2, 1, 1));
		right = new Box(AxisAlignedBB.getBoundingBox(0, 0, 0, 1, 1, 2));
	}

	@Override
	protected void onEnable() {
		Huzuni.registerRenderable(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterRenderable(this);
	}

	@Override
	public void render(RenderEvent event) {
		for (Object o : mc.theWorld.field_147482_g) {
			if (o instanceof TileEntityChest) {
				final TileEntityChest chest = (TileEntityChest) o;
				final double renderX = chest.field_145851_c - RenderManager.renderPosX;
				final double renderY = chest.field_145848_d - RenderManager.renderPosY;
				final double renderZ = chest.field_145849_e - RenderManager.renderPosZ;
				glPushMatrix();
				glTranslated(renderX, renderY, renderZ);
				
				if (chest.blockType == Blocks.trapped_chest)
					glColor4f(1F, 0F, 0F, 0.1F);
				else 
					glColor4f(1F, 1F, 0F, 0.1F);
				
				if (chest.field_145990_j != null) {
					left.setOpaque(true);
					left.render();
				} else if (chest.field_145988_l != null) {
					right.setOpaque(true);
					right.render();
				} else if (chest.field_145990_j == null && chest.field_145988_l == null && chest.field_145991_k == null && chest.field_145992_i == null) {
					normal.setOpaque(true);
					normal.render();
				}
				glPopMatrix();
			} else if (o instanceof TileEntityEnderChest) {
				final TileEntityEnderChest chest = (TileEntityEnderChest) o;
				final double renderX = chest.field_145851_c - RenderManager.renderPosX;
				final double renderY = chest.field_145848_d - RenderManager.renderPosY;
				final double renderZ = chest.field_145849_e - RenderManager.renderPosZ;
				glPushMatrix();
				glTranslated(renderX, renderY, renderZ);
				normal.setOpaque(true);
				glColor4f(1F, 0.1F, 1F, 0.1F);
				normal.render();
				glPopMatrix();
			}
		}
	}
}
