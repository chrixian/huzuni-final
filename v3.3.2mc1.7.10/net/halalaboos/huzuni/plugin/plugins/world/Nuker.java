package net.halalaboos.huzuni.plugin.plugins.world;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.ModeOption;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.api.plugin.ValueOption;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.plugin.value.Value;
import net.halalaboos.huzuni.plugin.value.ValueManager;
import net.halalaboos.huzuni.util.GameUtils;
import net.halalaboos.huzuni.util.Timer;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C0APacketAnimation;
import net.minecraft.util.MovingObjectPosition;

import org.lwjgl.input.Keyboard;

public class Nuker extends DefaultPlugin implements UpdateListener {
	
	private float curBlockDamage = 0F, oldYaw = 0F, oldPitch = 0F;
	
	private Block selectedBlock = null, block = null;
	
	private int x = -1, y = -1, z = -1, side = -1, blockHitDelay = 0;
		
	private boolean hasBlock = false;

	private final Value radius = new Value("Nuker Radius", 2F, 3F, 5F, 1F);
	
	private final Value speed = new Value("Nuker Speed", 0F, 3F, 5F, 1F);

	private final Timer blockSelectTimer = new Timer();
	
	private final ModeOption mode;
	
	public Nuker() {
		super("Nuker", Keyboard.KEY_L);
		setCategory(Category.WORLD);
		setAuthor("Halalaboos");
		setDescription("Mines blocks. Lots of them.");
		setOptions(mode = new ModeOption("Nuker mode", "Real", "Instant"), new ValueOption(radius), new ValueOption(speed));
		ValueManager.add(radius);
		ValueManager.add(speed);	
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
		if (hasSelectedBlock()) {
			sendPacket(1);
		}
		reset();
		selectedBlock = null;
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		oldYaw = mc.thePlayer.rotationYaw;
		oldPitch = mc.thePlayer.rotationPitch;
		getSelectedBlock();
		if (hasBlock = hasSelectedBlock()) {
			if (GameUtils.getDistance(x, y, z) > radius.getValue()) {
				sendPacket(1);
				reset();
			} else
				GameUtils.face(x + 0.5F, y + 0.5F, z + 0.5F);
			return;
		}
		int radius = (int) this.radius.getValue();
		
		int furthestX = -1, furthestY = -1, furthestZ = -1, furthestSide = -1;
		Block furthestBlock = null;
		
		for (int i = radius; i >= -radius; i--) {
			for (int j = -radius; j <= radius; j++) {
				for (int k = radius; k >= -radius; k--) {
					x = (int) (mc.thePlayer.posX + i);
					y = (int) (mc.thePlayer.posY + j);
					z = (int) (mc.thePlayer.posZ + k);
					block = mc.theWorld.getBlock(x, y, z);

					if ((block != null && block.getMaterial() != Material.air) && block == selectedBlock && GameUtils.getDistance(x, y, z) < radius) {
												
						if (mode.getSelected() == 1) {
							side = 1;
							if (mc.playerController.isInCreativeMode()) {
								sendPacket(0);
								breakBlock();
							} else {
								sendPacket(0);
								sendPacket(2);
								breakBlock();
							}
						} else if (mode.getSelected() == 0) {
							MovingObjectPosition result = rayTrace();
							side = result != null ? result.sideHit : 1;
							
							if (isSame(result)) {
								if (furthestBlock == null) {
									furthestBlock = block;
									furthestX = x;
									furthestY = y;
									furthestZ = z;
									furthestSide = side;
								} else {
									if (GameUtils.getDistance(x, y, z) > GameUtils.getDistance(furthestX, furthestY, furthestZ)) {
										furthestBlock = block;
										furthestX = x;
										furthestY = y;
										furthestZ = z;
										furthestSide = side;
									}
								}
							}
						}
					}
				}
			}
		}
		
		if (furthestBlock != null) {
			x = furthestX;
			y = furthestY;
			z = furthestZ;
			side = furthestSide;
			block = furthestBlock;
			GameUtils.face(x + 0.5F, y + 0.5F, z + 0.5F);
			curBlockDamage = 0;
			blockHitDelay = 5 - (int) speed.getValue();
		} else {
			x = -1;
			y = -1;
			z = -1;
			block = null;
			side = 0;
		}
	}
	
	private void getSelectedBlock() {
		if (mc.objectMouseOver != null && mc.objectMouseOver.typeOfHit == MovingObjectPosition.MovingObjectType.BLOCK && blockSelectTimer.hasReach(500)) {
			if (mc.gameSettings.keyBindUseItem.getIsKeyPressed()) {
				Block block = mc.theWorld.getBlock(mc.objectMouseOver.blockX, mc.objectMouseOver.blockY, mc.objectMouseOver.blockZ);
				if (block == Blocks.air || block == null)
					selectedBlock = null;
				else {
					if (selectedBlock != block) {
						selectedBlock = block;
						Huzuni.addChatMessage("Now nuking " + GameUtils.getBlockName(selectedBlock));
						blockSelectTimer.reset();
					}
				}
			}
		}
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
		mc.thePlayer.rotationYaw = oldYaw;
		mc.thePlayer.rotationPitch = oldPitch;
		if (hasBlock) {
			if (blockHitDelay > 0) {
				blockHitDelay--;
			} else {
	            mc.thePlayer.sendQueue.addToSendQueue(new C0APacketAnimation(mc.thePlayer, 1));
				if(block == null) {
					return;
				}
				if (curBlockDamage == 0) {
					sendPacket(0);
					if (mc.playerController.isInCreativeMode() || block.getPlayerRelativeBlockHardness(mc.thePlayer, mc.theWorld, x, y, z) >= 1) {
						breakBlock();
						reset();
						return;
					}
				}
				curBlockDamage += block.getPlayerRelativeBlockHardness(mc.thePlayer, mc.theWorld, x, y, z);
                mc.theWorld.destroyBlockInWorldPartially(mc.thePlayer.getEntityId(), x, y, z, (int)(curBlockDamage * 10.0F) - 1);

				if (curBlockDamage > 1.0F) {
					sendPacket(2);
					breakBlock();
					reset();
				}
			}
		}
	}
	
	private boolean hasSelectedBlock() {
		return mode.getSelected() == 0 && selectedBlock != null && block != null && block.getMaterial() != Material.air && block == selectedBlock;
	}
	
	private void breakBlock() {
		GameUtils.breakBlock(x, y, z);
	}
	
	private void sendPacket(int mode) {
		mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(mode, x, y, z, side));
	}
	
	private MovingObjectPosition rayTrace() {
        return GameUtils.rayTrace(x + 0.5F, y + 0.5F, z + 0.5F);
	}
	
	private boolean isSame(MovingObjectPosition result) {
		return result != null && result.blockX == x && result.blockY == y && result.blockZ == z;
	}
	
	private void reset() {
		block = null;
		x = -1;
		y = -1;
		z = -1;
		side = -1;
		curBlockDamage = 0;
		blockHitDelay = 5 - (int) (speed.getValue());
	}

}