package net.halalaboos.huzuni.plugin.plugins.world;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.ValueOption;
import net.halalaboos.huzuni.util.FileUtils;
import net.minecraft.block.material.Material;

import org.lwjgl.input.Keyboard;

import static org.lwjgl.opengl.GL11.*;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.api.plugin.ModeOption;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.plugin.value.Value;
import net.halalaboos.huzuni.plugin.value.ValueManager;
import net.halalaboos.huzuni.rendering.Box;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.util.AxisAlignedBB;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class Xray extends DefaultPlugin {

	public static final Xray instance = new Xray();

	private boolean enabledblocks[] = new boolean[4096];

	private float preGamma;

	private final Value opacity = new Value("Xray Opacity", 0F, 70F, 100F, 1F);
	private final File saveFile = new File(Huzuni.SAVE_DIRECTORY, "XrayBlocks.txt");

	/**
	 * Block.getRenderBlockPass for changing the render block pass
	 * RenderBlocks.renderStandardBlock for rendering xray blocks
	 * Tessellator.setColorOpaque_F for setting block alpha
	 * 		Right now it's just set to 140, we can add in the custom opacity once it's actually
	 * 		configurable in-game.
	 */
	private Xray() {
		super("Xray", Keyboard.KEY_X);
		setDescription("X-ray for blocks.");
		setAuthor("Halalaboos");
		this.setCategory(Category.WORLD);
		ValueManager.add(opacity);
		ValueOption valueOption = new ValueOption(opacity) {
			@Override
			public void mouseReleased(int x, int y, int buttonId) {
				super.mouseReleased(x, y, buttonId);
				if(isEnabled()) {
					mc.renderGlobal.loadRenderers();
				}
			}
		};
		setOptions(valueOption);
	}

	@Override
	protected void onEnable() {
		preGamma = mc.gameSettings.gammaSetting;
		mc.gameSettings.gammaSetting = 10;
	}

	@Override
	protected void onDisable() {
		mc.gameSettings.gammaSetting = preGamma;
	}

	@Override
	public void toggle() {
		super.toggle();
		Minecraft.getMinecraft().renderGlobal.loadRenderers();
	}

	public boolean isBlockEnabled(int blockId) {
		return enabledblocks[blockId];
	}

	public int getRenderBlockPass(Block block) {
		return isEnabled() ? isBlockEnabled(Block.getIdFromBlock(block)) ? 0 : 1 : 0;
	}

	public boolean renderAsNormalBlock(Block block) {
		return !isEnabled();
	}

	public int getOpacity() {
		int opacity = (int) ((this.opacity.getValue() / 100F) * 255F);
		if (isEnabled() && opacity > 11)
			return opacity;
		else
			return 255;
	}

	private final int ignoredBlocks[] = {
			6, 31, 32, 37, 38, 83, 106, 111, 175
	};

	public boolean shouldIgnore(int id) {
		return Arrays.binarySearch(ignoredBlocks, id) >= 0;
	}

	public boolean contains(int id) {
		return enabledblocks[id];
	}

	public void add(int id) {
		enabledblocks[id] = true;

	}

	public void remove(int id) {
		enabledblocks[id] = false;

	}

	public void clear() {
		for (int i = 0; i < enabledblocks.length; i++)
			enabledblocks[i] = false;
	}

	public void load() {
		if (!saveFile.exists()) {
			try {
				saveFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}
		clear();
		for (String content : FileUtils.readFile(saveFile)) {
			try {
				enabledblocks[Integer.parseInt(content)] = true;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void save() {
		if (!saveFile.exists())
			try {
				saveFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		List<String> lines = new ArrayList<String>();
		for (int i = 0; i < enabledblocks.length; i++) {
			if (enabledblocks[i])
				lines.add(i + "");
		}
		FileUtils.writeFile(saveFile, lines);
	}

	public void toggleBlock(int blockId) {
		enabledblocks[blockId] = !enabledblocks[blockId];
	}

	public int getMode() {
		return 0;
	}

}
