/**
 *
 */
package net.halalaboos.huzuni.util;

import static org.lwjgl.opengl.GL11.*;

import java.awt.*;
import java.util.Random;

import net.halalaboos.huzuni.Huzuni;
import net.minecraft.client.Minecraft;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

/**
 * @author Halalaboos
 * @since Aug 18, 2013
 */
public final class GLUtils {
	
	private static final Minecraft mc = Minecraft.getMinecraft();
	
	private static final Random random = new Random();
		
    private GLUtils() {
    	
    }

	/**
	 * Enables GL constants for 3D rendering.
	 */
	public static void enableGL3D() {
		glDisable(GL_ALPHA_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_DEPTH_TEST);
		glDepthMask(false);
		if (Huzuni.isAntialias())
            glEnable(GL_LINE_SMOOTH);
		glLineWidth(Huzuni.getLineSize());
		mc.entityRenderer.disableLightmap(0);
	}

	/**
	 * Disables GL constants for 3D rendering.
	 */
	public static void disableGL3D() {
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_DEPTH_TEST);
		glDisable(GL_BLEND);
		glEnable(GL_ALPHA_TEST);
		glDepthMask(true);
		if (Huzuni.isAntialias())
			glDisable(GL_LINE_SMOOTH);
	}

    public static int getMouseX() {
    	return (int) ((Mouse.getX() * getScreenWidth() / mc.displayWidth));
    }

    public static int getMouseY() {
    	return (int) ((getScreenHeight() - Mouse.getY() * getScreenHeight() / mc.displayHeight - 1));
    }

    public static int getScreenWidth() {
    	return mc.displayWidth / 2;
    }

    public static int getScreenHeight() {
    	return mc.displayHeight / 2;
    }

    public static Color getColorWithAffects(Color color, boolean mouseOver,
                                     boolean mouseDown) {
    	return mouseOver ? (mouseDown ? color.darker() : color.brighter()) : color;
    }
    
    public static void glColor(Color color) {
        glColor4f((float) color.getRed() / 255F, (float) color.getGreen() / 255F, (float) color.getBlue() / 255F, (float) color.getAlpha() / 255F);
    }
    
    public static void glColor(Color color, float alpha) {
        glColor4f((float) color.getRed() / 255F, (float) color.getGreen() / 255F, (float) color.getBlue() / 255F, alpha);
    }

    public static Color getRandomColor() {
        return getRandomColor(1000, 0.6F);
    }
    
    public static void glColor(int hex) {
        float alpha = (float) (hex >> 24 & 255) / 255.0F;
        float red = (float) (hex >> 16 & 255) / 255.0F;
        float green = (float) (hex >> 8 & 255) / 255.0F;
        float blue = (float) (hex & 255) / 255.0F;
        glColor4f(red, green, blue, alpha);
    }
    
    public static void drawRect(double x, double y, double x1, double y1, int color) {
        glDisable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glColor(color);
        drawRect((double) x, (double) y, (double) x1, (double) y1);
        glEnable(GL_TEXTURE_2D);
        glDisable(GL_BLEND);
    }
    
    public static void drawRect(double x, double y, double x1, double y1) {
    	glDisable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    	glBegin(GL_QUADS);
        glVertex2d(x, y1);
        glVertex2d(x1, y1);
        glVertex2d(x1, y);
        glVertex2d(x, y);
        glEnd();
        glEnable(GL_TEXTURE_2D);
        glDisable(GL_BLEND);
    }

    public static void drawRect(float[] area) {
    	drawRect(area[0], area[1], area[0] + area[2], area[1] + area[3]);
    }
    
	public static void drawRect(Rectangle rectangle, int color) {
		drawRect(rectangle.x, rectangle.y, rectangle.x + rectangle.width, rectangle.y + rectangle.height, color);
	}

	/**
     * This isn't mine, but it's the most beautiful random color generator I've seen. Reminds me of easter.
     * */
    public static Color getRandomColor(int saturationRandom, float luminance) {
        final float hue = random.nextFloat();
        final float saturation = (random.nextInt(saturationRandom) + 1000) / (float) saturationRandom + 1000F;
        return Color.getHSBColor(hue, saturation, luminance);
    }
	
	public static void drawTexturedRect(float x, float y, float width, float height, float u, float v, float t, float s) {
		glBegin(GL_TRIANGLES);
        glTexCoord2f(t, v);
        glVertex2d(x + width, y);
        glTexCoord2f(u, v);
        glVertex2d(x, y);
        glTexCoord2f(u, s);
        glVertex2d(x, y + height);
        glTexCoord2f(u, s);
        glVertex2d(x, y + height);
        glTexCoord2f(t, s);
        glVertex2d(x + width, y + height);
        glTexCoord2f(t, v);
        glVertex2d(x + width, y);
        glEnd();
	}

	/**
	 * Draws a bordered rectangle at the coordinates specified with the hexadecimal color.
	 */
	public static void drawBorderedRect(double x, double y, double x1, double y1, float width, int internalColor, int borderColor) {
		glColor(internalColor);
		drawRect(x + width, y + width, x1 - width, y1 - width);
		glColor(borderColor);
		drawRect(x + width, y, x1 - width, y + width);
		drawRect(x, y, x + width, y1);
		drawRect(x1 - width, y, x1, y1);
		drawRect(x + width, y1 - width, x1 - width, y1);
	}
	
	public static void glScissor(int x, int y, int x1, int y1) {
		int factor = GLUtils.getScaleFactor();
		GL11.glScissor((int) (x * factor), (int) (mc.displayHeight - (y1 * factor)), (int) ((x1 - x) * factor), (int) ((y1 - y) * factor));
		
	}
	
	public static int getScaleFactor() {
		int scaleFactor = 1;
		boolean isUnicode = mc.func_152349_b();
		int scaleSetting = mc.gameSettings.guiScale;

		if (scaleSetting == 0) {
			scaleSetting = 1000;
		}
		while (scaleFactor < scaleSetting && mc.displayWidth / (scaleFactor + 1) >= 320 && mc.displayHeight / (scaleFactor + 1) >= 240) {
			scaleFactor++;
		}

		if (isUnicode && scaleFactor % 2 != 0 && scaleFactor != 1) {
			scaleFactor--;
		}
		return scaleFactor;
	}
}
