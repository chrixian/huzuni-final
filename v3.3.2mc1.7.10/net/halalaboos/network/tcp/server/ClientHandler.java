package net.halalaboos.network.tcp.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import net.halalaboos.network.api.Handler;
import net.halalaboos.network.api.Packet;

public abstract class ClientHandler extends Thread implements Handler {

	private final Server server;
	
	private final Socket socket;
	
	private final DataInputStream inputStream;
	
	private final DataOutputStream outputStream;

	public ClientHandler(Server server, Socket socket) throws IOException {
		this.server = server;
		this.socket = socket;
		this.inputStream = new DataInputStream(socket.getInputStream());
		this.outputStream = new DataOutputStream(socket.getOutputStream());
	}
	
	@Override
	public void run() {
		try {
			server.onConnect(this);
			server.getConnectionHandler().getClients().add(this);
			while (isConnected()) {
				byte id = inputStream.readByte();
				server.getProtocolHandler().read(id, this);
			}
		} catch (Exception e) {
			System.err.println("CLIENT READ ERR: " + e.getMessage());
		} finally {
			server.getConnectionHandler().getClients().remove(this);
			try {
				cleanup();
			} catch (IOException e) {
				System.err.println("CLIENT CLEANUP ERR: " + e.getMessage());
			}
			server.onDisconnect(this);
		}
	}
	
	public void close() {
		if (isConnected()) {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void cleanup() throws IOException {
		if (inputStream != null) {
			inputStream.close();
		}
		if (outputStream != null) {
			outputStream.close();
		}
		
	}
	
	public boolean isConnected() {
		return socket.isClosed() ? false : socket.isConnected();
	}

	public String getAddress() {
		return socket.getInetAddress().getHostAddress();
	}
	
	@Override
	public Socket getSocket() {
		return socket;
	}

	@Override
	public DataInputStream getInputStream() {
		return inputStream;
	}

	@Override
	public DataOutputStream getOutputStream() {
		return outputStream;
	}

	protected Server getServer() {
		return server;
	}

	public void write(int id, Object data) {
		server.getProtocolHandler().write(id, data, this);
	}
	
	@Override
	public void onRecievedPacket(Packet packet, Object data) {
		server.onRecievedPacket(this, packet, data);
	}

}
