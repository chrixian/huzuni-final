package pw.brudin.huzuni.screen;

import net.halalaboos.huzuni.gui.screen.ChangelogScreen;
import net.minecraft.realms.RealmsBridge;
import pw.brudin.huzuni.util.screen.BruButton;
import pw.brudin.huzuni.util.screen.PanoramaRenderer;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.particle.ParticleEngine;
import net.halalaboos.huzuni.gui.screen.HuzuniDonorFeatures;
import net.halalaboos.huzuni.gui.screen.HuzuniOptions;
import net.halalaboos.huzuni.gui.screen.alt.AltManager;
import net.halalaboos.huzuni.rendering.Texture;
import net.minecraft.client.gui.*;
import net.minecraft.client.resources.I18n;
import org.lwjgl.Sys;
import org.lwjgl.opengl.GL11;

/**
 * The new main menu.
 *
 * @author brudin
 * @version 1.0
 * @since 1/4/14
 */
public class MainMenu extends GuiScreen {

    private PanoramaRenderer panoramaRenderer;
    private static final Texture TITLE = new Texture("huzuni/title.png");
    private static final ParticleEngine PARTICLE_ENGINE = new ParticleEngine(false);

    public MainMenu() {}

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }

    @Override
    public void initGui() {
        panoramaRenderer = new PanoramaRenderer(width, height);
        panoramaRenderer.init();
        buttonList.clear();
		int size = width / 6 + 1;
		int i = 0;
		buttonList.add(new BruButton(1, i, 0, size, 20, I18n.format("menu.singleplayer")));
		buttonList.add(new BruButton(2, i += size, 0, size, 20, I18n.format("menu.multiplayer")));
		buttonList.add(new BruButton(0, i += size, 0, size, 20, "Changelog"));
		buttonList.add(new BruButton(3, i += size, 0, size, 20, "Accounts"));
		buttonList.add(new BruButton(7, i += size, 0, size, 20, Huzuni.isDonator() ? "Donator Perks" : "VIP/Donate"));
		buttonList.add(new BruButton(5, i += size, 0, size, 20, "Huzuni"));
		buttonList.add(new BruButton(4, width - 80, height - 20, 80, 20, I18n.format("menu.quit")));
		buttonList.add(new BruButton(10, width - 80, height - 40, 80, 20, "Options"));
		if (Huzuni.getUpdateCode() == 1)
			buttonList.add(new BruButton(6, width / 2 - 22, this.height / 2 + 30, 45, 20, "Update"));
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        switch (button.id) {
            case 10:
                this.mc.displayGuiScreen(new GuiOptions(this, this.mc.gameSettings));
                break;

            case 1:
                this.mc.displayGuiScreen(new GuiSelectWorld(this));
                break;

            case 2:
                this.mc.displayGuiScreen(new GuiMultiplayer(this));
                break;

            case 3:
                this.mc.displayGuiScreen(new AltManager(this));
                break;

            case 4:
                this.mc.shutdown();
                break;

            case 5:
               this.mc.displayGuiScreen(new HuzuniOptions(this));
                break;

            case 6:
                Sys.openURL("http://halalaboos.net/huzuni.html");
                break;

            case 7:
                if (Huzuni.isDonator())
                    mc.displayGuiScreen(new HuzuniDonorFeatures(this));
                else
                    Sys.openURL("http://halalaboos.net/donate.html");
                break;
            case 8:
                Sys.openURL("http://halalaboos.net/huzuni.html");
            	break;
            case 0:
				mc.displayGuiScreen(new ChangelogScreen(this));
				break;
        }
    }

    @Override
    protected void mouseClicked(int x, int y, int buttonID) {
        super.mouseClicked(x, y, buttonID);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        super.drawDefaultBackground();
		String versionStatus = getReturnCodeStatus(Huzuni.getUpdateCode());
		float titleX = width / 2 - 150, titleY = height / 2 - 70;
		GL11.glColor3f(1, 1, 1);
		panoramaRenderer.renderSkybox(mouseX, mouseY, partialTicks);
		PARTICLE_ENGINE.render();
		super.drawScreen(mouseX, mouseY, partialTicks);
		GL11.glColor3f(1F, 1F, 1F);
		TITLE.render(titleX, titleY + 10, 300, 100);
		Huzuni.drawString(mc.getSession().getUsername() + " - " + "v" + Huzuni.VERSION + (Huzuni.isVip() ? " (VIP)" : ""), 2, height - Huzuni.getStringHeight("|"), 0x4fcccccc);
		Huzuni.drawString(versionStatus, width / 2 - Huzuni.getStringWidth(versionStatus) / 2, titleY + 90, 0xCFCCCCCC);
		panoramaRenderer.renderFade();
    }

    /**
     * @return String representing what the version return code means.
     */
    private String getReturnCodeStatus(int versionReturnCode) {
        switch (versionReturnCode) {
            case -1:
                return "Unable to connect.";
            case 0:
                return Huzuni.isVip() ? "Thank you :)" : "Donate!!";
            case 1:
                return "Out of date!";
            case -2:
                return "Retrieving...";
        }
        return "" + versionReturnCode;
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        panoramaRenderer.panoramaTick();
        PARTICLE_ENGINE.updateParticles();
    }
}