package net.halalaboos.huzuni.api.gui.layout;

import java.awt.Rectangle;
import java.util.List;

import net.halalaboos.huzuni.api.gui.Component;
import net.halalaboos.huzuni.api.gui.Container;

public interface Layout {

	Rectangle layout(Container container, List<Component> components);
	
}
