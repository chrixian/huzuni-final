package net.halalaboos.huzuni.api.mod;

/**
 * @author brudin
 * @version 1.0
 * @since 3/25/14
 */
public enum Category {

	MOVEMENT("Movement"),
	PLAYER("Player"),
	RENDER("Render"),
	WORLD("World"),
	CHAT("Chat"),
	COMBAT("Combat"),
	UNFINISHED("Unfinished");

	public final String formalName;

	Category(String formalName) {
		this.formalName = formalName;
	}
}
