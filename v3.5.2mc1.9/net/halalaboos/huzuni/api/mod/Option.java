package net.halalaboos.huzuni.api.mod;

import org.w3c.dom.Element;

import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.util.MathUtils;

public abstract class Option {

	protected float[] area = new float[] { 0, 0, 0, 12 };
	
	protected String name;

	protected String description;
	
	public Option(String name, String description) {
		this.name = name;
		this.description = description;
	}
	
	public abstract boolean mouseClicked(int x, int y, int buttonId);
	
	public abstract void mouseReleased(int x, int y, int buttonId) ;
	
	public abstract void render(Theme theme, int index, float[] area, boolean mouseOver, boolean mouseDown);
	
	public abstract void load(Element element);
	
	public abstract void save(Element element);

	protected boolean isPointInside(int x, int y) {
		return MathUtils.inside(x, y, area);
	}
	
	public void setArea(float[] area) {
		this.area = area;
	}
	
	public float getX() {
		return area[0];
	}
	
	public float getY() {
		return area[1];
	}
	
	public float getWidth() {
		return area[2];
	}
	
	public float getHeight() {
		return area[3];
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public float[] getArea() {
		return area;
	}
		
}
