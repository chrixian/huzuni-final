package net.halalaboos.huzuni.api.mod;

import org.w3c.dom.Element;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.gui.render.Theme;

public class ToggleOption extends Option {

	private boolean enabled = false;
	
	public ToggleOption(String name, String description) {
		super(name, description);
		
	}

	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
		return this.isPointInside(x, y);
	}

	@Override
	public void mouseReleased(int x, int y, int buttonId) {
		if (this.isPointInside(x, y)) {
			setEnabled(!enabled);
		}
	}

	@Override
	public void render(Theme theme, int index, float[] area, boolean mouseOver,
			boolean mouseDown) {
		float size = area[3] - 1;
		float[] area1 = { area[0], area[1], size, size };
		theme.renderSlot(0, 0, index, area1, enabled, mouseOver, mouseDown);
		Huzuni.drawString(this.name, area[0] + size + 2, area[1] + 2, 0xFFFFFF);
	}

	@Override
	public void load(Element element) {
		this.enabled = Boolean.parseBoolean(element.getAttribute(name.replaceAll(" ", "_")));
	}

	@Override
	public void save(Element element) {
		element.setAttribute(name.replaceAll(" ", "_"), Boolean.toString(enabled));
	}


	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
