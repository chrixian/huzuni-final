package net.halalaboos.huzuni.api.request;

import net.halalaboos.huzuni.api.mod.Mod;

public interface Applicant <D> {
	
	void onRequestDenied();
	
	void onRequestGranted(D data);
	
	void onRequestCancelled();
	
	Mod getMod();
}
