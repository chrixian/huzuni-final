package net.halalaboos.huzuni.api.request;

import net.halalaboos.huzuni.api.mod.Mod;

public class SimpleApplicant <D> implements Applicant<D> {

	protected boolean hasRequest = false;
	
	private final Mod mod;
	
	public SimpleApplicant(Mod mod) {
		this.mod = mod;
	}
	
	@Override
	public void onRequestDenied() {
		hasRequest = false;
	}

	@Override
	public void onRequestGranted(D data) {
		hasRequest = true;
	}

	@Override
	public void onRequestCancelled() {
		hasRequest = false;
	}

	public boolean hasRequest() {
		return hasRequest;
	}

	public void reset() {
		hasRequest = false;
	}

	@Override
	public Mod getMod() {
		return mod;
	}

}
