package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.api.mod.Mod;
import net.halalaboos.huzuni.mod.ModManager;


public class AllOff implements Command {
	
    @Override
    public String[] getAliases() {
        return new String[] {"alloff", "off"};
    }
    
    @Override
    public String[] getHelp() {
        return new String[] {"alloff"};
    }
    
    @Override
    public String getDescription() {
        return "Turns all enabled mods off.";
    }
    
    @Override
    public void run(String input, String[] args) {
        for (Mod mod : ModManager.getMods()) {
            if (mod.isEnabled())
            	mod.setEnabled(false);
        }
        Huzuni.addChatMessage("All mods turned off.");
    }

}