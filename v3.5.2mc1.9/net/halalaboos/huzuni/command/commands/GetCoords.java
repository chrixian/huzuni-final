package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;

import java.awt.*;
import java.awt.datatransfer.StringSelection;

/**
 * @author brudin
 * @version 1.0
 * @since 4/14/14
 */
public class GetCoords implements Command {
	@Override
	public String[] getAliases() {
		return new String[] {"getcoords", "gc", "cc", "copycoords"};
	}

	@Override
	public String[] getHelp() {
		return new String[] {"getcoords"};
	}

	@Override
	public String getDescription() {
		return "Copies your current coordinates to your clipboard.";
	}

	@Override
	public void run(String input, String[] args) {
		String coords = getFormattedCoordinates(Minecraft.getMinecraft().thePlayer);
		Huzuni.addChatMessage(coords + " copied to your clipboard.");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(coords), null);
	}

	private String getFormattedCoordinates(Entity entity) {
		return (int)entity.posX + ", " + (int)entity.posY + ", " + (int)entity.posZ;
	}
}
