package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.minecraft.client.Minecraft;

import java.awt.*;
import java.awt.datatransfer.StringSelection;

/**
 * @author brudin
 * @version 1.0
 * @since 4/14/14
 */
public class GetIP implements Command {
	@Override
	public String[] getAliases() {
		return new String[] { "getip" };
	}

	@Override
	public String[] getHelp() {
		return new String[] {"getip"};
	}

	@Override
	public String getDescription() {
		return "Copies the server IP to your clipboard.";
	}

	@Override
	public void run(String input, String[] args) {
		if(Minecraft.getMinecraft().isSingleplayer()) {
			Huzuni.addChatMessage("You're not connected to a server!");
		} else {
			String ip = Minecraft.getMinecraft().getCurrentServerData().serverIP;
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(ip), null);
			Huzuni.addChatMessage(ip + " copied to your clipboard.");
		}
	}
}
