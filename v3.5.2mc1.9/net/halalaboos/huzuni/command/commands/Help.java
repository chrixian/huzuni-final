package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.command.CommandManager;
import net.halalaboos.huzuni.util.StringUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.event.ClickEvent;
import net.minecraft.util.text.event.HoverEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * @author brudin
 * @version 1.0
 * @since 4/10/14
 */
public class Help implements Command {

	private final int COMMAND_PER_PAGE = 4;

	@Override
	public String[] getAliases() {
		return new String[] { "help" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "help <command|page>" };
	}

	@Override
	public String getDescription() {
		return "Gives you all of the help commands.";
	}

	@Override
	public void run(String originalString, String[] args) {
		if (args != null) {
			if (StringUtils.isInteger(args[0])) {
				listHelp(Integer.parseInt(args[0]));
			} else {
				Command command = findCommand(args[0]);
				if (command != null) {
					Huzuni.addChatMessage(TextFormatting.ITALIC + "Listing help for '" + TextFormatting.RESET + "\2477" + args[0] + "\247r" + TextFormatting.ITALIC + "'");
					String aliases = "";
					for(String s : command.getAliases()) { aliases += s + ", "; }
					Huzuni.addChatMessage("\2477" + TextFormatting.ITALIC + "Aliases: " + TextFormatting.RESET + aliases.substring(0, aliases.length() - 2));
					String usage = "";
					for(String s : command.getHelp()) { usage += s; }
					Huzuni.addChatMessage("\2477" + TextFormatting.ITALIC + "Usage: " + TextFormatting.RESET + usage);

					Huzuni.addChatMessage("\2477" + TextFormatting.ITALIC + "Description: " + TextFormatting.RESET + command.getDescription());
				} else
					Huzuni.addChatMessage("Command not found!");
			}
		} else {
			listHelp(1);
		}
	}

	/**
	 * @param string
	 * @return
	 */
	private Command findCommand(String string) {
		for (Command command : CommandManager.getCommands()) {
			for (String alias : command.getAliases()) {
				if (string.equalsIgnoreCase(alias)) {
					return command;
				}
			}
		}
		return null;
	}

	/**
	 * @return Lists all commands for the page.
	 * */
	public void listHelp(int wantedPage) {
		int pages = getPages();
		List<Command> commandsOnPage = getCommandsOnPage(wantedPage);
		if (commandsOnPage.isEmpty() || wantedPage <= 0) {
			Huzuni.addChatMessage("\247q'" + wantedPage + "' is an invalid page!");
			return;
		}
		Minecraft.getMinecraft().ingameGUI.getChatGUI().printChatMessage(getPageMessage(wantedPage, pages));
		for (Command command : commandsOnPage)
			Huzuni.addChatMessage(TextFormatting.GOLD + command.getAliases()[0] + TextFormatting.GRAY + " - " + command.getDescription());
	}

	/**
	 * @return All commands for the page.
	 * */
	private List<Command> getCommandsOnPage(int page) {
		List<Command> tempList = new ArrayList<Command>();
		int pageCount = 1, commandCount = 0;

		for (int i = 0; i < CommandManager.getCommands().size(); i++) {
			Command command = CommandManager.getCommands().get(i);
			if (command != this) {
				if (commandCount >= COMMAND_PER_PAGE) {
					pageCount++;
					commandCount = 0;
				}
				if (pageCount == page)
					tempList.add(command);
				commandCount++;
			}
		}
		return tempList;
	}

	/**
	 * @return Amount of pages of commands we have.
	 * */
	private int getPages() {
		boolean isDividedEvenly = false;
		return MathHelper.ceiling_float_int((float) (CommandManager.getCommands().size() - 1) / (float) COMMAND_PER_PAGE - (isDividedEvenly ? 1 : 0));
	}

	private TextComponentString getPageMessage(int wantedPage, int pages) {
		String text = TextFormatting.GOLD + "--- " + TextFormatting.GRAY + "Showing help page " + wantedPage + " of " + pages + " (" + CommandManager.COMMAND_PREFIX + "help <page>)"
				+ TextFormatting.GOLD + " ---";
		TextComponentString output = new TextComponentString(text);
		int nextPage = wantedPage == getPages() ? 1 : wantedPage + 1;
		output.getChatStyle().setChatHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponentString("Click to go to the next page!")));
		output.getChatStyle().setChatClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, CommandManager.COMMAND_PREFIX + "help " + nextPage));
		return output;
	}
}
