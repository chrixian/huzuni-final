package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.mod.ModManager;

/**
 * @since 6:53 PM on 4/1/2015
 */
public class RefreshColors implements Command {
	@Override
	public String[] getAliases() {
		return new String[] { "refreshcolors", "rc" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "<none>" };
	}

	@Override
	public String getDescription() {
		return "Changes the color of the mods you have enabled to a random one.";
	}

	@Override
	public void run(String input, String[] args) {
		for (DefaultMod mod : ModManager.getDefaultMods()) {
			if (mod.isEnabled()) {
				mod.refreshColor();
			}
		}
	}
}
