package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.util.StringUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.network.play.client.CPacketChatMessage;

public class Say implements Command {

	@Override
	public String[] getAliases() {
		return new String[] { "say" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "say <words>" };
	}

	@Override
	public String getDescription() {
		return "Says something in chat.";
	}

	@Override
	public void run(String input, String[] args) {
		String text = StringUtils.getAfter(input, 1);
		Minecraft.getMinecraft().getNetHandler().addToSendQueue(new CPacketChatMessage(text));
	}

}
