package net.halalaboos.huzuni.event.tick;

import net.halalaboos.huzuni.api.event.EventManager;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent.Type;

public class MotionUpdateEventManager extends EventManager <MotionUpdateEvent, UpdateListener> {

	@Override
	protected void invoke(UpdateListener listener, MotionUpdateEvent event) {
		if (event.getType() == Type.PRE)
			listener.onPreUpdate(event);
		else
			listener.onPostUpdate(event);
	}

}
