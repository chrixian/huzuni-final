package net.halalaboos.huzuni.gui.clickable;

import java.awt.Rectangle;
import java.util.List;

import net.halalaboos.huzuni.api.gui.Component;
import net.halalaboos.huzuni.api.gui.Container;
import net.halalaboos.huzuni.api.gui.layout.Layout;


public class DefaultLayout implements Layout {
    
	private final int COMPONENT_PADDING = 1, CONTAINER_PADDING = 3;

    @Override
    public Rectangle layout(Container container, List<Component> components) {
        float biggestWidth = container.getWidth();
        float y = CONTAINER_PADDING;
        for (Component component : components) {
            if (biggestWidth < component.getWidth())
                biggestWidth = component.getWidth();
        }
        
        for (Component component : components) {
        	component.layout(CONTAINER_PADDING, y, biggestWidth - CONTAINER_PADDING, component.getHeight());
            if ((int) (y + component.getHeight() + COMPONENT_PADDING) > y)
                y += component.getHeight() + COMPONENT_PADDING;
        }
        return new Rectangle(0, 0, (int) (biggestWidth + CONTAINER_PADDING), (int) y + CONTAINER_PADDING - COMPONENT_PADDING);
    }

}
