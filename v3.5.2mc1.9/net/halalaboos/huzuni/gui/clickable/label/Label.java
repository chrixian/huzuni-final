package net.halalaboos.huzuni.gui.clickable.label;

import java.awt.Color;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.clickable.DefaultComponent;

public class Label extends DefaultComponent {

	private String text;
	
	private Color color = Color.WHITE;
	
	public Label(String text) {
		super();
		this.text = text;
		this.setWidth(Huzuni.getStringWidth(text) + 2);
		this.setHeight(12);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
		this.setWidth(Huzuni.getStringWidth(text) + 2);
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
}
