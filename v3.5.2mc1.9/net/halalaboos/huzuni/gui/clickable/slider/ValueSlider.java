package net.halalaboos.huzuni.gui.clickable.slider;

import net.halalaboos.huzuni.api.mod.ValueOption;

public class ValueSlider extends Slider {

	private final ValueOption value;
	
	public ValueSlider(ValueOption value) {
		super(value.getName(), value.getMinValue(), value.getValue(), value.getMaxValue(), value.getIncrementValue());
		this.value = value;
	}
	
	@Override
	protected void invokeActionListeners() {
		super.invokeActionListeners();
		value.setValue(getValue());
	}

	public ValueOption getValueObj() {
		return value;
	}
}
