package net.halalaboos.huzuni.gui.clickable.slot;

import org.lwjgl.opengl.GL11;

import net.halalaboos.huzuni.api.gui.Component;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.MathUtils;

public class SlotTest extends SlotComponent<Component> {

	private Component mousedComponent;
	
	private boolean layering = true;
	
	public SlotTest(float width, float height) {
		super(width, height);
		
	}
	@Override
	public void update() {
		super.update();
		calculateMousedComponent();
		this.setTooltip(null);
	}
	
	@Override
	public void mouseReleased(int x, int y, int buttonId) {
		if (clickedComponent != null) {
			clickedComponent.mouseReleased(x, y, buttonId);
			clickedComponent = null;
			dragging = false;
		}
	}
	

	@Override
	protected void onReleased(int index, Component component, int mouseX, int mouseY) {
	}

	@Override
	protected void onClicked(int index, Component component, float[] area, int mouseX, int mouseY, int buttonId) {
	}
	
	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
		float[] sliderPoint = getSliderPoint();
		if (MathUtils.inside(x, y, sliderPoint)) {
			dragging = true;
		}
		int yPos = 2 - (int) (getScrollHeight());
		if (!components.isEmpty()) {
			for (int i = 0; i < components.size() && i >= 0; i++) {
				Component component = components.get(i);
				float[] componentArea = new float[] { (int) (this.getOffsetX() + this.getX() + 2), (int) (this.getOffsetY() + this.getY() + yPos), (int) (getWidth() - (hasEnoughToScroll() && hasSlider ? (sliderPoint[3]) : 0) - 4), getComponentHeight(component) };
				if (component.mouseClicked(x, y, buttonId)) {
					clickedComponent = component;
					return true;
				}
				yPos += componentArea[3] + componentPadding;
			}
		}
		return super.mouseClicked(x, y, buttonId);
	}
	
	@Override
	protected void render(Theme theme, int index, Component component, float[] area, boolean highlight, boolean mouseOver, boolean mouseDown) {
		component.setMouseOver(mouseOver);
		component.setMouseDown(this.clickedComponent == component);
		component.update();
		component.updateContainerOffset(area[0], area[1]);
		component.setWidth(area[2]);
		theme.renderComponent(area[0], area[1], component);
		if (mouseOver)
			this.setTooltip(component.getTooltip());
	}

	@Override
	public void renderComponents(Theme theme, float offsetX, float offsetY, boolean mouseOver, boolean mouseDown) {
		if (!components.isEmpty()) {
			int yPos = 2 - (int) (getScrollHeight());
			float[] sliderPoint = getSliderPoint();
			float[] mouse = new float[] { GLUtils.getMouseX(), GLUtils.getMouseY() };
			GL11.glEnable(GL11.GL_SCISSOR_TEST);
			GLUtils.glScissor(getX() + offsetX + 2, getY() + offsetY + 2, getX() + offsetX + getWidth(), getY() + offsetY + getHeight() - 2);
			for (int i = 0; i < components.size() && i >= 0; i++) {
				Component component = components.get(i);
				float[] componentArea = new float[] { getX() + offsetX + 2, getY() + offsetY + yPos, getWidth() - (hasEnoughToScroll() && hasSlider ? (sliderPoint[2]) : 0) - 4, getComponentHeight(component) };
				if (shouldRender(componentArea)) {
					render(theme, i, component, componentArea, selectedComponent == i, mouseOver && (clickedComponent == null ? mousedComponent == component : clickedComponent == component && mousedComponent == component), mouseDown && clickedComponent == component);
				}
				yPos += componentArea[3] + componentPadding;
			}
			GL11.glDisable(GL11.GL_SCISSOR_TEST);
		}
	}
	
	@Override
	protected int getComponentHeight(Component component) {
		return (int) component.getHeight();
	}

	public boolean isLayering() {
		return layering;
	}

	public void setLayering(boolean layering) {
		this.layering = layering;
	}
	private void calculateMousedComponent() {
		int mouseX = GLUtils.getMouseX(), mouseY = GLUtils.getMouseY();
		for (int i = components.size() - 1; i >= 0; i--) {
			Component component = components.get(i);
			if (component.isPointInside(mouseX, mouseY)) {
				this.mousedComponent = component;
				return;
			}
		}
		this.mousedComponent = null;
	}

}
