package net.halalaboos.huzuni.gui.clickable.sort;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.api.request.Applicant;
import net.halalaboos.huzuni.api.request.RequestQueue;

public class QueueSorter extends  SortComponent<Applicant> {

	private RequestQueue queue;
	
	public QueueSorter(RequestQueue queue, float width, float height) {
		super(width, height);
		this.setComponents(queue.getApplicants());
		this.queue = queue;
	}
	
	@Override
	public void update() {
		super.update();
		this.setTooltip(null);
	}

	@Override
	protected void renderComponent(Theme theme, int index, Applicant applicant, float[] area, boolean highlight, boolean mouseOver, boolean mouseDown) {
		theme.renderSlot(getOffsetX(), getOffsetY(), index, area, highlight, mouseOver, mouseDown);
		Huzuni.drawStringWithShadow(applicant.getMod().getName(), area[0] + 2, area[1] + 2, 0xFFFFFF);
		if (mouseOver)
			this.setTooltip(applicant.getMod().getDescription());
	}

}
