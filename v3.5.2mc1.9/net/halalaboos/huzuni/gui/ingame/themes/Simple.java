package net.halalaboos.huzuni.gui.ingame.themes;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.ingame.IngameTheme;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.mod.ModManager;
import net.minecraft.client.Minecraft;

import org.lwjgl.opengl.GL11;

import java.awt.*;

public class Simple implements IngameTheme {
    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
		int posY = 2;
		for (DefaultMod mod : ModManager.getDefaultMods()) {
			if (mod.isEnabled() && mod.getCategory() != null && mod.getVisible()) {
				GL11.glEnable(GL11.GL_BLEND);
				mc.fontRenderer.drawStringWithShadow(mod.getRenderName().toLowerCase(), 2, posY, 0xCFCCCCCC);
				posY += 10;
			}
		}
    }

    @Override
    public String getName() {
        return "Simple";
    }

    @Override
    public void onKeyTyped(int keyCode) {
    }
}
