package net.halalaboos.huzuni.gui.screen;

import net.halalaboos.huzuni.gui.clickable.menu.Menu;
import net.halalaboos.huzuni.gui.clickable.slot.SlotText;
import pw.brudin.huzuni.util.screen.BruButton;
import pw.brudin.huzuni.util.screen.PanoramaRenderer;
import net.halalaboos.huzuni.Huzuni;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

public class ChangelogScreen extends MenuScreen {

	private final GuiScreen lastScreen;
	private PanoramaRenderer panoramaRenderer;

	/**
	 * @param lastScreen
	 */
	public ChangelogScreen(GuiScreen lastScreen) {
		super("Changelog");
		this.lastScreen = lastScreen;
	}

	@Override
	protected void actionPerformed(GuiButton button) {
		switch (button.id) {
			case 0:
				mc.displayGuiScreen(lastScreen);
				break;
			default:
				break;
		}
	}

	@Override
	public void updateScreen() {
		super.updateScreen();
		panoramaRenderer.panoramaTick();
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		panoramaRenderer.renderSkybox(mouseX, mouseY, partialTicks);
		super.drawScreen(mouseX, mouseY, partialTicks);
		panoramaRenderer.renderFade();
	}

	@Override
	protected void setup(Menu menu) {
		menu.clear();
		SlotText textArea = new SlotText(width - 1, height - 50);

		if(Huzuni.getUpdateList() == null) {
			textArea.add("Unable to connect.");
		} else {
			for (String updateLine : Huzuni.getUpdateList()) {
				for (String formattedLine : Huzuni.fontRenderer.wrapWords(updateLine, width))
					textArea.add(formattedLine);
			}
		}
		menu.add(textArea);
		panoramaRenderer = new PanoramaRenderer(width, height);
		panoramaRenderer.init();
		buttonList.add(new BruButton(0, this.width / 2 - 100, this.height - 28, 200, 20, "Done"));
	}

}
