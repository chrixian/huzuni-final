package net.halalaboos.huzuni.gui.screen.alt;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import net.minecraft.client.gui.*;
import net.minecraft.util.text.TextFormatting;
import pw.brudin.huzuni.util.screen.PanoramaRenderer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;


import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.clickable.ContainerManager;
import net.halalaboos.huzuni.gui.clickable.menu.Menu;
import net.halalaboos.huzuni.gui.clickable.theme.HuzuniTheme;
import net.halalaboos.huzuni.util.FileUtils;
import net.halalaboos.huzuni.util.GLUtils;

public class AltManager extends GuiScreen implements GuiYesNoCallback {
   
	private final File altFile = new File(Huzuni.SAVE_DIRECTORY, "Accounts.txt");
    private final GuiScreen parentGui;
    private static String lastLoginStatus = "N/A";
    private PanoramaRenderer panoramaRenderer;

    private GuiTextField findField;
    private GuiButton use;
    private GuiButton remove;

    private final ContainerManager<Menu> container = new ContainerManager<Menu>(new HuzuniTheme());
    private final Menu menu = new Menu("Alts");
    private final SlotAlts slotAlts = new SlotAlts(this);
    
    private List<String> totalAccounts = new ArrayList<String>();
    
    public AltManager(GuiScreen par1GuiScreen) {
        parentGui = par1GuiScreen;
        menu.add(slotAlts);
    	container.add(menu);
    	totalAccounts = readAlts();
    	slotAlts.setComponents(readAlts());
    }

    @Override
    public void initGui() {
        int slotWidth = 300, slotHeight = (int) ((height - 110) * GLUtils.getScaleFactor()) / 2; // (GLUtils.getScreenHeight() - 114);
        panoramaRenderer = new PanoramaRenderer(width, height);
        panoramaRenderer.init();
    	menu.setX((GLUtils.getScreenWidth() / 2 - slotWidth / 2));
        menu.setY(30);
        menu.setWidth(slotWidth);
        menu.setHeight(slotHeight);
        slotAlts.setX(2);
        slotAlts.setY(2);
        slotAlts.setWidth(slotWidth - 4);
        slotAlts.setHeight(slotHeight - 16);
    	findField = new GuiTextField(1, mc.fontRenderer, width / 2 - 110, height - 76, 220, 20);
    	findField.setFocused(true);
    	
        buttonList.add(new GuiButton(15, this.width / 2 - 154, this.height - 28, 152, 20, "Load File.."));
        buttonList.add(new GuiButton(6, this.width / 2 + 2, this.height - 28, 152, 20, "Done"));
        buttonList.add(new GuiButton(12, this.width / 2 - 74, this.height - 52, 70, 20, "Add"));
        buttonList.add(new GuiButton(5, this.width / 2 + 4, this.height - 52, 70, 20, "Direct"));

        use = new GuiButton(14, this.width / 2 - 154, this.height - 52, 75, 20, "Login");
        buttonList.add(use);
        use.enabled = slotAlts.getSelectedItem() >= 0;
        remove = new GuiButton(13, width / 2 + 4 + 76, height - 52, 75, 20, "Remove");
        buttonList.add(remove);
        remove.enabled = slotAlts.getSelectedItem() >= 0;
    }

    /**
     * Fired when a control is clicked. This is the equivalent of
     * ActionListener.actionPerformed(ActionEvent e).
     */
    @Override
    protected void actionPerformed(GuiButton par1GuiButton) {
        if (!par1GuiButton.enabled) {
            return;
        }

        switch (par1GuiButton.id) {
            case 5:
                mc.displayGuiScreen(new HuzuniLogin(this, false));
                break;
            case 6:
                mc.displayGuiScreen(parentGui);
                break;
            case 12:
               mc.displayGuiScreen(new HuzuniLogin(this, true));
                break;
            case 13:
                mc.displayGuiScreen(new GuiYesNo(this, "Delete account '" + ((String) slotAlts.getComponents().get(slotAlts.getSelectedItem())).split(":")[0] + "'", "Are you sure?", "Delete", "Back", 0));
                break;
            case 14:
                if (slotAlts.getSelectedItem() >= 0 && slotAlts.getComponents().size() > 0)
                	slotAlts.login();
                break;
            case 15:
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        JFileChooser fileChooser = new JFileChooser();
                        if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                            File selectedFile = fileChooser.getSelectedFile();
                            BufferedReader reader = null;
                            try {
                                reader = new BufferedReader(new FileReader(selectedFile));
                                for (String line; (line = reader.readLine()) != null; ) {
                                    // If it's got a colon and it's not currently inside of our alt list, we're gonna add it.
                                    if (line.contains(":")) {
                                        if (!totalAccounts.contains(line))
                                        	totalAccounts.add(line);
                                    }
                                }
                                updateAccountsList();
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            } finally {
                                if (reader != null)
                                    try {
                                        reader.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                            }
                        }
                    }
                });
                break;
            default:
                //altList.actionPerformed(par1GuiButton);
                break;
        }
    }

    @Override
    public void confirmClicked(boolean confirm, int id) {
        if (confirm && id == 0) {
        	this.totalAccounts.remove(slotAlts.getSelectedComponentO());
        	slotAlts.removeAccount();
        	findField.setText("");
        	updateAccountsList();
           	FileUtils.writeFile(altFile, slotAlts.getComponents());
        }
        mc.displayGuiScreen(this);
    }

    @Override
    protected void keyTyped(char c, int keyCode) throws IOException {
        super.keyTyped(c, keyCode);
        this.container.keyTyped(keyCode, c);
        findField.textboxKeyTyped(c, keyCode);
        updateAccountsList();
    }

    private void updateAccountsList() {
        if (!findField.getText().isEmpty()) {
        	slotAlts.getComponents().clear();
        	for (String account : totalAccounts) {
        		if (account.toLowerCase().contains(findField.getText().toLowerCase()))
        			slotAlts.getComponents().add(account);
        	}
        } else
        	slotAlts.setComponents(new ArrayList(totalAccounts));
        slotAlts.setScrollPercentage(0F);
	}

    @Override
    protected void mouseClicked(int x, int y, int buttonId) throws IOException {
        super.mouseClicked(x, y, buttonId);
        container.mouseClicked(GLUtils.getMouseX(), GLUtils.getMouseY(), buttonId);
        findField.mouseClicked(x, y, buttonId);
    }

    @Override
    public void drawScreen(int par1, int par2, float par3) {
        panoramaRenderer.renderSkybox(par1, par2, par3);
        GLUtils.drawRect(0, 0, width, 16, 0x4F000000);
        Huzuni.drawString("ALTS: " + (slotAlts.getComponents().size()) + TextFormatting.GRAY +  " // " + TextFormatting.RESET + "Last login: " +
                mc.getSession().getUsername() + " (" + TextFormatting.GRAY + this.lastLoginStatus + TextFormatting.RESET + ")", 2, 3, 0xffffff);
        use.enabled = slotAlts.getSelectedItem() >= 0;
        remove.enabled = slotAlts.getSelectedItem() >= 0;
        super.drawScreen(par1, par2, par3);
        //        int slotWidth = 300, slotHeight = height / 2;
        findField.drawTextBox();
        GLUtils.setupOverlayDefaultScale();
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        container.render();
        glDisable(GL_BLEND);
        mc.entityRenderer.setupOverlayRendering();
        panoramaRenderer.renderFade();
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        panoramaRenderer.panoramaTick();
        findField.updateCursorCounter();
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        findField.setText("");
        updateAccountsList();
        FileUtils.writeFile(altFile, slotAlts.getComponents());
    }

    public List<String> readAlts() {
        if (altFile.exists()) {
            return FileUtils.readFile(altFile);
        }
        return new ArrayList<String>();
    }

    public static String getLastLoginStatus() {
        return lastLoginStatus;
    }

    public static void setLastLoginStatus(String lastLoginStatus) {
        AltManager.lastLoginStatus = lastLoginStatus;
    }

    public File getAltFile() {
        return altFile;
    }

	public SlotAlts getSlotAlts() {
		return slotAlts;
	}
	
	public void addAccount(String account) {
		if (!totalAccounts.contains(account)) {
			totalAccounts.add(account);
			this.updateAccountsList();
		}
	}
}
