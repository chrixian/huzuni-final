package net.halalaboos.huzuni.gui.screen.macro;

import net.halalaboos.huzuni.Huzuni;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;

import org.lwjgl.input.Keyboard;

import java.io.IOException;

public class EditMacro extends GuiScreen {

	private final MacroManager macroManager;
	
	private GuiTextField macroField, keyField;
	
	private final int originalKeyCode;
	
	private final String originalMacro;
	
	private boolean first = true;
	
	public EditMacro(MacroManager macroManager, int keyCode, String macro) {
		this.macroManager = macroManager;
		this.originalKeyCode = keyCode;
		this.originalMacro = macro;
	}
	
	public EditMacro(MacroManager macroManager) {
		this(macroManager, Keyboard.KEY_NONE, "");
	}
	

	@Override
    public void initGui() {
    	Keyboard.enableRepeatEvents(true);
    	int leftAlign = this.width / 2 - 152,
                top = this.height / 6 + 114,
                bottom = this.height / 6 + 140;
		macroField = new GuiTextField(0, mc.fontRenderer, leftAlign + 30, top, 240, 20);
		macroField.setFocused(true);
		keyField = new GuiTextField(0, mc.fontRenderer, leftAlign + 30, bottom, 240, 20);
		if (first) {
			macroField.setText(originalMacro);
			keyField.setText("" + Keyboard.getKeyName(originalKeyCode));
			first = false;
		}
        buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height / 6 + 168, "Done"));
    }
	
	@Override
	protected void actionPerformed(GuiButton button) {
		if (button.id == 0) {
			if (!keyField.getText().equals("NONE") && !keyField.getText().isEmpty() && !macroField.getText().isEmpty()) {
				Huzuni.MACROS[Keyboard.getKeyIndex(keyField.getText())] = macroField.getText();
				macroManager.updateMacrosList();
			}
			mc.displayGuiScreen(macroManager);
		}
	}
	
	@Override
	public void onGuiClosed() {
    	Keyboard.enableRepeatEvents(false);
	}
	

    @Override
    public void drawScreen(int mouseX, int mouseY, float par3) {
        super.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, par3);
        macroField.drawTextBox();
        keyField.drawTextBox();
        int leftAlign = this.width / 2 - 152,
                top = this.height / 6 + 114,
                bottom = this.height / 6 + 140;
		mc.fontRenderer.drawString("Macros are in a command format.", leftAlign - 2, 25, 0xFFFFFF);
		mc.fontRenderer.drawString("So if you want the macro to be a chat message, it would have to be:", leftAlign - 2, 35, 0xFFFFFF);
		mc.fontRenderer.drawString("say \247omessage", leftAlign - 2, 45, 0xFFFFFF);
        mc.fontRenderer.drawString("Macro", leftAlign - 2, top + 8, 0xFFFFFF);
        mc.fontRenderer.drawString("Key", leftAlign, bottom + 8, 0xFFFFFF);
    }
    
    @Override
    protected void keyTyped(char c, int keyCode) throws IOException {
        super.keyTyped(c, keyCode);
        macroField.textboxKeyTyped(c, keyCode);
        if (keyField.isFocused()) {
        	keyField.setText(Keyboard.getKeyName(keyCode));
        }
    }

    @Override
    protected void mouseClicked(int x, int y, int buttonId) throws IOException {
        super.mouseClicked(x, y, buttonId);
        macroField.mouseClicked(x, y, buttonId);
        keyField.mouseClicked(x, y, buttonId);
    }
    
    @Override
    public void updateScreen() {
    	super.updateScreen();
    	macroField.updateCursorCounter();
    	keyField.updateCursorCounter();
    }

}
