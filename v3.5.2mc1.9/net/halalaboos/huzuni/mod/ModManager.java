package net.halalaboos.huzuni.mod;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.TransformerException;

import net.halalaboos.huzuni.mod.mods.MiddleClickFriends;
import net.halalaboos.huzuni.mod.mods.render.StatusHUD;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Mod;
import net.halalaboos.huzuni.mod.mods.unfinished.InventoryHelper;
import net.halalaboos.huzuni.mod.mods.chat.*;
import net.halalaboos.huzuni.mod.mods.combat.*;
import net.halalaboos.huzuni.mod.mods.movement.*;
import net.halalaboos.huzuni.mod.mods.player.*;
import net.halalaboos.huzuni.mod.mods.render.*;
import net.halalaboos.huzuni.mod.mods.unfinished.Autobuild;
import net.halalaboos.huzuni.mod.mods.unfinished.BowAimbot;
import net.halalaboos.huzuni.mod.mods.unfinished.JumpPrediction;
import net.halalaboos.huzuni.mod.mods.world.*;
import net.halalaboos.huzuni.util.io.XMLFileHandler;

public final class ModManager {

	private static final List<Mod> mods = new ArrayList<Mod>();
	
	private static final List<DefaultMod> defaultMods = new ArrayList<DefaultMod>();

	public static final File MOD_FILE = new File(Huzuni.SAVE_DIRECTORY, "Mods.txt");

	private static final XMLFileHandler fileHandler = new XMLFileHandler(MOD_FILE);
	
	private ModManager() {
		
	}
	
	public static void setupMods() {
		registerMod(new Autotool());
		registerMod(Antiknockback.instance);
		registerMod(new Anticensor());
		registerMod(new Dolanspeak());
		registerMod(new Nofall());
		registerMod(Sprint.instance);
		registerMod(new Respawn());
		registerMod(Flight.instance);
		registerMod(Xray.instance);
		registerMod(new Bright());
		registerMod(new Killaura());
		registerMod(new Speedmine());
		registerMod(new Autofish());
		registerMod(new Dolphin());
		registerMod(new Glide());
		registerMod(new Retard());
		registerMod(new Sneak());
		registerMod(new Autodisconnect());
		registerMod(new StorageESP());
		registerMod(Tracer.instance);
		registerMod(new AutoTPA());
		registerMod(new Spider());
		registerMod(Freecam.instance);
		registerMod(new Projectiles());
		registerMod(new Breadcrumb());
		registerMod(Nametags.instance);
		registerMod(new Fastladder());
		registerMod(new Step());
		registerMod(Autosign.instance);
		registerMod(new Nuker());
		registerMod(new Autofarm());
		registerMod(new Fastplace());
		registerMod(Wireframe.instance);
		registerMod(new Cheststealer());
		registerMod(new Vehicleonehit());
		registerMod(new Autoarmor());
		registerMod(new Autosoup());
		registerMod(new Clickaimbot());
		registerMod(new Autobuild());
		registerMod(new Criticals());
		registerMod(MiddleClickFriends.instance);
		registerMod(Noslowdown.instance);
		registerMod(StatusHUD.instance);
		registerMod(InventoryHelper.instance);
		registerMod(new JumpPrediction());
		registerMod(new ChestXploder());
		registerMod(new Timer());
		registerMod(new BowAimbot());
		registerMod(new Autopotion());
		//registerMod(new Hackusation());
	}
	
	public static void loadMods() {
			try {
			// Read the document.
			Document doc = fileHandler.read();
			// Get a nodelist of all the Mod elements.
			NodeList elementList = doc.getElementsByTagName("Mod");
			for (int i = 0; i < elementList.getLength(); i++) {
				Node node = elementList.item(i);
				// If it's an element.
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					try {
						String name = element.getAttribute("Name");
						Mod mod = getMod(name);                    		
						if (mod != null) {
							boolean ModState = Boolean.parseBoolean(element.getAttribute("Enabled").toUpperCase());
							for (int j = 0; j < mod.length(); j++)
								mod.get(j).load(element);
							boolean vipMod = mod instanceof VipMod;
							boolean checkModState = vipMod ? Huzuni.isVip() : true;
							if (checkModState) {
								if (mod.isEnabled() != ModState) {
									mod.setEnabled(ModState);
								}
							}
						}
	
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void saveMods() {
		// Setup the xml document.
        fileHandler.createDocument();
        Element modsRoot = fileHandler.createElement("Mods");
        fileHandler.addElement(modsRoot);
        for (Mod mod : mods) {
			Element modElement = fileHandler.createElement("Mod");
			modsRoot.appendChild(modElement);
			modElement.setAttribute("Name", mod.getName());
			modElement.setAttribute("Enabled", Boolean.toString(mod.isEnabled()));
			for (int i = 0; i < mod.length(); i++) {
				mod.get(i).save(modElement);
			}
        }
        try {
            fileHandler.write();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
	}
	
	public static void registerMod(Mod mod) {
		mods.add(mod);
		if (mod instanceof DefaultMod) {
			defaultMods.add((DefaultMod) mod);
			Huzuni.addKeybind((DefaultMod) mod);
		}
	}
	
	public static void unregisterMod(Mod mod) {
		mods.remove(mod);
		if (mod instanceof DefaultMod) {
			defaultMods.remove((DefaultMod) mod);
		}
	}
	
	public static List<Mod> getMods() {
		return mods;
	}
	
	public static List<DefaultMod> getDefaultMods() {
		return defaultMods;
	}

	public static Mod getMod(String name) {
		for (int i = 0; i < mods.size(); i++) {
			Mod mod = mods.get(i);
			if (mod.getName().replaceAll(" ", "").equalsIgnoreCase(name.replaceAll(" ", "")))
				return mod;
		}
		return null;
	}
		
}
