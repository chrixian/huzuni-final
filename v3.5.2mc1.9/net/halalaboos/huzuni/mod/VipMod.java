package net.halalaboos.huzuni.mod;

import net.halalaboos.huzuni.Huzuni;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.event.HoverEvent;
import pw.brudin.huzuni.util.ChatComponentHelper;


public abstract class VipMod extends DefaultMod {

	public VipMod(String name, int keyCode) {
		super(name, keyCode);
		
	}
	
	public VipMod(String name) {
		super(name);
		
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		if (Huzuni.isVip()) {
			super.setEnabled(enabled);
		} else {
			Huzuni.addChatMessage(getErrorMessage());
		}
	}

	private TextComponentString getErrorMessage() {
		TextComponentString output = new TextComponentString(TextFormatting.DARK_GREEN + "[H] " + TextFormatting.RESET + "You must be VIP to use this!");
		TextComponentString alreadyPurchased = new TextComponentString(TextFormatting.GREEN + " \247o(Already have VIP?)");
		output.getChatStyle().setChatHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponentString("\247oClick here to get VIP!")));
		TextComponentString hover = ChatComponentHelper.multiline("Our site may be down", "if your VIP isn't working.");
		alreadyPurchased.getChatStyle().setChatHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, hover));
		output.appendSibling(alreadyPurchased);
		return output;
	}
}
