package net.halalaboos.huzuni.mod.keybinds;


import org.lwjgl.input.Keyboard;

import net.halalaboos.huzuni.api.mod.Keybind;
import net.halalaboos.huzuni.gui.screen.WindowEditorMenu;
import net.halalaboos.huzuni.gui.screen.WindowScreen;
import net.minecraft.client.Minecraft;

public class WindowOpener implements Keybind {

	private int keyCode = -1;
	
	public WindowOpener() {
		this.keyCode = Keyboard.KEY_RSHIFT;
	}
	
	@Override
	public void pressed() {
		//Minecraft.getMinecraft().displayGuiScreen(new WindowEditorMenu());
		Minecraft.getMinecraft().displayGuiScreen(new WindowScreen());
	}

	@Override
	public void setKeybind(int keyCode) {
		this.keyCode = keyCode;
	}

	@Override
	public int getKeycode() {
		return keyCode;
	}

	@Override
	public boolean isBound() {
		return keyCode != -1 && keyCode != 0;
	}

	@Override
	public String getKeyName() {
		return isBound() ? Keyboard.getKeyName(keyCode) : "None";
	}

	@Override
	public String getName() {
		return "GUI";
	}

	@Override
	public String getDescription() {
		return "Opens the window screen.";
	}

	@Override
	public boolean isPressed() {
		return isBound() ? Keyboard.isKeyDown(keyCode) : false;
	}

}
