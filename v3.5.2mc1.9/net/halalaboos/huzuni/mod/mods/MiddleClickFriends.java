package net.halalaboos.huzuni.mod.mods;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.RayTraceResult;

/**
 * @author brudin
 * @version 1.0
 * @since 12:48 PM on 8/20/2014
 */
public class MiddleClickFriends extends DefaultMod {

	public static MiddleClickFriends instance = new MiddleClickFriends();

	public MiddleClickFriends() {
		super("Middle Click Friends");
		setEnabled(true);
		setVisible(false);
		setCategory(Category.WORLD);
		setDescription("Adds players as friends when you middle click them.");
	}

	@Override
	protected void onEnable() {

	}

	@Override
	protected void onDisable() {

	}

	public void onClick(int buttonID) {
		if (buttonID == 2) {
			if (mc.objectMouseOver != null) {
				if (mc.objectMouseOver.typeOfHit == RayTraceResult.Type.ENTITY && mc.objectMouseOver.entityHit instanceof EntityPlayer) {
					if (Huzuni.isFriend(mc.objectMouseOver.entityHit.getName())) {
						Huzuni.addChatMessage(String.format("Removed %s as a friend.", mc.objectMouseOver.entityHit.getName()));
						Huzuni.removeFriend(mc.objectMouseOver.entityHit.getName());
					} else {
						Huzuni.addFriend(mc.objectMouseOver.entityHit.getName());
						Huzuni.addChatMessage(String.format("Added %s as a friend.", mc.objectMouseOver.entityHit.getName()));
					}
				}
			}
		}
	}
}
