package net.halalaboos.huzuni.mod.mods;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.command.CommandManager;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.mod.mods.movement.Flight;
import net.halalaboos.huzuni.mod.mods.player.Freecam;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.CPacketPlayerAbilities;
import net.minecraft.network.play.client.CPacketTabComplete;
import net.minecraft.network.play.server.SPacketPlayerAbilities;


/**
 * @since 5:05 PM on 3/21/2015
 */
public class Patcher implements PacketListener {

	private boolean shouldHideFlying = true;

	/**
	 * PATCHER isn't really a mod, but moreso a way to prevent the client from sending things that
	 * would make it clear if the user is hacking. Because of this, it can only be disabled when huzuni is.
	 *
	 * TODO:
	 * 	prevent client from sending server fly state (ez)
	 */

	public Patcher() {}

	public void init() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	public void onPacket(PacketEvent event, PacketEvent.PacketType type) {
		if(!Huzuni.isEnabled())
			return;
		if (type == PacketEvent.PacketType.READ) {
			if (event.getPacket() instanceof SPacketPlayerAbilities) {
				SPacketPlayerAbilities packet = (SPacketPlayerAbilities)event.getPacket();
				shouldHideFlying = !(packet.isAllowFlying() || packet.isFlying());
			}
		}
		if (type == PacketEvent.PacketType.SENT) {
			if (event.getPacket() instanceof CPacketTabComplete) {
				event.setPacket(hideCommands((CPacketTabComplete) event.getPacket()));
			}
			
			if (event.getPacket() instanceof CPacketPlayerAbilities) {
				event.setPacket(removeFlying((CPacketPlayerAbilities) event.getPacket()));
			}
		}
	}

	private Packet removeFlying(CPacketPlayerAbilities packet) {
		if ((Flight.instance.isEnabled() || Freecam.instance.isEnabled()) && shouldHideFlying) {
			packet.setFlying(false);
		}
		return packet;
	}

	/**
	 * When you tab complete a message, it will send the server the entire message.  This means that servers will
	 * be able to tell if someone does .add [playername] [alias] or whatever because the client is literally telling
	 * the server that when autocompleting the player names.
	 * What this does is only sends the last part of the message if it starts with a '.'.  So in the case of:
	 *		.add b[TABCOMPLETE]
	 * Instead of sending that entire message, it will only be sending:
	 * 		b
	 * Fun!
	 * @param packet	The tab complete packet to modify
	 * @return			A new tab complete packet!
	 */
	private CPacketTabComplete hideCommands(CPacketTabComplete packet) {
		String packetOutput = packet.getMessage();
		if (!packetOutput.startsWith(CommandManager.COMMAND_PREFIX))
			return packet;
		String[] packetOutputArray = packetOutput.split(" ");
		String toSend = packetOutputArray[packetOutputArray.length - 1];
		if (toSend.startsWith(CommandManager.COMMAND_PREFIX)) {
			toSend = toSend.substring(1, toSend.length());
		}
		return new CPacketTabComplete(toSend, null, false);
	}
}
