package net.halalaboos.huzuni.mod.mods.chat;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.minecraft.client.Minecraft;
import net.minecraft.network.play.server.SPacketChat;

/**
 * @author brudin
 * @version 1.0
 * @since 4/14/14
 */
public class AutoTPA extends DefaultMod implements PacketListener {

	public AutoTPA() {
		super("Auto Accept");
		setVisible(false);
		setAuthor("brudin");
		setDescription("Automatically accepts teleport requests from friends.");
		setCategory(Category.CHAT);
	}

	@Override
	public void onPacket(PacketEvent event, PacketEvent.PacketType type) {
		if (type == PacketEvent.PacketType.READ) {
			if (event.getPacket() instanceof SPacketChat) {
				SPacketChat packet = (SPacketChat)event.getPacket();
				if (packet.getChatComponent().getFormattedText().contains("has requested to teleport to you")) {
					for (String s : Huzuni.FRIENDS) {
						if (packet.getChatComponent().getFormattedText().toLowerCase().contains(s.toLowerCase())) {
							Minecraft.getMinecraft().thePlayer.sendChatMessage("/etpyes " + s);
							break;
						}
					}
				}
			}
		}
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
	}
}
