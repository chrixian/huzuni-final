package net.halalaboos.huzuni.mod.mods.combat;

import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.ValueOption;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.mod.DefaultMod;

/**
 * TODO: this
 * @since 9:03 PM on 3/13/2016
 */
public class NewAutosoup extends DefaultMod implements UpdateListener {

	private static final int BOWL_SOUP = 282, BOWL_SOUP_EMPTY = 281;
	private int oldItem = -1, soupItem = -1;
	private boolean eating = false;
	private final ValueOption health = new ValueOption("Health", 1F, 14F, 20F, 1F, "Health level you want to begin eating soups");

	public NewAutosoup() {
		super("Auto Soup");
		setAuthor("Halalaboos");
		setDescription("Instantly eats soup for you.");
		setCategory(Category.COMBAT);
		this.setOptions(health);
	}

	@Override
	protected void onEnable() {

	}

	@Override
	protected void onDisable() {

	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {

	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {

	}


}
