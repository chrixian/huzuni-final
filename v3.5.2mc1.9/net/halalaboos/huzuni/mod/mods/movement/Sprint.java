package net.halalaboos.huzuni.mod.mods.movement;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.ValueOption;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.mod.DefaultMod;
import org.lwjgl.input.Keyboard;

/**
 * @author brudin
 * @version 1.0
 * @since 3/25/14
 */
public final class Sprint extends DefaultMod implements UpdateListener {

	public static final Sprint instance = new Sprint();
	
	public final ValueOption speed = new ValueOption("Run speed", 1F, 1F, 5F, 0.2F, "Speed you will run at");

	private Sprint() {
		super("Sprint", Keyboard.KEY_M);
		setCategory(Category.MOVEMENT);
		setAuthor("brudin");
		setDescription("Automatically sprints for you.");
		this.setOptions(speed);
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
		if (mc.thePlayer != null)
			mc.thePlayer.setSprinting(false);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (!Noslowdown.instance.isEnabled() && mc.thePlayer.isHandActive()) {
			return;
		}
		mc.thePlayer.setSprinting(mc.thePlayer.moveForward > 0 && !mc.thePlayer.isSneaking() && !mc.thePlayer.isCollidedHorizontally && mc.thePlayer.getFoodStats().getFoodLevel() > 6);
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {

	}

	public float getSpeed() {
		return this.speed.getValue();
	}
}
