package net.halalaboos.huzuni.mod.mods.player;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.minecraft.network.play.client.CPacketClientStatus;
import net.minecraft.network.play.server.SPacketUpdateHealth;

/**
 * @author brudin
 * @version 1.0
 * @since 3/25/14
 */
public class Respawn extends DefaultMod implements PacketListener {

	public Respawn() {
		super("Respawn");
		setCategory(Category.PLAYER);
		setAuthor("brudin");
		setDescription("Automatically respawns you when you die.");
		setVisible(false);
	}

	@Override
	public void onPacket(PacketEvent event, PacketEvent.PacketType type) {
		if (type == PacketEvent.PacketType.READ) {
			if (event.getPacket() instanceof SPacketUpdateHealth) {
				SPacketUpdateHealth packet = (SPacketUpdateHealth)event.getPacket();
				if (packet.getHealth() > 0.0F)
					return;
				event.setCancelled(true);
				mc.thePlayer.sendQueue.addToSendQueue(new CPacketClientStatus(CPacketClientStatus.State.PERFORM_RESPAWN));
			}
		}
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
	}
}
