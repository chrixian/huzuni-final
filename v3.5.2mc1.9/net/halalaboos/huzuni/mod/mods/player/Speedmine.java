package net.halalaboos.huzuni.mod.mods.player;

import net.minecraft.network.play.client.CPacketPlayerDigging;
import net.minecraft.util.EnumFacing;

import net.minecraft.util.math.BlockPos;
import org.lwjgl.input.Keyboard;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.ValueOption;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketEvent.PacketType;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.util.GameUtils;
import net.minecraft.block.Block;

public class Speedmine extends DefaultMod implements PacketListener, UpdateListener {

	private boolean digging = false;
	
	private float curBlockDamage = 0;

	private EnumFacing facing;

	private BlockPos position;

	private Block block;

	private final ValueOption speed = new ValueOption("Mine speed", 1F, 1F, 2F, "Mine speed modifier");

	public Speedmine() {
		super("Speedmine", Keyboard.KEY_V);
		setCategory(Category.PLAYER);
		setAliases("sm", "mine");
		setAuthor("Halalaboos");
		setDescription("Mines blocks faster.");
		this.setOptions(speed);
	}

	@Override
	public void onPacket(PacketEvent event, PacketType type) {
		if (type == PacketType.SENT && !mc.playerController.isInCreativeMode()) {
			if (event.getPacket() instanceof CPacketPlayerDigging) {
				CPacketPlayerDigging packet = (CPacketPlayerDigging) event.getPacket();
				if (packet.getAction() == CPacketPlayerDigging.Action.START_DESTROY_BLOCK) {
					digging = true;
					this.position = packet.getPosition();
					this.facing = packet.getFacing();
					this.block = GameUtils.getBlock(this.position);
					this.curBlockDamage = 0;
				} else if (packet.getAction() == CPacketPlayerDigging.Action.STOP_DESTROY_BLOCK || packet.getAction() == CPacketPlayerDigging.Action.ABORT_DESTROY_BLOCK) {
					digging = false;
					this.position = null;
					this.facing = null;
					block = null;
				}
			}
		}
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
		Huzuni.unregisterUpdateListener(this);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (digging) {
			curBlockDamage += block.getPlayerRelativeBlockHardness(block.getDefaultState(), this.mc.thePlayer, this.mc.theWorld, this.position) * (speed.getValue());
			if (curBlockDamage >= 1.0F) {
				mc.playerController.func_187103_a(this.position);
				mc.getNetHandler().addToSendQueue(new CPacketPlayerDigging(CPacketPlayerDigging.Action.STOP_DESTROY_BLOCK, this.position, this.facing));
				curBlockDamage = 0F;
				digging = false;
			}
		}
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
	}

}
