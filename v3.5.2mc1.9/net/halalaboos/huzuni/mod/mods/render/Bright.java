package net.halalaboos.huzuni.mod.mods.render;

import net.halalaboos.huzuni.api.mod.ModeOption;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;

import org.lwjgl.input.Keyboard;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.util.Timer;

/**
 * @author brudin
 * @version 1.0
 * @since 3/26/14
 */
public class Bright extends DefaultMod implements UpdateListener {

	private float oldGamma = 0F;

	private final int GAMMA = 0, NIGHTVISION = 1;
	private final float MAX_BRIGHTNESS = 10F, INCREMENT = 0.25F;

	private final Timer timer = new Timer();

//	private final ModeOption mode;

	public Bright() {
		super("Bright", Keyboard.KEY_C);
		setCategory(Category.RENDER);
		setAliases("fullbright");
		setDescription("Brightens up the world.");
		setAuthor("Halalaboos");
//		setOptions(mode = new ModeOption("Bright mode", null, "Gamma", "NightVision"));
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				if (isEnabled()) {
					mc.gameSettings.gammaSetting = oldGamma;
					mc.gameSettings.saveOptions();
				}
			}
		});
	}

	@Override
	protected void onEnable() {
		oldGamma = mc.gameSettings.gammaSetting;
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (timer.hasReach(25)) {
			if (this.isEnabled()) {
				mc.gameSettings.gammaSetting += INCREMENT;
				if (mc.gameSettings.gammaSetting >= MAX_BRIGHTNESS) {
					mc.gameSettings.gammaSetting = MAX_BRIGHTNESS;
				}
			} else {
				mc.gameSettings.gammaSetting -= INCREMENT;
				if (mc.gameSettings.gammaSetting <= oldGamma) {
					mc.gameSettings.gammaSetting = oldGamma;
					Huzuni.unregisterUpdateListener(this);
				}
			}
			timer.reset();
		}
//		else if (mode.getSelected() == NIGHTVISION) {
//			int duration = 1000000;
//			PotionEffect nightVision = new PotionEffect(Potion.nightVision.id, duration, 1);
//			nightVision.setPotionDurationMax(true);
//			mc.thePlayer.addPotionEffect(nightVision);
//		}
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
	}
}
