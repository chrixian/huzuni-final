package net.halalaboos.huzuni.mod.mods.render;

import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

import net.minecraft.util.EnumHand;
import org.lwjgl.input.Keyboard;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.NameProtect;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.ToggleOption;
import net.halalaboos.huzuni.api.mod.ValueOption;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.GameUtils;

import java.util.ArrayDeque;
import java.util.Queue;

import static org.lwjgl.opengl.GL11.*;

public class Nametags extends DefaultMod {

	public static final Nametags instance = new Nametags();

	public final ValueOption scale = new ValueOption("Nametag scale", 0F, 1F, 5F, "Nametag scale size");
	public final ValueOption opacity = new ValueOption("Opacity", 0F, 25F, 100F, 1F, "Nametag background opacity");

	private final ToggleOption armor = new ToggleOption("Show armor", "Render player's armor"),
			doScale = new ToggleOption("Scale", "Scale nametags"), health = new ToggleOption("Show health", "Render player's health"),
					enchants = new ToggleOption("Show enchants", "Render enchantments on the player's armor in small text"), visibility,
							ping = new ToggleOption("Show ping", "Render player's ping");

	/**
	 * http://stackoverflow.com/questions/6129805/what-is-the-fastest-java-collection-with-the-basic-functionality-of-a-queue
	 */
	private Queue<Tag> tags = new ArrayDeque<Tag>();

	private Nametags() {
		super("Nametags", Keyboard.KEY_P);
		setCategory(Category.RENDER);
		setAliases("tags", "names", "nt");
		setAuthor("brudin");
		setDescription("Scales nametags when they're far.");
		setVisible(false);
		visibility = new ToggleOption("Ignore Culling", "Ignores world culling, might slightly impact performance") {
			@Override
			public void setEnabled(boolean enabled) {
				if (mc.theWorld != null) {
					mc.renderGlobal.loadRenderers();
				}
				super.setEnabled(enabled);
			}
		};
		this.setOptions(scale, opacity, armor, health, ping, doScale, enchants, visibility);
		doScale.setEnabled(true);
		visibility.setEnabled(true);
	}

	@Override
	protected void onEnable() {
	}

	@Override
	protected void onDisable() {
		tags.clear();
	}
	
	public boolean renderNameplate(Entity entity, String name, double x, double y, double z) {
		if (entity instanceof EntityLivingBase) {
			if (name.length() > 0) {
				this.tags.add(new Tag(name, x, y, z, (EntityLivingBase) entity));
			}
		}
		return true;
	}

	private String getColorForHealth(int curHealth, int maxHealth) {
		return curHealth >= maxHealth / 2 ? "\247e" : curHealth >= maxHealth / 3 ? "\2476" : "\247c";
	}

	private String getFormattedHealth(double health) {
		String out = "" + Math.ceil(health) / 2;
		if (out.endsWith(".0")) {
			out = out.substring(0, out.length() - 2);
		}
		return out;
	}
	
	public void render(float delta) {
		glPushMatrix();
		GLUtils.enableGL3D();
		while (!tags.isEmpty()) {
			glPushMatrix();
			tags.poll().render(delta); //TODO: Interpolate scaling so it looks less choppy
			glPopMatrix();
		}
		GLUtils.disableGL3D();
		glPopMatrix();
	}

	/**
	 * TODO: Stop using 3d and fix 2d rendering for blocks, or make it not rotate
	 */
	private void renderItems(EntityPlayer player, float rY) {
		int totalItems = 0;
		glPushMatrix();
		for (int i = 0; i < 4; i++)
			if (player.inventory.armorItemInSlot(i) != null)
				totalItems++;
		if (player.getHeldItem(EnumHand.MAIN_HAND) != null)
			totalItems++;
		int itemSize = 18, center = (-itemSize / 2), halfTotalSize = ((totalItems * itemSize) / 2 - itemSize) + (itemSize / 2), count = 0;
		if (player.getHeldItem(EnumHand.MAIN_HAND) != null) {
			GLUtils.draw3dItem(player.getHeldItem(EnumHand.MAIN_HAND), (center - halfTotalSize) + itemSize * count + 2, (int) rY - 16, 0);
			if (enchants.isEnabled())
				renderEnchantments(player.getHeldItem(EnumHand.MAIN_HAND), (center - halfTotalSize) + itemSize * count + 2, (int) rY - 16, 0.25F);
			count++;
		}
		for (int i = 4; i > 0; i--) {
			ItemStack armor = player.inventory.armorItemInSlot(i - 1);
			if (armor != null) {
				GLUtils.draw3dItem(armor, (center - halfTotalSize) + itemSize * count, (int) rY - 16, 0);
				if (enchants.isEnabled())
					renderEnchantments(armor, (center - halfTotalSize) + itemSize * count, (int) rY - 16, 0.25F);
				count++;
			}
		}
		glPopMatrix();
	}
	
	private void renderEnchantments(ItemStack item, float x, float y, float scale) {
		float scaleInverse = 1F / scale, increment = 10F / scaleInverse;
		if (item.getEnchantmentTagList() != null) {
			NBTTagList enchantments = item.getEnchantmentTagList();
			for (int j = 0; j < enchantments.tagCount(); j++) {
				NBTTagCompound compound = enchantments.getCompoundTagAt(j);
				glPushMatrix();
				glScalef(scale, scale, scale);
				if (compound != null && Enchantment.getEnchantmentByID(compound.getByte("id")) != null)
					mc.fontRenderer.drawStringWithShadow(Enchantment.getEnchantmentByID(compound.getByte("id")).getTranslatedName(compound.getByte("lvl")).substring(0, 4) + " " + compound.getByte("lvl"), x * scaleInverse, ((int) y + (increment * j)) * scaleInverse, 0xFFFFFF);
				glPopMatrix();
			}
		}
	}

	private void drawRect(float rX, float rY, float rX1, float rY1, Tessellator tessellator) {
		VertexBuffer worldRenderer = tessellator.getBuffer();
		worldRenderer.begin(7, DefaultVertexFormats.POSITION);
		GlStateManager.color(0, 0, 0, opacity.getValue() / 100F);
		worldRenderer.pos(rX, rY, 0.0D).endVertex();
		worldRenderer.pos(rX, rY1, 0.0D).endVertex();
		worldRenderer.pos(rX1, rY1, 0.0D).endVertex();
		worldRenderer.pos(rX1, rY, 0.0D).endVertex();
		tessellator.draw();
	}

	public boolean getVisibilityEnabled() {
		return visibility.isEnabled();
	}

	public class Tag {
		
		private String name;
		
		private int responseTime;
		
		private double x, y, z;
		
		private EntityLivingBase target;

		public Tag(String name, double x, double y, double z, EntityLivingBase target) {
			this.name = name;
			this.x = x;
			this.y = y;
			this.z = z;
			this.target = target;
			setup();
		}

		private void setup() {
			boolean hasPlayer = target instanceof EntityPlayer;
			if (hasPlayer) {
				EntityPlayer player = (EntityPlayer) target;
				NetworkPlayerInfo playerInfo = GameUtils.getPlayerInfo(player);
				if (name.toLowerCase().contains(player.getName().toLowerCase()))
					name = NameProtect.replace(name);
				if (health.isEnabled()) {
					name += " " + getColorForHealth((int) player.getHealth(), (int) player.getMaxHealth()) + getFormattedHealth(player.getHealth());
				}
				if (ping.isEnabled()) {
					responseTime = playerInfo == null ? -1 : playerInfo.getResponseTime();
				} else
					responseTime = -1;
			}
		}

		public void render(float delta) {
			GlStateManager.enableBlend();
			byte offset = 2;
			Tessellator tesselator = Tessellator.getInstance();
			float distance = mc.thePlayer.getDistanceToEntity(target),
					nametagScale = distance / (2F + (5F - (Nametags.instance.scale.getValue()))),
					scale = 0.016666668F * 1.6F, yOffset = responseTime != -1 ? -10 - offset: 0;
			int width = mc.fontRenderer.getStringWidth(name) / 2;
			float rX = -width - offset, rY = yOffset - offset, rX1 = width + offset, rY1 = yOffset + 8 + offset;
			boolean friend = Huzuni.isFriend(target.getName()), sneaking = target.isSneaking();
			int color = friend ? 0xFF5ABDFC : sneaking ? 0xFF0080 : -1;
			glTranslatef((float) x, (float) y + target.height + 0.5F, (float) z);
			glNormal3f(0.0F, 1.0F, 0.0F);
			glRotatef(-mc.getRenderManager().playerViewY, 0.0F, 1.0F, 0.0F);
			if (mc.gameSettings.thirdPersonView == 2) {
				glRotatef(-mc.getRenderManager().playerViewX, 1.0F, 0.0F, 0.0F);
			} else {
				glRotatef(mc.getRenderManager().playerViewX, 1.0F, 0.0F, 0.0F);
			}
			glScalef(-scale, -scale, scale);

			if (doScale.isEnabled())
				glTranslatef(0F, -(nametagScale * 7), 0F);
			if (nametagScale > 1 && doScale.isEnabled() && (target instanceof EntityPlayer))
				glScalef(nametagScale, nametagScale, nametagScale);
			drawRect(rX, rY, rX1, rY1, tesselator);
			glEnable(GL_TEXTURE_2D);
			mc.fontRenderer.drawStringWithShadow(name, -mc.fontRenderer.getStringWidth(name) / 2, yOffset, color);
			if (responseTime != -1) {
				glDisable(GL_TEXTURE_2D);
				drawRect(-mc.fontRenderer.getStringWidth("" + responseTime) / 2 - 2, rY - yOffset, mc.fontRenderer.getStringWidth("" + responseTime) / 2 + 2, rY1 - yOffset - 1, tesselator);
				glEnable(GL_TEXTURE_2D);
				mc.fontRenderer.drawStringWithShadow("" + responseTime, -mc.fontRenderer.getStringWidth("" + responseTime) / 2, 0, 0xFFFFFF);
			}
			if (target instanceof EntityPlayer && armor.isEnabled()) {
				renderItems((EntityPlayer) target, rY);
			}
			glDisable(GL_TEXTURE_2D);
			GlStateManager.disableBlend();
		}
	}
}
