package net.halalaboos.huzuni.mod.mods.render;

import java.util.HashSet;

import net.halalaboos.huzuni.event.render.Renderable;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import pw.brudin.huzuni.util.render.OutlinedBox;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.ModeOption;
import net.halalaboos.huzuni.api.mod.ToggleOption;
import net.halalaboos.huzuni.event.render.RenderEvent;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.rendering.Box;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.GameUtils;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityTNTPrimed;
import net.minecraft.entity.player.EntityPlayer;
import org.lwjgl.input.Keyboard;
import com.google.common.collect.Sets;
import static org.lwjgl.opengl.GL11.*;

/**
 * @author brudin
 * @version 1.0
 * @since 4/10/14
 */
public class Tracer extends DefaultMod implements Renderable {

	public static final Tracer instance = new Tracer();
	
	private OutlinedBox[] outlinedBox = new OutlinedBox[99];
	
	private Box tntBox = null;

	private final ToggleOption players = new ToggleOption("Players", "Traces to players"), 
			team = new ToggleOption("Team", "Color the lines/boxes based on team mode"),
			mobs = new ToggleOption("Mobs", "Traces to mobs"), 
			animals = new ToggleOption("Animals", "Traces to animals"),
			boxes = new ToggleOption("Boxes", "Renders boxes"),
			lines = new ToggleOption("Lines", "Renders lines");
	
	private final ModeOption colorOption = new ModeOption("Color Option", "Color the lines/boxes based on... ", "Distance", "Theme");
	
	private Tracer() {
		super("Tracer", Keyboard.KEY_B);
		setVisible(false);
		setAuthor("brudin");
		setAliases("esp", "tracers", "playeresp");
		setDescription("Draws lines to players and draws boxes around them.");
		setCategory(Category.RENDER);
		setOptions(colorOption, team, players, mobs, animals, boxes, lines);
		boxes.setEnabled(true);
		lines.setEnabled(true);
		players.setEnabled(true);
		float width = 0.6F / 1.5F;
		float minX = -width, minY = 0, minZ = -width, maxX = width, maxY = 1.9F, maxZ = width;
		outlinedBox[6] = new OutlinedBox(new AxisAlignedBB(minX, minY, minZ, maxX, maxY, maxZ));
		tntBox = new Box(new AxisAlignedBB(0, 0, 0, 1, 1, 1));
	}

	@Override
	protected void onEnable() {
		Huzuni.registerRenderable(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterRenderable(this);
	}
	
	public void render(RenderEvent event) {
		for (Object o : mc.theWorld.loadedEntityList) {
			Entity entity = (Entity) o;
			if (entity == mc.thePlayer || entity.isDead || !shouldRender(entity))
				continue;
			float renderX = (float) (event.interpolate(entity.prevPosX, entity.posX) - mc.getRenderManager().renderPosX);
			float renderY = (float) (event.interpolate(entity.prevPosY, entity.posY) - mc.getRenderManager().renderPosY);
			float renderZ = (float) (event.interpolate(entity.prevPosZ, entity.posZ) - mc.getRenderManager().renderPosZ);
			float distance = mc.thePlayer.getDistanceToEntity(entity);
			boolean friend = entity instanceof EntityPlayer && Huzuni.isFriend(entity.getName());
			if (lines.isEnabled()) {
				colorLines(entity, distance, friend, 1F);

				glBegin(GL_LINES);
				glVertex3d(0.0F, mc.thePlayer.getEyeHeight(), 0.0F);
				glVertex3d(renderX, renderY, renderZ);
				glEnd();
			}

			if (boxes.isEnabled()) {
				colorLines(entity, distance, friend, 0.35F);
				glPushMatrix();
				glTranslatef(renderX, renderY, renderZ);
				glRotatef(-entity.rotationYaw, 0F, 1F, 0F);

				if (outlinedBox[(int) (entity.width * 10)] != null)
					outlinedBox[(int) (entity.width * 10)].render();
				else {
					double wX = entity.getEntityBoundingBox().maxX - entity.getEntityBoundingBox().minX,
							wY = entity.getEntityBoundingBox().maxY - entity.getEntityBoundingBox().minY,
							wZ = entity.getEntityBoundingBox().maxZ - entity.getEntityBoundingBox().minZ;
					double minX = -wX/2, minY = 0, minZ = -wZ/2, maxX = wX/2, maxY = wY, maxZ = wZ/2;
					outlinedBox[(int) (entity.width * 10)] = new OutlinedBox(new AxisAlignedBB(minX, minY, minZ, maxX, maxY, maxZ));
					outlinedBox[(int) (entity.width * 10)].render();
				}
				glPopMatrix();
			}
		}
	}
	
	private boolean shouldRender(Entity entity) {
		return GameUtils.checkType(entity, true, mobs.isEnabled(), animals.isEnabled(), players.isEnabled());
	}

	private void colorLines(Entity entity, float distance, boolean friend, float opacity) {
		if (friend) {
			GLUtils.glColor(0.35F, 0.75F, 1.0F, opacity);
		} else {
			if (Huzuni.getTeam().isEnabled() && team.isEnabled()) {
				if (Huzuni.getTeam().isTeam(entity)) {
					GLUtils.glColor(Huzuni.getTeam().getColor(), opacity);
					return;
				} else {
					int teamColor = Huzuni.getTeam().getTeamColor(entity);
					if (teamColor != -1) {
						GLUtils.glColor(teamColor, opacity);
						return;
					}
				}
			}
			if (colorOption.getSelected() == 0) {
				GLUtils.glColor(1F, distance / 64F, 0F, opacity);
			} else if (colorOption.getSelected() == 1) {
				GLUtils.glColor(Huzuni.getColorTheme(), opacity);
			}
		}
	}
}
