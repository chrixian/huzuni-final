package net.halalaboos.huzuni.mod.mods.unfinished;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.ToggleOption;
import net.halalaboos.huzuni.api.mod.ValueOption;
import net.halalaboos.huzuni.api.request.SimpleApplicant;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.queue.rotation.RotationData;
import net.halalaboos.huzuni.util.GameUtils;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemEgg;
import net.minecraft.item.ItemEnderPearl;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemSnowball;

public class BowAimbot extends DefaultMod implements UpdateListener {
	
	private final SimpleApplicant<Object> application = new SimpleApplicant<Object>(this);
	
	private Entity target = null;
	
	private final ToggleOption silent  = new ToggleOption("Silent", "Makes the aimbot invisible client-side"), 
			players = new ToggleOption("Players", "Attack players"), 
			mobs = new ToggleOption("Mobs", "Attack mobs"), 
			animals = new ToggleOption("Animals", "Attack animals"),
			invisible = new ToggleOption("Invisible", "Attack invisible entities");
	
	private final ValueOption reach = new ValueOption("Reach", 0F, 128F, 256F, 2F, "Aimbot reach distance");
	
	public BowAimbot() {
		super("Bow aimbot");
		this.setDescription("Calculates angles required to hit an object.");
		this.setCategory(Category.COMBAT);
		this.setAuthor("Stl Missouri");
		players.setEnabled(true);
		invisible.setEnabled(true);
		silent.setEnabled(true);
		setOptions(reach, silent, players, mobs, animals, invisible);
		Huzuni.rotationQueue.registerApplicant(application);
	}
	
	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
	}
	

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (!isUsingBow())
			return;
		target = GameUtils.getClosestEntity(reach.getValue(), 2.5F, invisible.isEnabled(), mobs.isEnabled(), animals.isEnabled(), players.isEnabled());
		if (target == null)
			return;

		int use = mc.thePlayer.getActiveItemStack().getMaxItemUseDuration() - mc.thePlayer.getItemInUseCount();
		float progress = use / 20.0F;
		progress = (progress * progress + progress * 2.0F) / 3.0F;
		if (progress >= 1.0F)
			progress = 1.0F;
		double v = progress * 3.0F;
		// Static MC gravity
		double g = 0.05F;
		setAngles(v, g);
	}
	
	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
	}
	
	
	private boolean isUsingBow() {
		if (mc.thePlayer.getActiveItemStack() != null) {
            Item item = mc.thePlayer.getActiveItemStack().getItem();
            if (!(item instanceof ItemBow || item instanceof ItemSnowball || item instanceof ItemEnderPearl || item instanceof ItemEgg || (item instanceof ItemPotion && mc.thePlayer.getActiveItemStack().getItemDamage() != 0)))
                return false;
            if ((item instanceof ItemBow)) {
                return true;
            }
            //} else if (item instanceof ItemPotion && player.getCurrentEquippedItem().getItemDamage() != 0) {
            //	mode = 2;
            //}
        }
		return false;
	}
	
	private void setAngles(double v, double g) {
        double pitch = -Math.toDegrees(getLaunchAngle(this.target, v, g));
        if(Double.isNaN(pitch))
            return;
        double difX = this.target.posX - mc.thePlayer.posX, difZ = this.target.posZ - mc.thePlayer.posZ;
        float yaw = (float) (Math.atan2(difZ, difX) * 180 / Math.PI) - 90;
		Huzuni.rotationQueue.request(application, new RotationData(yaw, (float) pitch, silent.isEnabled()));
	}

    /**
     * Gets launch angle required to hit a target with the specified velocity and gravity
     *
     * @param targetEntity Target entity
     * @param v            Projectile velocity
     * @param g            World gravity
     * @return
     */
    private float getLaunchAngle(Entity targetEntity, double v, double g) {
        double yDif = ((targetEntity.posY + (targetEntity.getEyeHeight() / 2)) - (mc.thePlayer.posY + mc.thePlayer.getEyeHeight()));
        double xDif = (targetEntity.posX - mc.thePlayer.posX);
        double zDif = (targetEntity.posZ - mc.thePlayer.posZ);

        /**
         * Pythagorean theorem to merge x/z
         *           /|
         *          / |
         * xCoord  /  | zDif
         *        /   |
         *       /    |
         *      /_____|
         * (player) xDif
         */
        double xCoord = Math.sqrt((xDif * xDif) + (zDif * zDif));

        return theta(v, g, xCoord, yDif);
    }

    /**
     * Calculates launch angle to hit a specified point based on supplied parameters
     *
     * @param v Projectile velocity
     * @param g World gravity
     * @param x x-coordinate
     * @param y y-coordinate
     * @return angle of launch required to hit point x,y
     * <p/>
     * Whoa there! You just supplied us with a method to hit a 2D point, but Minecraft is a 3D game!
     * <p/>
     * Yeah. Unfortunately this is 100x easier to do than write a method to find the 3D point,
     * so we can just merge the x/z axis of Minecraft into one (using the pythagorean theorem).
     * Have a look at getLaunchAngle to see how that's done
     */
    private float theta(double v, double g, double x, double y) {
        double yv = 2 * y * (v * v);
        double gx = g * (x * x);
        double g2 = g * (gx + yv);
        double insqrt = (v * v * v * v) - g2;
        double sqrt = Math.sqrt(insqrt);

        double numerator = (v * v) + sqrt;
        double numerator2 = (v * v) - sqrt;

        double atan1 = Math.atan2(numerator, g * x);
        double atan2 = Math.atan2(numerator2, g * x);

        /**
         * Ever heard of a quadratic equation? We're gonna have to have two different results
         * here, duh! It's probably best to launch at the smaller angle because that will
         * decrease the total flight time, thus leaving less room for error. If you're just
         * trying to impress your friends you could probably fire it at the maximum angle, but
         * for the sake of simplicity, we'll use the smaller one here.
         */
        return (float) Math.min(atan1, atan2);
    }

}
