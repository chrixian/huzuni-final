package net.halalaboos.huzuni.queue.packet;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.huzuni.util.Timer;
import net.minecraft.client.Minecraft;
import net.minecraft.network.Packet;

public class PacketQueue {

	private static final Timer timer = new Timer();

    private static final List<PacketData> packets = new ArrayList<PacketData>();
    
    public static void add(Packet packet, int waitTime) {
    	packets.add(new PacketData(packet, waitTime));
    	timer.reset();
    }
    
    public static void sendPackets() {
    	if (hasQueue()) {
    		PacketData packetData = packets.get(0);
    		if (timer.hasReach(packetData.waitTime)) {
    			Minecraft.getMinecraft().getNetHandler().addToSendQueue(packetData.packet);
    			packets.remove(packetData);
    			timer.reset();
    		}
    	} else
    		timer.reset();
    }
    
    public static boolean hasQueue() {
    	return !packets.isEmpty();
    }
}
