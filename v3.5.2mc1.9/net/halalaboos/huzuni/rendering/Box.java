package net.halalaboos.huzuni.rendering;

import net.minecraft.util.math.AxisAlignedBB;

import static org.lwjgl.opengl.GL11.*;

/**
 * @author brudin
 * @version 1.0
 * @since 4/10/14
 */
public class Box extends Vbo {

	private boolean opaque = false;

	public Box(AxisAlignedBB boundingBox, boolean opaque) {
		super();
		this.opaque = opaque;
		setup(boundingBox);
	}

	public Box(AxisAlignedBB boundingBox) {
		this(boundingBox, true);
	}

	public void setup(AxisAlignedBB boundingBox) {
		addVertex(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		addVertex(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		// left
		addVertex(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		addVertex(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		addVertex(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		addVertex(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		// right
		addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		// front
		addVertex(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		addVertex(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		// top
		addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);

		// bottom
		addVertex(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		addVertex(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		addVertex(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		addVertex(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		compile();
	}

	public void render() {
		this.render(opaque ? GL_QUADS : GL_LINE_STRIP);
	}

	public boolean isOpaque() {
		return opaque;
	}

	public void setOpaque(boolean opaque) {
		this.opaque = opaque;
	}
}
