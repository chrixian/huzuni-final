package net.halalaboos.huzuni.rendering;

import static org.lwjgl.opengl.GL11.*;

public class Circle extends Vbo {
	
	public Circle(float diameter, float slices) {
		super();
		setup(diameter, slices);
	}
	
	public Circle(float diameter) {
		this(diameter, 360);
	}
	
	public void setup(float diameter, float slices) {
		float radius = diameter / 2F;
		for (int i = 0; i < slices; i++) {
			float theta = 2.0F * (float) Math.PI * i / slices;
			addVertex(Math.cos(theta) * radius, Math.sin(theta) * radius, 0);
		}
        compile();
	}
	
	public void render() {
		this.render(GL_LINE_LOOP);
	}
	
}
