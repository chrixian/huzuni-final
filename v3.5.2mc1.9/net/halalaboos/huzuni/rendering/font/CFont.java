package net.halalaboos.huzuni.rendering.font;

import java.awt.*;
import java.awt.image.BufferedImage;

import net.halalaboos.huzuni.rendering.TextureUtils;
import static org.lwjgl.opengl.GL11.*;

public class CFont {
	
    protected int texId = -1;
    protected CharData[] charData = new CharData[256];
    protected Font font;
    protected boolean antiAlias;
    protected boolean fractionalMetrics;

    protected int fontHeight = -1;
    protected int kerning = 0;

    public CFont(Font font, boolean antiAlias, boolean fractionalMetrics) {
        this.font = font;
        this.antiAlias = antiAlias;
        this.fractionalMetrics = fractionalMetrics;
        setupTexture();
    }

	protected void setupTexture() {
		Object[] fontData = generateFontImage(font, antiAlias, fractionalMetrics, charData);
		BufferedImage img = (BufferedImage) fontData[0];
		charData = (CharData[]) fontData[1];
		texId = TextureUtils.applyTexture(texId == -1 ? TextureUtils.genTexture() : texId, img, antiAlias ? GL_LINEAR : GL_NEAREST, GL_REPEAT);
	}

    protected int setupTexture(int texId, BufferedImage img) {
        try {
            return TextureUtils.applyTexture(texId == -1 ? TextureUtils.genTexture() : texId, img, antiAlias ? GL_LINEAR : GL_NEAREST, GL_REPEAT);
        } catch (final NullPointerException e) {
            e.printStackTrace();
            return 0;
        }
    }
    
    protected Object[] generateFontImage(Font font, boolean antiAlias, boolean fractionalMetrics, CharData[] chars) {
    	// Image we'll use to store our font onto for rendering.
        BufferedImage bufferedImage = new BufferedImage(font.getSize() * 32, font.getSize() * 32, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = (Graphics2D) bufferedImage.getGraphics();
        g.setFont(font);

        // Give blank background
        g.setColor(new Color(255, 255, 255, 0));
        g.fillRect(0, 0, font.getSize() * 32, font.getSize() * 32);
        
        // Set color to white for rendering the font onto the texture.
        g.setColor(Color.WHITE);
        g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, fractionalMetrics ? RenderingHints.VALUE_FRACTIONALMETRICS_ON : RenderingHints.VALUE_FRACTIONALMETRICS_OFF);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, antiAlias ? RenderingHints.VALUE_TEXT_ANTIALIAS_ON : RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, antiAlias ? RenderingHints.VALUE_ANTIALIAS_ON : RenderingHints.VALUE_ANTIALIAS_OFF);
        FontMetrics fontMetrics = g.getFontMetrics();
        // Max height for the characters.
        int charHeight = 0;
        int positionX = 0;
        int positionY = 0;
        // Stolen from the Slick library, it loads the characters into the image used for font rendering.
        for (int i = 0; i < chars.length; i++) {
            char ch = (char) i;
            CharData charData = new CharData();

            int height = fontMetrics.getHeight();
            int width = fontMetrics.stringWidth(String.valueOf(ch));
            
            charData.width = width;
            charData.height = height;

            if (positionX + charData.width >= font.getSize() * 32) {
                positionX = 0;
                positionY += charHeight + 4;
                charHeight = 0;
            }

            if (charData.height > charHeight) {
            	charHeight = charData.height;
            }
            
            charData.storedX = positionX;
            charData.storedY = positionY;

            if (charData.height / 2 > fontHeight) {
                fontHeight = charData.height / 2;
            }
            
            chars[i] = charData;
            // Draw the char onto the final image we'll be using to render this font in game.
            g.drawString(String.valueOf(ch), positionX, positionY + fontMetrics.getAscent());

            positionX += charData.width + 4; // Add an extra pixel, so the characters have some space.
        }
        return new Object[] { bufferedImage, chars };
    }

    /**
     * Private drawing method used within other drawing methods.
     */
    public void drawChar(CharData[] chars, char c, float x, float y)
            throws ArrayIndexOutOfBoundsException {
        try {
            drawQuad(x, y, (float) chars[c].width / 2, (float) chars[c].height / 2, chars[c].storedX, chars[c].storedY, (float) chars[c].width, (float) chars[c].height);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Again, stolen from the Slick library. Renders at the given image coordinates.
     */
    protected void drawQuad(float x, float y, float width, float height, float srcX, float srcY, float srcWidth, float srcHeight) {
        float renderSRCX = (srcX) / (font.getSize() * 32),
                renderSRCY = (srcY) / (font.getSize() * 32),
                renderSRCWidth = (srcWidth) / (font.getSize() * 32),
                renderSRCHeight = (srcHeight) / (font.getSize() * 32);
        glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY);
        glVertex2d(x + width, y);
        glTexCoord2f(renderSRCX, renderSRCY);
        glVertex2d(x, y);
        glTexCoord2f(renderSRCX, renderSRCY + renderSRCHeight);
        glVertex2d(x, y + height);
        glTexCoord2f(renderSRCX, renderSRCY + renderSRCHeight);
        glVertex2d(x, y + height);
        glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY + renderSRCHeight);
        glVertex2d(x + width, y + height);
        glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY);
        glVertex2d(x + width, y);
    }

    /**
     * Renders the text.
     */
    public float drawString(String text, double x, double y,
                           Color color, boolean shadow) {
        glPushMatrix();
        glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, texId);
        glColor(shadow ? color.darker().darker() : color);
        int size = text.length();
        glBegin(GL_TRIANGLES);
        for (int i = 0; i < size; i++) {
            char character = text.charAt(i);
            if (character < charData.length && character >= 0) {
                drawChar(charData, character, (float) x, (float) y);
                x += (charData[character].width + kerning) / 2F;
            }
        }
        glEnd();
        glPopMatrix();
        return (float) x;
    }

    /**
     * OpenGL coloring.
     */
    public void glColor(Color color) {
        float red = (float) color.getRed() / 255F, green = (float) color.getGreen() / 255F, blue = (float) color.getBlue() / 255F, alpha = (float) color.getAlpha() / 255F;
        glColor4f(red, green, blue, alpha);
    }

    /**
     * @return The height of the given string.
     */
    public int getStringHeight(String text) {
    	int height = 0;
        for (char c : text.toCharArray()) {
            if (c < charData.length && c >= 0) {
            	if (charData[c].height / 2 > height)
            		height = charData[c].height / 2;
            }
        }
        return height;
    }

    /**
     * @return Total height that the current font can take up.
     */
    public int getHeight() {
        return fontHeight;
    }

    /**
     * @return The width of the given string.
     */
    public int getStringWidth(String text) {
        int width = 0;
        for (char c : text.toCharArray()) {
            if (c < charData.length && c >= 0)
                width += (charData[c].width + kerning) / 2F;
        }
        return width;
    }

    public boolean isAntiAlias() {
        return antiAlias;
    }

    public void setAntiAlias(boolean antiAlias) {
        if (this.antiAlias != antiAlias) {
            this.antiAlias = antiAlias;
            setupTexture();
        }
    }

    public boolean isFractionalMetrics() {
		return fractionalMetrics;
	}

	public void setFractionalMetrics(boolean fractionalMetrics) {
		if (this.fractionalMetrics != fractionalMetrics) {
			this.fractionalMetrics = fractionalMetrics;
        	setupTexture();
		}
	}

	public Font getFont() {
        return font;
    }
    
    public void setFont(Font font) {
    	this.font = font;
        setupTexture();
    }

    protected class CharData {
        public int width;
        public int height;
        public int storedX;
        public int storedY;
    }
}
