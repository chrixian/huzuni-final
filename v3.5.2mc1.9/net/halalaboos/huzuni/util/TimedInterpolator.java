package net.halalaboos.huzuni.util;

import java.awt.Color;

public class TimedInterpolator extends ColorInterpolator {

	private final Timer timer = new Timer();
	
	private int randomizationRate;
	
	private float ar, ag, ab, aa;
	
	public TimedInterpolator(int randomizationRate) {
		super();
		this.randomizationRate = randomizationRate;
		this.ar = red;
		this.ag = green;
		this.ab = blue;
		this.aa = alpha;
	}
	
	public void update(boolean keepAlpha, float coefficient) {
		if (timer.hasReach(randomizationRate) || (Math.abs(red - ar) <= 0.05F && Math.abs(green - ag) <= 0.05F && Math.abs(blue - ab) <= 0.05F)) {
			Color nice = getRandomColor(random);
			ar = nice.getRed() / 255F;
			ag = nice.getGreen() / 255F;
			ab = nice.getBlue() / 255F;
			aa = keepAlpha ? this.alpha : nice.getAlpha() / 255F;
			timer.reset();
		}
		this.interpolate(ar, ag, ab, aa, coefficient);
	}

	public int getRandomizationRate() {
		return randomizationRate;
	}

	public void setRandomizationRate(int randomizationRate) {
		this.randomizationRate = randomizationRate;
	}
	
}
