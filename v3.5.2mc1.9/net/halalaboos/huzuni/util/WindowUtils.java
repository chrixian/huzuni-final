package net.halalaboos.huzuni.util;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public final class WindowUtils {

	private WindowUtils() {
		
	}
	
	public static boolean displayQuestion(String title, String question) {
		return JOptionPane.showConfirmDialog(null, question, title, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
	}
	
	public static int displayOptionQuestion(String title, String question, Object[] options) {
		return JOptionPane.showOptionDialog(null, question, title, JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
	}
	
	public static File chooseFile(String chooseMessage) {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.showDialog(null, chooseMessage);
		return fileChooser.getSelectedFile();
	}
	
}
