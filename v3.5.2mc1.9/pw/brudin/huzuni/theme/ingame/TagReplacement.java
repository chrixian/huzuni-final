package pw.brudin.huzuni.theme.ingame;

/**
 * @author brudin
 * @version 1.0
 * @since 1:10 PM on 7/18/2014
 */
public class TagReplacement {
	public String originalName;
	public String newName;
	public int newColor;

	public TagReplacement(String originalName, String newName, int newColor) {
		this.originalName = originalName;
		this.newName = newName;
		this.newColor = newColor;
	}
}
