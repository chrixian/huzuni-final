package pw.brudin.huzuni.util.render;

import net.halalaboos.huzuni.rendering.Vbo;
import net.minecraft.util.math.AxisAlignedBB;

import static org.lwjgl.opengl.GL11.GL_LINES;

/**
 * @author brudin
 * @version 1.0
 * @since 3/25/14
 */
public class BoxBeams extends Vbo {

	public BoxBeams(AxisAlignedBB boundingBox) {
		setup(boundingBox);
	}

	public void setup(AxisAlignedBB boundingBox) {
		addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		addVertex(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		addVertex(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		addVertex(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		addVertex(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		compile();
	}


	public void render() {
		this.render(GL_LINES);
	}

}
